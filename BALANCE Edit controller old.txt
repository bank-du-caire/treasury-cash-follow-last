package com.bdc.treasury.web.screens.balance;

import com.bdc.treasury.entity.*;
import com.bdc.treasury.service.ClosedAccountService;
import com.bdc.treasury.service.OpeningBalanceValidationService;
import com.bdc.treasury.service.TransactionService;
import com.haulmont.cuba.core.entity.Entity;
import com.haulmont.cuba.core.global.CommitContext;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.Events;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.WindowContext;
import com.haulmont.cuba.gui.actions.list.CreateAction;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.model.DataContext;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.entity.Group;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@UiController("treasury_Balance.edit")
@UiDescriptor("balance-edit.xml")
@EditedEntityContainer("balanceDc")
@LoadDataBeforeShow
public class BalanceEdit extends StandardEditor<Balance> {
    @Inject
    private LookupField<String> makerStatus;
    @Inject
    private LookupField<String> checkerStatus;

    @Inject
    UserSession userSession;
    @Inject
    private InstanceContainer<Balance> balanceDc;
    @Inject
    private LookupField<String> depitCreditId;

    @Inject
    private CollectionContainer<CrossAccount> crossAccountsDc;

    @Inject
    private DataManager dataManager;
    @Inject
    private LookupField<Currency> currencyField;
    @Inject
    private TextField<BigDecimal> balanceField;
    @Inject
    private ClosedAccountService closedAccountService;
    @Inject
    private Notifications notifications;
    @Inject
    OpeningBalanceValidationService openingBalanceValidationService;
    public String eventState = "";
    @Inject
    private LookupField<CrossAccount> crossAccountField;

    public Balance globalbalance;
    @Inject
    private CollectionLoader<CrossAccount> crossAccountsDl;
    @Inject
    private DateField<LocalDate> statementDateField;
    @Inject
    private TextField<BigDecimal> balanceFieldMaker;
    @Inject
    private TextArea<String> descriptionField1;
    @Inject
    private Button excludeChecker;
    @Inject
    private Button addMaker;
    @Inject
    private TransactionService transactionService;
    @Inject
    private WindowContext windowContext;
    @Inject
    private Screens screens;


    @Subscribe
    public void onInit(InitEvent event) {
        // setup drop down list
        List<String> checkerList = new ArrayList<>();
        List<String> makerList = new ArrayList<>();
        List<String> makerDepitCreditStateList = new ArrayList<>();
        makerDepitCreditStateList.add(DepitCreditState.CREDIT.name());
        makerDepitCreditStateList.add(DepitCreditState.DEBIT.name());
        checkerList.add(CheckerStatus.RETURNED.name());
        checkerList.add(CheckerStatus.VALIDATED.name());
        makerList.add(Status.SAVED.name());
        makerList.add(Status.SUBMITTED.name());
        checkerStatus.setOptionsList(checkerList);
        makerStatus.setOptionsList(makerList);
        depitCreditId.setOptionsList(makerDepitCreditStateList);
//        crossAccountsDc.getMutableItems().clear();
        List<CrossAccount> accountList = new ArrayList<>();
        crossAccountField.setOptionsList(accountList);

    }

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        if (balanceDc.getItem().getCrossAccount() == null
                && balanceDc.getItem().getBalance() == null) {
            eventState = "create";
        } else {
            eventState = "edit";
            if (balanceDc.getItem().getStatus().equals(CheckerStatus.VALIDATED.name())) {
                currencyField.setEditable(false);
                currencyField.setEditable(false);
                crossAccountField.setEditable(false);
                statementDateField.setEditable(false);
                balanceFieldMaker.setEditable(false);
                depitCreditId.setEditable(false);
                makerStatus.setEditable(false);
                descriptionField1.setEditable(false);
                excludeChecker.setVisible(false);
                addMaker.setVisible(false);
            }
        }
        globalbalance = balanceDc.getItem();
    }


    @Subscribe(target = Target.DATA_CONTEXT)
    public void onPreCommit(DataContext.PreCommitEvent event) {
        LocalDate now = LocalDate.now();
        Group group = userSession.getUser().getGroup();
        Balance balance = balanceDc.getItem();
        if (balance.getCrossAccount() != null && balance.getCreationDate() != null) {
            if (closedAccountService.checkOfClosedCrossAccount(balance.getCrossAccount(), balance.getCreationDate(), CloseAccountState.CLOSED.name())) {
                event.preventCommit();
                notifications.create().withCaption("This account ( " + balance.getCrossAccount().getName() + " ) is closed with value date = " + balance.getCreationDate()).show();
            }
        }
        if (eventState == "create")
            balance.setCreationDate(now);
        balance.setGroupID(group.getId());
        if (depitCreditId.getValue() != null && depitCreditId.getValue().matches(DepitCreditState.CREDIT.name())) {
            balanceDc.getItem().setBalance(balanceField.getValue());
        }
        if (depitCreditId.getValue() != null && depitCreditId.getValue().matches(DepitCreditState.DEBIT.name())) {
            balanceDc.getItem().setBalance(BigDecimal.ZERO.subtract(balanceField.getValue()));
        }
//        if (eventState.equals("edit")) {
//            balanceDc.getItem().setBalance((balanceField.getValue().abs()));
//            if (balance != null)
//                openingBalanceValidationService.updateBalance(balance);
//        }
        boolean openingBalanceExist = openingBalanceValidationService.validateOpenBalanceExist(crossAccountField.getValue(), now);
        System.out.println("Exceed 1");
        if (openingBalanceExist && eventState.equals("create")) {
            event.preventCommit();
            notifications.create().withCaption("Opening Balance For This account ( " + balance.getCrossAccount().getName() + "  ) already is created with value date = " + balance.getCreationDate()).show();
        }
        if (balanceDc.getItem().getStatus().equals(CheckerStatus.VALIDATED.name()) &&
                openingBalanceExist && eventState.equals("edit")) {
            event.preventCommit();
            notifications.create().withCaption("Opening Balance For This account ( " + balance.getCrossAccount().getName() + "  ) already is created with value date = " + balance.getCreationDate()).show();
        }
        if (statementDateField.getValue() != null && statementDateField.getValue().compareTo(now) > 0) {
            event.preventCommit();
            notifications.create().withCaption("Statement Date should be equal or less than entry date").show();
        }
        if (currencyField.getValue() != null && crossAccountField.getValue() != null
                && !transactionService.checkCurrencyOfAccount(crossAccountField.getValue(), currencyField.getValue())) {
            event.preventCommit();
            notifications.create().withCaption("The Currency of this account not equal selected currency").show();
        }

        BigDecimal totalUnkown = BigDecimal.ZERO;
        BigDecimal totalExcluded = BigDecimal.ZERO;
        if (balanceDc.getItem().getBalancesList() != null)
            for (BalancesList balancesList : balanceDc.getItem().getBalancesList()) {
                totalExcluded = totalExcluded.add((balancesList.getExludedAmount() != null ? balancesList.getExludedAmount() : BigDecimal.ZERO));
                totalUnkown = totalUnkown.add((balancesList.getUnkownFunds() != null ? balancesList.getUnkownFunds() : BigDecimal.ZERO));
            }
        System.out.println("Exceed 2");
        balanceDc.getItem().setUnkownFunds(totalUnkown);
        balanceDc.getItem().setExludedAmount(totalExcluded);
        System.out.println("Exceed 3");
        if (balanceDc.getItem().getDepitCreditState().equals(DepitCreditState.CREDIT.name()))
            balanceDc.getItem().setBalance((balanceField.getValue().abs()));
        if (balanceDc.getItem().getDepitCreditState().equals(DepitCreditState.DEBIT.name()))
            balanceDc.getItem().setBalance(BigDecimal.ZERO.subtract(balanceField.getValue().abs()));
        try {
            dataManager.commit(balanceDc.getItem());
            System.out.println("Exceed 4");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //closeWithDefaultAction();

    }

   /* @Subscribe
    public void onAfterClose(AfterCloseEvent event) {
        notifications.create().withCaption(eventState).show();
        if (eventState.equals("edit") && globalbalance != null) {
            *//**//*
        }
    }*/


    @Subscribe("currencyField")
    public void onCurrencyFieldValueChange(HasValue.ValueChangeEvent<Currency> event) {
        if (currencyField.getValue() != null && currencyField.getValue().getName() != null) {
            List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
                    .query("select t from treasury_CrossAccount t where t.currency =:currency and t.status=:status")
                    .parameter("status", CheckerStatus.VALIDATED.name())
                    .parameter("currency", currencyField.getValue())
                    .view("crossAccount-view")
                    .list();
            crossAccountsDc.getMutableItems().clear();
            crossAccountsDc.getMutableItems().addAll(crossAccountList);
            crossAccountField.setOptionsList(crossAccountList);
        } else {
            List<CrossAccount> crossAccountList = new ArrayList<>();
            crossAccountField.setOptionsList(crossAccountList);
        }
//        else {
//            List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
//                    .query("select t from treasury_CrossAccount t ")
//                    .view("crossAccount-view")
//                    .list();
//            crossAccountsDc.getMutableItems().clear();
//            crossAccountsDc.getMutableItems().addAll(crossAccountList);
//            crossAccountField.setOptionsList(crossAccountList);
//        }
    }


}