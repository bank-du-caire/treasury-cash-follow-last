package com.bdc.treasury.web.screens.crossaccountholidays;

import com.bdc.treasury.entity.*;
import com.bdc.treasury.service.UserService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@UiController("treasury_CrossAccountHolidays.edit")
@UiDescriptor("cross-account-holidays-edit.xml")
@EditedEntityContainer("crossAccountHolidaysDc")
@LoadDataBeforeShow
public class CrossAccountHolidaysEdit extends StandardEditor<CrossAccountHolidays> {
    @Inject
    private UserService userService;
    @Inject
    private LookupField<String> statusField;
    private final static String Holiday_maker_RULE = "Holiday-maker";
    private final static String Holiday_checker_RULE = "Holiday-checker";
    @Inject
    private TextField<String> dayField;
    @Inject
    private TextField<String> monthField;
    @Inject
    private Table<CrossAccount> crossAccountTable;
    @Inject
    private Button addBtn;
    @Inject
    private Button excludeBtn;
    @Inject
    private GroupBoxLayout crossAccountBox;
    @Inject
    private InstanceContainer<CrossAccountHolidays> crossAccountHolidaysDc;
    @Inject
    private Notifications notifications;


    @Subscribe
    public void onInit(InitEvent event) {
        List<String> HolidayMakerList = new ArrayList<>();
        List<String> HolidayCheckerList = new ArrayList<>();

        HolidayMakerList.add(Status.SAVED.name());
        HolidayMakerList.add(Status.SUBMITTED.name());
        HolidayCheckerList.add(CheckerStatus.VALIDATED.name());
        HolidayCheckerList.add(CheckerStatus.RETURNED.name());
        if (userService.getUserRoles().contains(Holiday_maker_RULE)) {
            statusField.setOptionsList(HolidayMakerList);
        } else if (userService.getUserRoles().contains(Holiday_checker_RULE)) {
            dayField.setEditable(false);
            monthField.setEditable(false);
            crossAccountTable.setEditable(false);
            addBtn.setResponsive(false);
            excludeBtn.setVisible(false);
            crossAccountBox.setEnabled(false);
            statusField.setOptionsList(HolidayCheckerList);
        }
    }

    @Subscribe
    public void onBeforeCommitChanges(BeforeCommitChangesEvent event) {

        if (crossAccountHolidaysDc.getItem().getCrossAccount().stream().count() == 0) {
            event.preventCommit();
            notifications.create().withCaption("Please set Crosspondent Account").show();
        }
    }


}