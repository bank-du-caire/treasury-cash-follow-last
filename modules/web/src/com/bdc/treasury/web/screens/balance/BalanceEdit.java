package com.bdc.treasury.web.screens.balance;

import ch.qos.logback.classic.Logger;
import com.bdc.treasury.entity.*;
import com.bdc.treasury.entity.Currency;
import com.bdc.treasury.service.*;
import com.bdc.treasury.web.screens.FrontOfficeDetails;
import com.haulmont.cuba.core.entity.Entity;
import com.haulmont.cuba.core.global.*;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.ScreenBuilders;
import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.actions.list.CreateAction;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.model.DataContext;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.entity.Group;
import com.haulmont.cuba.security.global.UserSession;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.ConstraintViolation;
import javax.xml.validation.Validator;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.TextStyle;
import java.util.*;

@UiController("treasury_Balance.edit")
@UiDescriptor("balance-edit.xml")
@EditedEntityContainer("balanceDc")
@LoadDataBeforeShow
public class BalanceEdit extends StandardEditor<Balance> {
    @Inject
    private LookupField<String> makerStatus;
    @Inject
    private LookupField<String> checkerStatus;

    @Inject
    UserSession userSession;
    @Inject
    private InstanceContainer<Balance> balanceDc;
    @Inject
    private LookupField<String> depitCreditId;

    @Inject
    private CollectionContainer<CrossAccount> crossAccountsDc;

    @Inject
    private DataManager dataManager;
    @Inject
    private LookupField<Currency> currencyField;
    @Inject
    private TextField<BigDecimal> balanceField;
    @Inject
    private ClosedAccountService closedAccountService;
    @Inject
    private Notifications notifications;
    @Inject
    OpeningBalanceValidationService openingBalanceValidationService;
    public String eventState = "";
    @Inject
    private LookupField<CrossAccount> crossAccountField;

    public Balance globalbalance;
    @Inject
    private CollectionLoader<CrossAccount> crossAccountsDl;
    @Inject
    private DateField<LocalDate> statementDateField;
    @Inject
    private TextField<BigDecimal> balanceFieldMaker;
    @Inject
    private TextArea<String> descriptionField1;
    @Inject
    private Button excludeChecker;
    @Inject
    private Button addMaker;
    @Inject
    private TransactionService transactionService;
    @Inject
    private Screens screens;
    @Inject
    private Metadata metadata;
    @Inject
    private Form form;
    @Inject
    private Form makerForm;
    @Inject
    private ScreenBuilders screenBuilders;
    @Inject
    private Table<BalancesList> balancesListMaker;
    @Inject
    private DataContext dataContext;

    private final static String CHECKER_RULE = "Reconciliation-Role-Checker";
    private final static String MAKER_RULE = "Reconciliation-Role-Maker";
    @Inject
    private UserService userService;

    @Subscribe
    public void onInit(InitEvent event) {
        // setup drop down list
        List<String> checkerList = new ArrayList<>();
        List<String> makerList = new ArrayList<>();
        List<String> makerDepitCreditStateList = new ArrayList<>();
        makerDepitCreditStateList.add(DepitCreditState.CREDIT.name());
        makerDepitCreditStateList.add(DepitCreditState.DEBIT.name());
        checkerList.add(CheckerStatus.RETURNED.name());
        checkerList.add(CheckerStatus.VALIDATED.name());
        makerList.add(Status.SAVED.name());
        makerList.add(Status.SUBMITTED.name());
        checkerStatus.setOptionsList(checkerList);
        makerStatus.setOptionsList(makerList);
        depitCreditId.setOptionsList(makerDepitCreditStateList);
//        crossAccountsDc.getMutableItems().clear();
        List<CrossAccount> accountList = new ArrayList<>();
        crossAccountField.setOptionsList(accountList);

    }

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        if (balanceDc.getItem().getCrossAccount() == null
                && balanceDc.getItem().getBalance() == null) {
            eventState = "create";
        } else {
            eventState = "edit";
            if (balanceDc.getItem().getStatus().equals(CheckerStatus.VALIDATED.name())) {
                currencyField.setEditable(false);
                currencyField.setEditable(false);
                crossAccountField.setEditable(false);
                statementDateField.setEditable(false);
                balanceFieldMaker.setEditable(false);
                depitCreditId.setEditable(false);
                makerStatus.setEditable(false);
                descriptionField1.setEditable(false);
                excludeChecker.setVisible(false);
                addMaker.setVisible(false);
                List<String> rolesNames = userService.getUserRoles();
                if (rolesNames.contains(CHECKER_RULE) || rolesNames.contains(MAKER_RULE)) {
                    makerStatus.setEditable(false);
                    checkerStatus.setEditable(false);
                } else {
                    makerStatus.setEditable(true);
                    checkerStatus.setEditable(true);
                }
            }
        }
        globalbalance = balanceDc.getItem();
    }


    /*@Subscribe(target = Target.DATA_CONTEXT)
    public void onPreCommit(DataContext.PreCommitEvent event) {
        LocalDate now = LocalDate.now();
        Group group = userSession.getUser().getGroup();
        Balance balance = balanceDc.getItem();
        if (balance.getCrossAccount() != null && balance.getCreationDate() != null) {
            if (closedAccountService.checkOfClosedCrossAccount(balance.getCrossAccount(), balance.getCreationDate(), CloseAccountState.CLOSED.name())) {
                event.preventCommit();
                notifications.create().withCaption("This account ( " + balance.getCrossAccount().getName() + " ) is closed with value date = " + balance.getCreationDate()).show();
            }
        }
        if (eventState == "create")
            balance.setCreationDate(now);
        balance.setGroupID(group.getId());
        if (depitCreditId.getValue() != null && depitCreditId.getValue().matches(DepitCreditState.CREDIT.name())) {
            balanceDc.getItem().setBalance(balanceField.getValue());
        }
        if (depitCreditId.getValue() != null && depitCreditId.getValue().matches(DepitCreditState.DEBIT.name())) {
            balanceDc.getItem().setBalance(BigDecimal.ZERO.subtract(balanceField.getValue()));
        }
//        if (eventState.equals("edit")) {
//            balanceDc.getItem().setBalance((balanceField.getValue().abs()));
//            if (balance != null)
//                openingBalanceValidationService.updateBalance(balance);
//        }
        boolean openingBalanceExist = openingBalanceValidationService.validateOpenBalanceExist(crossAccountField.getValue(), now);
        if (openingBalanceExist && eventState.equals("create")) {
            event.preventCommit();
            notifications.create().withCaption("Opening Balance For This account ( " + balance.getCrossAccount().getName() + "  ) already is created with value date = " + balance.getCreationDate()).show();
        }
        if (balanceDc.getItem().getStatus().equals(CheckerStatus.VALIDATED.name()) &&
                openingBalanceExist && eventState.equals("edit")) {
            event.preventCommit();
            notifications.create().withCaption("Opening Balance For This account ( " + balance.getCrossAccount().getName() + "  ) already is created with value date = " + balance.getCreationDate()).show();
        }
        if (statementDateField.getValue() != null && statementDateField.getValue().compareTo(now) > 0) {
            event.preventCommit();
            notifications.create().withCaption("Statement Date should be equal or less than entry date").show();
        }
        if (currencyField.getValue() != null && crossAccountField.getValue() != null
                && !transactionService.checkCurrencyOfAccount(crossAccountField.getValue(), currencyField.getValue())) {
            event.preventCommit();
            notifications.create().withCaption("The Currency of this account not equal selected currency").show();
        }

        BigDecimal totalUnkown = BigDecimal.ZERO;
        BigDecimal totalExcluded = BigDecimal.ZERO;
        if (balanceDc.getItem().getBalancesList() != null)
            for (BalancesList balancesList : balanceDc.getItem().getBalancesList()) {
                totalExcluded = totalExcluded.add((balancesList.getExludedAmount() != null ? balancesList.getExludedAmount() : BigDecimal.ZERO));
                totalUnkown = totalUnkown.add((balancesList.getUnkownFunds() != null ? balancesList.getUnkownFunds() : BigDecimal.ZERO));
            }
        balanceDc.getItem().setUnkownFunds(totalUnkown);
        balanceDc.getItem().setExludedAmount(totalExcluded);
    }*/


    @Subscribe("currencyField")
    public void onCurrencyFieldValueChange(HasValue.ValueChangeEvent<Currency> event) {
        crossAccountField.clear();
        if (currencyField.getValue() != null && currencyField.getValue().getName() != null) {
            List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
                    .query("select t from treasury_CrossAccount t where t.currency =:currency and t.status=:status")
                    .parameter("status", CheckerStatus.VALIDATED.name())
                    .parameter("currency", currencyField.getValue())
                    .view("crossAccount-view")
                    .list();
            crossAccountsDc.getMutableItems().clear();
            crossAccountsDc.getMutableItems().addAll(crossAccountList);
            crossAccountField.setOptionsList(crossAccountList);
        } else {
            List<CrossAccount> crossAccountList = new ArrayList<>();
            crossAccountField.setOptionsList(crossAccountList);
        }
//        else {
//            List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
//                    .query("select t from treasury_CrossAccount t ")
//                    .view("crossAccount-view")
//                    .list();
//            crossAccountsDc.getMutableItems().clear();
//            crossAccountsDc.getMutableItems().addAll(crossAccountList);
//            crossAccountField.setOptionsList(crossAccountList);
//        }
    }


    @Inject
    protected ScreenValidation screenValidation;
    @Inject
    private Events events;
    @Inject
    private BeanValidation beanValidation;

    Balance balanceGlobalObject = new Balance();
    @Inject
    BalanceService balanceService;

    @Transactional
    @Subscribe("save")
    public void onSaveClick(Button.ClickEvent event) {
        boolean saveAllowed = true;
        ValidationErrors errors = screenValidation.validateUiComponents(makerForm);
        if (!errors.isEmpty()) {
            screenValidation.showValidationErrors(this, errors);
            return;
        }

        LocalDate now = LocalDate.now();
        Group group = userSession.getUser().getGroup();
        Balance balance = balanceDc.getItem();
        /*if (balance.getCrossAccount() != null && balance.getCreationDate() != null) {
            if (closedAccountService.checkOfClosedCrossAccount(balance.getCrossAccount(), balance.getCreationDate(), CloseAccountState.CLOSED.name())) {
                saveAllowed = false;
                notifications.create().withCaption("This account ( " + balance.getCrossAccount().getName() + " ) is closed with value date = " + balance.getCreationDate()).show();
            }
        }*/
        if (eventState == "create") {
            balance.setCreationDate(now);
            balance.setGroupID(group.getId());
        }
        if (depitCreditId.getValue() != null && depitCreditId.getValue().matches(DepitCreditState.CREDIT.name())) {
            balanceDc.getItem().setBalance(balanceField.getValue());
        }
        if (depitCreditId.getValue() != null && depitCreditId.getValue().matches(DepitCreditState.DEBIT.name())) {
            balanceDc.getItem().setBalance(BigDecimal.ZERO.subtract(balanceField.getValue()));
        }
//        if (eventState.equals("edit")) {
//            balanceDc.getItem().setBalance((balanceField.getValue().abs()));
//            if (balance != null)
//                openingBalanceValidationService.updateBalance(balance);
//        }
        boolean openingBalanceExist = openingBalanceValidationService.validateOpenBalanceExist(crossAccountField.getValue(), now);
        if (openingBalanceExist && eventState.equals("create")) {
            saveAllowed = false;
            notifications.create().withCaption("Opening Balance For This account ( " + balance.getCrossAccount().getName() + "  ) already is created with value date = " + balance.getCreationDate()).show();
        }
        if (balanceDc.getItem().getStatus().equals(CheckerStatus.VALIDATED.name()) &&
                openingBalanceExist && eventState.equals("edit")) {
            saveAllowed = false;
            notifications.create().withCaption("Opening Balance For This account ( " + balance.getCrossAccount().getName() + "  ) already is created with value date = " + balance.getCreationDate()).show();
        }
        if (statementDateField.getValue() != null && statementDateField.getValue().compareTo(now) > 0) {
            saveAllowed = false;
            notifications.create().withCaption("Statement Date should be equal or less than entry date").show();
        }
        if (currencyField.getValue() != null && crossAccountField.getValue() != null
                && !transactionService.checkCurrencyOfAccount(crossAccountField.getValue(), currencyField.getValue())) {
            saveAllowed = false;
            notifications.create().withCaption("The Currency of this account not equal selected currency").show();
        }
        BigDecimal totalUnkown = BigDecimal.ZERO;
        BigDecimal totalExcluded = BigDecimal.ZERO;
        if (balanceDc.getItem().getBalancesList() != null) {
            for (BalancesList balancesList : balanceDc.getItem().getBalancesList()) {
                totalExcluded = totalExcluded.add((balancesList.getExludedAmount() != null ? balancesList.getExludedAmount() : BigDecimal.ZERO));
                totalUnkown = totalUnkown.add((balancesList.getUnkownFunds() != null ? balancesList.getUnkownFunds() : BigDecimal.ZERO));
                balancesList.setOpeningbalance(balanceDc.getItem());
                /*balanceService.updateBalanceList(balancesList);*/
            }
        }
        balanceDc.getItem().setUnkownFunds(totalUnkown);
        balanceDc.getItem().setExludedAmount(totalExcluded);
        if (globalbalance.getDepitCreditState().equals(DepitCreditState.CREDIT.name()))
            balanceDc.getItem().setBalance((balanceField.getValue().abs()));
        if (globalbalance.getDepitCreditState().equals(DepitCreditState.DEBIT.name()))
            balanceDc.getItem().setBalance(BigDecimal.ZERO.subtract(balanceField.getValue().abs()));
        try {
            if (saveAllowed) {
                dataManager.commit(balanceDc.getItem());
                if (balanceDc.getItem().getBalancesList() != null) {
                    for (BalancesList balancesList : balanceDc.getItem().getBalancesList()) {
                        totalExcluded = totalExcluded.add((balancesList.getExludedAmount() != null ? balancesList.getExludedAmount() : BigDecimal.ZERO));
                        totalUnkown = totalUnkown.add((balancesList.getUnkownFunds() != null ? balancesList.getUnkownFunds() : BigDecimal.ZERO));
                        balancesList.setOpeningbalance(balanceDc.getItem());
                        balanceService.updateBalanceList(balancesList);
                    }
                }
                BalanceBrowse screen = screens.create(BalanceBrowse.class, OpenMode.NEW_TAB);
                screens.removeAll();
                screens.show(screen);
                // screenBuilders.screen(this).build().close(StandardOutcome.CLOSE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}