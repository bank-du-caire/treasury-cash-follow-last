package com.bdc.treasury.web.screens.currency;

import com.haulmont.cuba.gui.components.Action;
import com.haulmont.cuba.gui.components.TextField;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.treasury.entity.Currency;

import javax.inject.Inject;

@UiController("treasury_Currency.browse")
@UiDescriptor("currency-browse.xml")
@LookupComponent("currenciesTable")
@LoadDataBeforeShow
public class CurrencyBrowse extends StandardLookup<Currency> {
    @Inject
    private CollectionLoader<Currency> currenciesDl;
    @Inject
    private TextField<String> symbol;
    @Inject
    private TextField<String> currencyName;
    @Inject
    private TextField<String> code;

    @Subscribe("currenciesTable.clearFilter")
    public void onCustomerBasesTableClearFilter(Action.ActionPerformedEvent event) {
        symbol.setValue(null);
        currencyName.setValue(null);
        code.setValue(null);
    }
    @Subscribe
    public void onInit(InitEvent event) {
        currenciesDl.setMaxResults(10);
    }
}