package com.bdc.treasury.web.screens.closedcrossaccount;

import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.treasury.entity.ClosedCrossAccount;

import javax.inject.Inject;

@UiController("treasury_ClosedCrossAccount.browse")
@UiDescriptor("closed-cross-account-browse.xml")
@LookupComponent("closedCrossAccountsTable")
@LoadDataBeforeShow
public class ClosedCrossAccountBrowse extends StandardLookup<ClosedCrossAccount> {
    @Inject
    private CollectionLoader<ClosedCrossAccount> closedCrossAccountsDl;

    @Subscribe
    public void onInit(InitEvent event) {
        closedCrossAccountsDl.setMaxResults(10);
    }
}