package com.bdc.treasury.web.screens.users;

import com.bdc.treasury.entity.Status;
import com.bdc.treasury.entity.UserRole;
import com.bdc.treasury.service.BaseService;
import com.bdc.treasury.web.screens.transactionstransfered.TransactionsTransferedBrowse;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.PersistenceHelper;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.components.Component;
import com.haulmont.cuba.gui.components.GroupTable;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.treasury.entity.Users;
import com.haulmont.cuba.security.entity.Role;
import com.haulmont.cuba.security.global.UserSession;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.util.Optional;

@UiController("treasury_Users.edit")
@UiDescriptor("users-edit.xml")
@EditedEntityContainer("usersDc")
@LoadDataBeforeShow
public class UsersEdit extends StandardEditor<Users> {
    @Inject
    private InstanceContainer<Users> usersDc;
    @Inject
    private BaseService baseService;
    @Inject
    private UserSession userSession;
    @Inject
    private DataManager dataManager;
    @Inject
    private LookupField<Role> rolesLKUP;
    @Inject
    private GroupTable<UserRole> rolesTable;
    @Inject
    private CollectionContainer<UserRole> usersRolesDc;
    @Inject
    private CollectionLoader<UserRole> usersRolesDl;
    @Inject
    private Logger log;
    @Inject
    private Screens screens;
    @Inject
    private Notifications notifications;

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {

        Optional<Object> userid = Optional.of(this.getEditedEntity().getId());
        log.info(this.getEditedEntity().getId().toString());

        if (userid.isPresent())
            usersRolesDl.setParameter("userID", userid.get());
        getScreenData().loadAll();
    }

    @Subscribe("addRole")
    public void onAddRoleClick(Button.ClickEvent event) {
        if (rolesLKUP.getValue() != null) {
            boolean exist = usersRolesDc.getMutableItems().stream().anyMatch(e -> e.getRoleId().equals(rolesLKUP.getValue().getId()));
            log.info("exist =>" + exist);
            if (exist) {

                //TODO show notification

                return;
            }
            UserRole userRole = dataManager.create(UserRole.class);
            userRole.setRoleId(rolesLKUP.getValue().getId());
            userRole.setRoleName(rolesLKUP.getValue().getName());
            userRole.setUsersId(usersDc.getItem().getId());
            usersRolesDc.getMutableItems().add(userRole);
        }

    }


    @Subscribe("removeRole")
    public void onRemoveRoleClick(Button.ClickEvent event) {
        if (rolesTable.getItems() != null) {
            usersRolesDc.getMutableItems().remove(rolesTable.getSingleSelected());
        }
    }

    public void saveUserAndRoles(Component source) {
        if (userSession.getUser().getLogin().equals(usersDc.getItem().getName())) {
            notifications.create().withCaption("You can't edit in your user").show();
        } else {

            boolean wasNew = PersistenceHelper.isNew(usersDc.getItem());
            if (wasNew) {
                usersDc.getItem().setStatus(Status.CREATE_PENDING);
                baseService.addEditUser(this.getEditedEntity(), usersRolesDc.getItems());
                screens.removeAll();
                screens.create(UsersBrowse.class, OpenMode.NEW_TAB).show();
            } else {
                if (usersDc.getItem().getStatus().equals(Status.CREATE_PENDING)) {
                    baseService.updateUserAndRoles(this.getEditedEntity(), usersRolesDc.getItems());
                } else {
                    usersDc.getItem().setStatus(Status.UPDATE_PENDING);
                    baseService.updateUserAndRoles(this.getEditedEntity(), usersRolesDc.getItems());
                }
                screens.removeAll();
                screens.create(UsersBrowse.class, OpenMode.NEW_TAB).show();
            }
        }
    }

    @Subscribe
    public void onBeforeCommitChanges(BeforeCommitChangesEvent event) {
        if (userSession.getUser().getLogin().equals(usersDc.getItem().getName())) {
            event.preventCommit();
            notifications.create().withCaption("You can't edit in your user").show();
        }
    }
}