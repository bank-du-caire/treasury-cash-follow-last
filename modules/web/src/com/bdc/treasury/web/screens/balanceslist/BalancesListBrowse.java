package com.bdc.treasury.web.screens.balanceslist;

import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.treasury.entity.BalancesList;

import javax.inject.Inject;

@UiController("treasury_BalancesList.browse")
@UiDescriptor("balances-list-browse.xml")
@LookupComponent("table")
@LoadDataBeforeShow
public class BalancesListBrowse extends MasterDetailScreen<BalancesList> {
    @Inject
    private CollectionLoader<BalancesList> balancesListsDl;

    @Subscribe
    public void onInit(InitEvent event) {
        balancesListsDl.setMaxResults(10);
    }
    @Inject
    private CollectionContainer<BalancesList> balancesListsDc;


    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        balancesListsDc.getMutableItems().clear();
    }

}