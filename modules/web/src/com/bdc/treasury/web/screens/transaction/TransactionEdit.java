package com.bdc.treasury.web.screens.transaction;

import com.bdc.treasury.entity.*;
import com.bdc.treasury.entity.Currency;
import com.bdc.treasury.service.ClosedAccountService;
import com.bdc.treasury.service.CrossAccountHolidayService;
import com.bdc.treasury.service.TransactionService;
import com.bdc.treasury.service.UserService;
import com.google.common.collect.Sets;
import com.haulmont.cuba.core.entity.Entity;
import com.haulmont.cuba.core.entity.FileDescriptor;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.FileStorageException;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.actions.list.CreateAction;
import com.haulmont.cuba.gui.actions.list.RefreshAction;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.export.ExportDisplay;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.model.DataContext;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.gui.upload.FileUploadingAPI;
import com.haulmont.cuba.security.entity.Group;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.TextStyle;
import java.util.*;
import java.util.stream.Collectors;

@UiController("treasury_Transaction.edit")
@UiDescriptor("transaction-edit.xml")
@EditedEntityContainer("transactionDc")
@LoadDataBeforeShow
public class TransactionEdit extends StandardEditor<Transaction> {

    private final static String CHECKER_RULE = "checker";
    private final static String MAKER_RULE = "maker";
    private final static String FRONT_MAKER_RULE = "front-office-maker";
    private final static String FRONT_CHECKER_RULE = "front-office-checker";
    //    private final static String CONTROL_RULE = "control";
    private final static String INCOMING_RULE = "incoming";
    private final static String Checks_Group_Name = "FCY Checks Collection";
    private final static String CONTROL_maker_RULE = "control-maker";
    private final static String CONTROL_checker_RULE = "control-checker";
    public boolean attachTableVisabilty = false;
    @Inject
    private UserSession userSession;

    @Inject
    private LookupField<String> checkerStatusField;

    @Inject
    private LookupField<String> statusField;

    @Inject
    private InstanceContainer<Transaction> transactionDc;

    @Inject
    private Notifications notifications;

    @Inject
    private UserService userService;
    @Inject
    private TransactionService transactionService;
    @Inject
    private CrossAccountHolidayService crossAccountHolidayService;
    @Inject
    private DataManager dataManager;
    @Inject
    private CollectionContainer<CrossAccount> accountsDc;
    @Inject
    private LookupField<Currency> currencyField;
    @Inject
    private ClosedAccountService closedAccountService;
    private final static Long TEMP_TIME = 5L; // 5 minutes
    String oldStatus = new String();
    @Inject
    private DateField<LocalDate> valueDateField;
    @Inject
    private LookupField<CrossAccount> accountField;
    @Inject
    private TextField<BigDecimal> creditField;
    @Inject
    private TextField<BigDecimal> deptField;
    @Inject
    private Table<Attachement> attacheTable;
    @Inject
    private CollectionContainer<Attachement> attachementsDc;
    @Inject
    private ExportDisplay exportDisplay;
    @Inject
    private CollectionLoader<Attachement> attachementsDl;
    @Inject
    private FileMultiUploadField multiUploadField;
    @Inject
    private FileUploadingAPI fileUploadingAPI;
    @Inject
    private Metadata metadata;
    public List<Attachement> attachementList = new ArrayList<>();
    @Inject
    private GroupBoxLayout attacheBox;
    @Inject
    private Table<Attachement> attacheTableChecker;
    @Inject
    private GroupBoxLayout attacheBoxChecker;
    public boolean validatedStatus = false;
    @Inject
    private TextField<String> referenceNumberField;
    @Inject
    private TextField<String> makerDiscription;

    public boolean createState = false;
    @Inject
    private DataContext dataContext;

    @Subscribe
    public void onInit(InitEvent event) {
        List<String> checkerList = new ArrayList<>();
        List<String> makerList = new ArrayList<>();
        List<String> controlMakerList = new ArrayList<>();
        List<String> controlCheckerList = new ArrayList<>();

        checkerList.add(CheckerStatus.RETURNED.name());
        checkerList.add(CheckerStatus.VALIDATED.name());

        makerList.add(Status.SAVED.name());
        makerList.add(Status.SUBMITTED.name());

        controlMakerList.add(Status.SAVED.name());
        controlMakerList.add(Status.SUBMITTED.name());

        controlCheckerList.add(Status.TEMPORARY.name());
        controlCheckerList.add(CheckerStatus.RETURNED.name());
        controlCheckerList.add(CheckerStatus.VALIDATED.name());

        if (userService.getUserRoles().contains(CONTROL_maker_RULE) ||
                userService.getUserRoles().contains(FRONT_MAKER_RULE)) {
//            checkerStatusField.setOptionsList(controlList);
            statusField.setOptionsList(controlMakerList);

        } else if (userService.getUserRoles().contains(CONTROL_checker_RULE)) {
            checkerStatusField.setOptionsList(controlCheckerList);
        } else if (userService.getUserRoles().contains(CHECKER_RULE)) {
            checkerStatusField.setOptionsList(checkerList);

        } else {
            statusField.setOptionsList(makerList);
        }
        List<CrossAccount> accountList = new ArrayList<>();
        accountField.setOptionsList(accountList);
        //multiUploadField.setPermittedExtensions(Sets.newHashSet(".pdf", ".docx"));
        multiUploadField.addQueueUploadCompleteListener(queueUploadCompleteEvent -> {
            for (Map.Entry<UUID, String> entry : multiUploadField.getUploadsMap().entrySet()) {
                UUID fileId = entry.getKey();
                String fileName = entry.getValue();
                FileDescriptor fd = fileUploadingAPI.getFileDescriptor(fileId, fileName);
                if (fd != null && fileId != null && fileName != null) {
                    try {

                        fileUploadingAPI.putFileIntoStorage(fileId, fd);
                        Attachement attachement = metadata.create(Attachement.class);
                        attachement.setFileId(fd);
                        attachement.setFileName(fileName);
                        attachement.setTransactionCode(transactionDc.getItem().getId().toString());
                        attachementList.add(attachement);
                        attachementsDc.getMutableItems().add(attachement);
                    } catch (FileStorageException e) {
                        throw new RuntimeException("Error saving file to FileStorage", e);
                    }
                    dataManager.commit(fd);
                }
            }
            notifications.create()
                    .withCaption("Uploaded files: " + multiUploadField.getUploadsMap().values())
                    .show();

        });

        multiUploadField.addFileUploadErrorListener(queueFileUploadErrorEvent -> {
            notifications.create()
                    .withCaption("خطأ في ارفاق الملف")
                    .show();
        });
        if (userService.getUserRoles().contains(FRONT_MAKER_RULE)) {
            currencyField.setEditable(true);
            accountField.setEditable(true);
            statusField.setEditable(true);
            deptField.setEditable(true);
            creditField.setEditable(true);
            valueDateField.setEditable(true);
            referenceNumberField.setEditable(true);
            makerDiscription.setEditable(true);
        }
        if (userService.getUserRoles().contains(MAKER_RULE)) {
            makerDiscription.setVisible(true);
        }
    }

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        if (transactionDc.getItem() != null && transactionDc.getItem().getAccount() == null) {
            createState = true;
        }
        if (transactionDc.getItem() != null && transactionDc.getItem().getStatus() != null
                && transactionDc.getItem().getStatus().equals(CheckerStatus.VALIDATED.name())) {
            validatedStatus = true;
        }
        if (transactionDc.getItem() != null && transactionDc.getItem().getStatus() != null)
            oldStatus = transactionDc.getItem().getStatus();
        List<String> rolesNames = userService.getUserRoles();
        List<String> frontList = new ArrayList<>();
        if (rolesNames.contains(FRONT_CHECKER_RULE)) {
            checkerStatusField.setEditable(true);
            frontList.add(CheckerStatus.RETURNED.name());
            frontList.add(CheckerStatus.VALIDATED.name());
            checkerStatusField.setOptionsList(frontList);
        }
        // Exception
        if (rolesNames.contains(CHECKER_RULE) && !userService.getUserRoles().contains(INCOMING_RULE)
                && !userSession.getUser().getGroup().getName().equals(Checks_Group_Name)) {
            LocalDate now = LocalDate.now();
            LocalDate tomorrow = now.plusDays(1);
            List<String> checkerList = new ArrayList<>();
            if (transactionDc.getItem().getValueDate().equals(now)
                    || transactionDc.getItem().getValueDate().equals(tomorrow)) {
                checkerList.add(CheckerStatus.RETURNED.name());
                checkerList.add(CheckerStatus.EXCEPTION.name());
                checkerStatusField.setOptionsList(checkerList);
            } else {
                checkerList.add(CheckerStatus.RETURNED.name());
                checkerList.add(CheckerStatus.VALIDATED.name());
                checkerStatusField.setOptionsList(checkerList);
            }
        }
        attachementsDl.setParameter("transactionCode", transactionDc.getItem().getId().toString());
        getScreenData().loadAll();
        LocalDate now = LocalDate.now();
        LocalDate tomorrow = now.plusDays(1);
        if (transactionDc.getItem() != null && transactionDc.getItem().getValueDate() != null) {
            if (transactionDc.getItem().getValueDate().equals(now)
                    || transactionDc.getItem().getValueDate().equals(tomorrow)) {
                attacheBox.setVisible(true);
                attacheBoxChecker.setVisible(true);
            } else {
                if (attacheTable.getItems() != null && attacheTable.getItems().size() != 0) {
                    attacheBox.setVisible(true);
                    attacheBoxChecker.setVisible(true);
                } else {
                    attacheBox.setVisible(false);
                    attacheBoxChecker.setVisible(false);
                }
            }
        } else {
            attacheBox.setVisible(false);
            attacheBoxChecker.setVisible(false);
        }
        if (transactionDc.getItem() != null && transactionDc.getItem().getStatus() != null
                && transactionDc.getItem().getStatus().equals(CheckerStatus.EXCEPTION.name())) {
            attacheBoxChecker.setVisible(true);
        }
        if (userService.getUserRoles().contains(CONTROL_checker_RULE) ||
                userService.getUserRoles().contains(CONTROL_maker_RULE) ||
                userService.getUserRoles().contains(INCOMING_RULE) ||
                userService.getUserRoles().contains(FRONT_MAKER_RULE) ||
                userSession.getUser().getGroup().getName().equals(Checks_Group_Name)) {
            attacheBox.setVisible(false);
            attacheBoxChecker.setVisible(false);
        }
    }


    @Subscribe(target = Target.DATA_CONTEXT)
    public void onPreCommit(DataContext.PreCommitEvent event) {
        Group group = userSession.getUser().getGroup();
        List<String> rolesNames = userService.getUserRoles();
        Transaction transaction = transactionDc.getItem();

        if (!userService.getUserRoles().contains(CONTROL_maker_RULE) && !userService.getUserRoles().contains(CONTROL_checker_RULE)
                && !userService.getUserRoles().contains(FRONT_MAKER_RULE) && !userService.getUserRoles().contains(FRONT_CHECKER_RULE)) {
            transaction.setGroupID(group.getId());
            transaction.setGroupName(group.getName());
            if (valueDateField.getValue() == null) {
                event.preventCommit();
                notifications.create().withCaption("Value date must be filled").show();
            }
        }
        if (userService.getUserRoles().contains(CONTROL_maker_RULE)) {
            transaction.setGroupID(group.getId());
            transaction.setGroupName(group.getName());
        }
        if (userService.getUserRoles().contains(FRONT_MAKER_RULE) || userService.getUserRoles().contains(FRONT_CHECKER_RULE)
        ) {
            if (createState == true) {
                transaction.setGroupID(group.getId());
                transaction.setGroupName(group.getName());
                if (valueDateField.getValue() == null) {
                    event.preventCommit();
                    notifications.create().withCaption("Value date must be filled").show();
                }
            }
        }
        LocalDate valueDate = transaction.getValueDate();
        LocalDate now = LocalDate.now();
        LocalDate nowPlusOne = now.plusDays(1);
        if (valueDate != null && valueDate.isBefore(now)) {
            if (!rolesNames.contains(CONTROL_maker_RULE) &&
                    !rolesNames.contains(CONTROL_checker_RULE)) {
                event.preventCommit();
                notifications.create().withCaption("Value date must be equal or plus current date").show();
            }
        }
        if (transaction.getAccount() != null && transaction.getValueDate() != null) {
            if (valueDate != null && !valueDate.isBefore(now) && crossAccountHolidayService.checkHolidaysOfCrossAccount(transaction.getAccount(), transaction.getValueDate())) {
                event.preventCommit();
                notifications.create().withCaption("The value date of this correspondent account ( " + transaction.getAccount().getName() + " ) matches with a holiday day.").show();
            }
        }
        if (transaction.getAccount() != null && transaction.getValueDate() != null) {
            if (valueDate != null && !valueDate.isBefore(now) && closedAccountService.checkOfClosedCrossAccount(transaction.getAccount(), transaction.getValueDate(), CloseAccountState.CLOSED.name(),group)) {
                event.preventCommit();
                notifications.create().withCaption("This account ( " + transaction.getAccount().getName() + " ) is closed with value date = " + transaction.getValueDate()).show();
            }
        }

        if (oldStatus.equals(Status.TEMPORARY.name())) {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date currentDate = new Date();
            Date updateTs = transaction.getUpdateTs();
            LocalDateTime localDateTimeUpdateTs = new Timestamp(updateTs.getTime()).toLocalDateTime();
            LocalDateTime currentLocalDateTime = LocalDateTime.now();
            //check same date
            boolean condition1 = fmt.format(transaction.getUpdateTs()).equals(fmt.format(currentDate));
            boolean condition2 = currentLocalDateTime.getHour() == localDateTimeUpdateTs.getHour();
            boolean condition3 = currentLocalDateTime.getMinute() - localDateTimeUpdateTs.getMinute() <= TEMP_TIME;
            if ((condition1 && condition2 && condition3) == false) {
                event.preventCommit();
                notifications.create().withCaption("Time expired to edit this transaction").show();
            }
        }
        transactionDc.getItem().setCreationDate(now);
        if (userService.getUserRoles().contains(CONTROL_maker_RULE) ||
                userService.getUserRoles().contains(INCOMING_RULE)) {
            if (creditField.getValue() != null && deptField.getValue() != null) {
                event.preventCommit();
                notifications.create().withCaption("Not Allowed to add credit and debit Together").show();
            }
        }
        /*if (rolesNames.contains(CONTROL_checker_RULE) && transactionDc.getItem().getGroupName().equals("Treasury Control") && transactionDc.getItem().getStatus().equals(CheckerStatus.RETURNED.name())) {
            event.preventCommit();
            notifications.create().withCaption("Not allowed to return this Transaction").show();
        }*/
        if (!rolesNames.contains(CONTROL_checker_RULE) && validatedStatus &&
                transactionDc.getItem().getStatus().equals(CheckerStatus.RETURNED.name())) {
            event.preventCommit();
            notifications.create().withCaption("Not allowed to return validated Transaction").show();
        }
        /*if (currencyField.getValue() != null && accountField.getValue() != null
                && !transactionService.checkCurrencyOfAccount(accountField.getValue(), currencyField.getValue())) {
            event.preventCommit();
            notifications.create().withCaption("The Currency of this account not equal selected currency").show();
        }*/
        if (!userService.getUserRoles().contains(CONTROL_maker_RULE)
                && !userService.getUserRoles().contains(CONTROL_checker_RULE)
                && !userService.getUserRoles().contains(FRONT_MAKER_RULE)
                && !userService.getUserRoles().contains(FRONT_CHECKER_RULE)
                && !userService.getUserRoles().contains(INCOMING_RULE)
                && !userSession.getUser().getGroup().getName().equals(Checks_Group_Name)
                && valueDate != null) {
            if (checkExceptionAfterHoliday()) {
                if (attacheTable.getItems().size() == 0) {
                    event.preventCommit();
                    notifications.create().withCaption("You should attach email").show();
                }
            }
        }
        if (!userService.getUserRoles().contains(CONTROL_maker_RULE)
                && !userService.getUserRoles().contains(CONTROL_checker_RULE)
                && !userService.getUserRoles().contains(FRONT_MAKER_RULE)
                && !userService.getUserRoles().contains(FRONT_CHECKER_RULE)
                && !userService.getUserRoles().contains(INCOMING_RULE)
                && !userSession.getUser().getGroup().getName().equals(Checks_Group_Name)
                && valueDate != null) {
            if (valueDate.equals(now) || valueDate.equals(nowPlusOne)) {
                if (attacheTable.getItems().size() == 0) {
                    event.preventCommit();
                    notifications.create().withCaption("You should attach email").show();
                }
            }
        }
        for (Attachement attachement1 : attachementList) {
            Attachement attachement2 = metadata.create(Attachement.class);
            attachement2.setFileId(attachement1.getFileId());
            attachement2.setTransactionCode(attachement1.getTransactionCode());
            dataManager.commit(attachement2);
        }
        List<String> arabicWeekend = new ArrayList<>();
        arabicWeekend.add("SAR");
        arabicWeekend.add("AED");
        arabicWeekend.add("QAR");
        arabicWeekend.add("KWD");
        String day = "";
        if (transactionDc.getItem().getValueDate() != null) {
            day = transactionDc.getItem().getValueDate().getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.ENGLISH);
        }

        if (currencyField.getValue() != null &&
                arabicWeekend.contains(currencyField.getValue().getName())) {
            if (day.equals("Fri") || day.equals("Sat")) {
                event.preventCommit();
                notifications.create().withCaption("This value date (" + valueDateField.getValue() + ") is a weekend = (" + day + ")").show();
            }
        } else {// Foreign currency
            if (day.equals("Sat") || day.equals("Sun")) {
                event.preventCommit();
                notifications.create().withCaption("This value date (" + valueDateField.getValue() + ") is a weekend = (" + day + ")").show();
            }
        }
    }

    @Subscribe("valueDateField")
    public void onValueDateFieldValueChange(HasValue.ValueChangeEvent<LocalDate> event) {
        List<String> rolesNames = userService.getUserRoles();
        if (rolesNames.contains(MAKER_RULE) && !userService.getUserRoles().contains(INCOMING_RULE)
                && !userSession.getUser().getGroup().getName().equals(Checks_Group_Name)) {
            LocalDate now = LocalDate.now();
            LocalDate tomorrow = now.plusDays(1);
            List<String> checkerList = new ArrayList<>();
            if (transactionDc.getItem().getValueDate().equals(now)
                    || transactionDc.getItem().getValueDate().equals(tomorrow)) {
                notifications.create().withCaption("You are trying to add an exception transaction").show();
                attacheBox.setVisible(true);
            } else {
                attacheBox.setVisible(false);
            }
            checkExceptionAfterHoliday();
        }
    }

    public boolean checkExceptionAfterHoliday() {
        boolean b = false;
        LocalDate valueDateSelected = null;
        LocalDate now = LocalDate.now();
        LocalDate tempDate = now;
        if (transactionDc.getItem().getAccount() != null && transactionDc.getItem().getAccount().getName() != null
                && transactionDc.getItem().getValueDate() != null) {
            valueDateSelected = transactionDc.getItem().getValueDate();
            do {
                tempDate = tempDate.plusDays(1);
                if (!tempDate.equals(valueDateSelected)) {
                    if (crossAccountHolidayService.checkHolidaysOfCrossAccount(transactionDc.getItem().getAccount(), tempDate)) {
                        attacheBox.setVisible(false);
                        // notifications.create().withCaption("You are trying to add an exception transaction").show();
                    } else {
                        break;
                    }
                } else {
                    attacheBox.setVisible(true);
                    notifications.create().withCaption("You are trying to add an exception transaction").show();
                    b = true;
                }
            } while (!tempDate.equals(valueDateSelected));
        }
        if (transactionDc.getItem().getValueDate() != null && transactionDc.getItem().getValueDate().equals(now)) {
            attacheBox.setVisible(true);
            b = true;
        }
        return b;
    }

    @Install(to = "accountsDl", target = Target.DATA_LOADER)
    private List<CrossAccount> accountsDlLoadDelegate(LoadContext<CrossAccount> loadContext) {
        return new ArrayList<>();
    }

    @Subscribe("currencyField")
    public void onCurrencyFieldValueChange(HasValue.ValueChangeEvent<Currency> event) {
        //transactionDc.getItem().setAccount(new CrossAccount());
        accountField.clear();
        if (currencyField.getValue() != null && currencyField.getValue().getName() != null) {
            List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
                    .query("select t from treasury_CrossAccount t where t.currency =:currency and t.status=:status")
                    .parameter("status", CheckerStatus.VALIDATED.name())
                    .parameter("currency", currencyField.getValue())
                    .view("crossAccountCurrency-view")
                    .list();
            accountsDc.getMutableItems().clear();
            accountsDc.getMutableItems().addAll(crossAccountList);
            accountField.setOptionsList(crossAccountList);

        } else {
            List<CrossAccount> crossAccountList = new ArrayList<>();
            accountField.setOptionsList(crossAccountList);
        }


    }

    /*@Subscribe(id = "accountsDc", target = Target.DATA_CONTAINER)
    public void onAccountsDcItemPropertyChange(InstanceContainer.ItemPropertyChangeEvent<CrossAccount> event) {
        itemPropertyChanged(event);
    }


    @Install(to = "accountField", subject = "lookupSelectHandler")
    private void accountFieldLookupSelectHandler(Collection collection) {
        collection.clear();
        collection.add(new CrossAccount());
    }

    private void itemPropertyChanged(InstanceContainer.ItemPropertyChangeEvent<? extends Entity> event) {
        String msg = event.getItem().getClass().getSimpleName() + "." + event.getProperty() + " = " + event.getValue();
        event.getItem().setValue("",new CrossAccount());

        notifications.create()
                .withCaption(msg)
                .show();
    }*/

   /* @Subscribe("accountField")
    public void onAccountFieldValueChange(HasValue.ValueChangeEvent<CrossAccount> event) {


        notifications.create()
                .withCaption("Before: " + event.getPrevValue() +
                        ". After: " + event.getValue())
                .show();
    }*/

    public void deleteFile() throws FileStorageException {
        if (attacheTable.getSingleSelected() != null) {
            dataManager.remove(attacheTable.getSingleSelected());
            attachementsDc.getMutableItems().clear();
            attachementsDc.getMutableItems().addAll(refreshAttachementTable());
            notifications.create().withCaption("تم حذف الملف بنجاح").show();
        } else {
            notifications.create().withCaption("يجب إختيار ملف").show();
        }
    }

    public void downloadFile() {
        if (attacheTable.getSingleSelected() != null) {
            FileDescriptor fd = attacheTable.getSingleSelected().getFileId();
            exportDisplay.show(fd);
        } else {
            notifications.create().withCaption("يجب إختيار ملف").show();
        }
    }

    public void downloadFileChecker() {
        if (attacheTableChecker.getSingleSelected() != null) {
            FileDescriptor fd = attacheTableChecker.getSingleSelected().getFileId();
            exportDisplay.show(fd);
        } else {
            notifications.create().withCaption("You should select raw").show();
        }
    }

    public List<Attachement> refreshAttachementTable() {
        return dataManager.load(Attachement.class)
                .query("select e from treasury_Attachement e where e.transactionCode =:transactionCode ")
                .parameter("transactionCode", transactionDc.getItem().getId().toString())
                .view("attachement-view")
                .list();
    }

}