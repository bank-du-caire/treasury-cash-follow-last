package com.bdc.treasury.web.screens.treasuryctrl;

import com.bdc.treasury.entity.*;
import com.bdc.treasury.service.TresuryControlService;
import com.bdc.treasury.web.screens.FrontOfficeDetails;
import com.bdc.treasury.web.screens.Treasuryctrldetails;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.core.global.MetadataTools;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.components.actions.BaseAction;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.gui.screen.LookupComponent;

import javax.inject.Inject;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@UiController("treasury_TreasuryCtrl.browse")
@UiDescriptor("treasury-ctrl-browse.xml")
@LookupComponent("treasuryCtrlsTable")
@LoadDataBeforeShow
public class TreasuryCtrlBrowse extends StandardLookup<TreasuryCtrl> {

    @Inject
    TresuryControlService tresuryControlService;
    @Inject
    private LookupField<CrossAccount> crossAccount;
    @Inject
    private CollectionContainer<TreasuryCtrl> treasuryCtrlsDc;
    @Inject
    private DatePicker<LocalDate> dateField;
    @Inject
    private Notifications notifications;
    public List<TreasuryCtrl> treasuryCtrlFilteredList = new ArrayList<>();
    @Inject
    private LookupField<Currency> currency;
    @Inject
    private DataManager dataManager;
    @Inject
    private CollectionContainer<CrossAccount> crossAccountsDc;
    @Inject
    private GroupTable<TreasuryCtrl> treasuryCtrlsTable;
    @Inject
    private Screens screens;
    @Inject
    private MetadataTools metadataTools;

    @Install(to = "treasuryCtrlsDl", target = Target.DATA_LOADER)
    private List<TreasuryCtrl> treasuryCtrlsDlLoadDelegate(LoadContext<TreasuryCtrl> loadContext) throws ParseException {
        List<TreasuryCtrl> treasuryCtrlList = new ArrayList<>();
//        if (startDate.getValue() != null && endDate.getValue() != null) {
//            treasuryCtrlList = loadTreasuryCtrlsData(startDate.getValue(), endDate.getValue());
//        }
        return treasuryCtrlList;
    }

    public List<TreasuryCtrl> loadTreasuryCtrlsData(LocalDate startDate) throws ParseException {
        return tresuryControlService.getTreasuryCtrls(startDate);
    }

    @Subscribe("crossAccount")
    public void onCrossAccountValueChange(HasValue.ValueChangeEvent<CrossAccount> event) throws ParseException {
        if (!dateField.isEmpty() && dateField.getValue() != null) {
            treasuryCtrlFilteredList = tresuryControlService.getTreasuryCtrls(dateField.getValue()).stream().filter(treasuryCtrl -> {
                if (treasuryCtrl != null && crossAccount.getValue() != null && crossAccount.getValue().getName() != null && currency.getValue() != null &&
                        currency.getValue().getCode() != null && crossAccount.getValue().getCurrency() != null
                        && crossAccount.getValue().getCurrency().getCode().equals(currency.getValue().getCode()))
                    return treasuryCtrl.getCorrespondentName().getName().equals(crossAccount.getValue().getName()) && treasuryCtrl.getCorrespondentName().getCurrency().equals(currency.getValue());
                return false;
            }).collect(Collectors.toList());
            treasuryCtrlsDc.getMutableItems().clear();
            treasuryCtrlsDc.getMutableItems().addAll(treasuryCtrlFilteredList);
        } else {
            notifications.create().withCaption("Must select  date ").show();
        }
    }

    @Subscribe("treasuryCtrlsTable.clearFilter")
    public void onTreasuryCtrlsTableClearFilter(Action.ActionPerformedEvent event) throws ParseException {
        currency.setValue(null);
        crossAccount.setValue(null);
        treasuryCtrlsDc.getMutableItems().clear();
        treasuryCtrlsDc.getMutableItems().addAll(loadTreasuryCtrlsData(dateField.getValue()));

    }

    @Subscribe("dateField")
    public void onDateFieldValueChange(HasValue.ValueChangeEvent<LocalDate> event) throws ParseException {

        if (!dateField.isEmpty() && dateField.getValue() != null) {
            treasuryCtrlFilteredList = tresuryControlService.getTreasuryCtrls(dateField.getValue());
            treasuryCtrlsDc.getMutableItems().clear();
            treasuryCtrlsDc.getMutableItems().addAll(treasuryCtrlFilteredList);
        } else {
            notifications.create().withCaption("Must select  date ").show();
        }
    }

    @Subscribe
    public void onInit(InitEvent event) {
        List<CrossAccount> accountList = new ArrayList<>();
        crossAccount.setOptionsList(accountList);
        treasuryCtrlsTable.setItemClickAction(new BaseAction("itemClickAction")
                .withHandler(actionPerformedEvent -> {
                    Treasuryctrldetails screen = screens.create(Treasuryctrldetails.class, OpenMode.NEW_TAB);
                    screen.setCrossAccount(treasuryCtrlsTable.getSingleSelected().getCorrespondentName());
                    if (dateField.getValue() != null) {
                        screen.setValueDate(dateField.getValue());
                        screens.show(screen);
                    }

                }));
        treasuryCtrlsTable.setEnterPressAction(new BaseAction("enterPressAction")
                .withHandler(actionPerformedEvent -> {
                    TreasuryCtrl treasuryCtrl = treasuryCtrlsTable.getSingleSelected();
                    if (treasuryCtrl != null) {
//                        popupView.setPopupVisible(true);
                        notifications.create()
                                .withCaption("Enter pressed for: " + metadataTools.getInstanceName(treasuryCtrl))
                                .show();
                    }
                }));

    }
    @Subscribe("currency")
    public void onCurrencyValueChange(HasValue.ValueChangeEvent<Currency> event) throws ParseException {
        if (currency.getValue() != null && currency.getValue().getName() != null) {
            List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
                    .query("select t from treasury_CrossAccount t where t.currency =:currency and t.status=:status")
                    .parameter("status", CheckerStatus.VALIDATED.name())
                    .parameter("currency", currency.getValue())
                    .view("crossAccount-view")
                    .list();
            crossAccountsDc.getMutableItems().clear();
            crossAccountsDc.getMutableItems().addAll(crossAccountList);
            crossAccount.setOptionsList(crossAccountList);
        } else {
            List<CrossAccount> crossAccountList = new ArrayList<>();
            crossAccount.setOptionsList(crossAccountList);
        }
        if (!dateField.isEmpty() && dateField.getValue() != null) {
            treasuryCtrlFilteredList = tresuryControlService.getTreasuryCtrls(dateField.getValue()).stream().filter(treasuryCtrl -> {
                if (currency.getValue() != null && currency.getValue().getCode() != null) {
                    if (treasuryCtrl.getCurrency() != null)
                        return treasuryCtrl.getCurrency().equals(currency.getValue().getName());
                }
                return false;
            }).collect(Collectors.toList());
            treasuryCtrlsDc.getMutableItems().clear();
            treasuryCtrlsDc.getMutableItems().addAll(treasuryCtrlFilteredList);
        } else {
            notifications.create().withCaption("Must select  date ").show();
        }
    }


}