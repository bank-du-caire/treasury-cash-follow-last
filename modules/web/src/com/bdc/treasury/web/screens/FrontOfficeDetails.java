package com.bdc.treasury.web.screens;

import com.bdc.treasury.entity.*;
import com.bdc.treasury.service.DealsService;
import com.bdc.treasury.service.RemTransactionService;
import com.bdc.treasury.service.TresuryControlService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.screen.Screen;
import com.haulmont.cuba.gui.screen.Subscribe;
import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;
import com.haulmont.cuba.security.entity.Group;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@UiController("treasury_FrontOfficeDetails")
@UiDescriptor("front-office-details.xml")
public class FrontOfficeDetails extends Screen {
    public LocalDate valueDate;
    public CrossAccount crossAccount;

    @Inject
    private DataManager dataManager;
    @Inject
    private GroupTable<TreasuryFrontOffice> treasuryFrontOfficeTable;
    @Inject
    private CollectionContainer<TreasuryFrontOffice> treasuryFrontOfficesDc;
    @Inject
    private TextField<String> account;
    @Inject
    private TextField<LocalDate> lastOpeningDateField;
    @Inject
    private RemTransactionService remTransactionService;
    @Inject
    private DealsService dealsService;
    @Inject
    private TextField<BigDecimal> openingBalanceField;
    @Inject
    private TextField<BigDecimal> totalExcludeAmountField;
    @Inject
    private TextField<BigDecimal> totalUnkownFundsField;
    @Inject
    private TresuryControlService treasuryFrontOfficeService;

    public LocalDate getValueDate() {
        return valueDate;
    }

    public void setValueDate(LocalDate valueDate) {
        this.valueDate = valueDate;
    }

    public CrossAccount getCrossAccount() {
        return crossAccount;
    }

    public void setCrossAccount(CrossAccount crossAccount) {
        this.crossAccount = crossAccount;
    }


    List<TreasuryFrontOffice> treasuryFrontOfficeListFinal = new ArrayList<>();
    List<TreasuryFrontOffice> treasuryFrontOfficeListFilterd = new ArrayList<>();

    public List<Transaction> getTransactionWithoutValueDate() {
        List<Transaction> transactionList = new ArrayList<>();
        return transactionList = dataManager.load(Transaction.class)
                .query("select e from treasury_Transaction e where e.account =:account and  e.valueDate is null and e.deleteTs is null and (e.status = :stat1 or e.status = :stat2) ")
                .parameter("account", crossAccount)
                .parameter("stat1", CheckerStatus.VALIDATED.name())
                .parameter("stat2", Status.TEMPORARY.name())
                .view("transaction-view")
                .list();
    }

    public List<TreasuryFrontOffice> loadTransaction() {
        // Get last date of opening balance
        Timestamp lastOpeningBalanceDateTS = null;
        LocalDate lastOpeningBalanceDate = null;
        if (!treasuryFrontOfficeService.validateExistOpeningBalance(crossAccount, valueDate)) {
            lastOpeningBalanceDateTS = treasuryFrontOfficeService.getLastCreationDateOfOpeningBalance(crossAccount, valueDate);
            if (lastOpeningBalanceDateTS != null) {
                lastOpeningBalanceDate = (lastOpeningBalanceDateTS.toLocalDateTime().toLocalDate());
            }
        } else {
            lastOpeningBalanceDate = valueDate;
        }
        List<Balance> balances = loadOpeningBalancesList(crossAccount, lastOpeningBalanceDate);
        BigDecimal totalExludedAmount = BigDecimal.ZERO;
        BigDecimal totalUnkownFunds = BigDecimal.ZERO;
        BigDecimal openingBalance = BigDecimal.ZERO;
        for (Balance balance : balances) {
            for (BalancesList balancesList : balance.getBalancesList()) {
                lastOpeningDateField.setValue(balance.getStatementDate());
                totalExludedAmount = totalExludedAmount.add(balancesList.getExludedAmount() != null ? balancesList.getExludedAmount() : BigDecimal.ZERO);
                totalUnkownFunds = totalUnkownFunds.add(balancesList.getUnkownFunds() != null ? balancesList.getUnkownFunds() : BigDecimal.ZERO);
            }
            openingBalance = balance.getBalance();
        }
        openingBalanceField.setValue(openingBalance);
        totalExcludeAmountField.setValue(totalExludedAmount);
        totalUnkownFundsField.setValue(totalUnkownFunds);
        account.setValue(crossAccount.getName());
        lastOpeningDateField.setValue(lastOpeningBalanceDate);
        List<TreasuryFrontOffice> treasuryFrontOfficeList = new ArrayList<>();
        LocalDate lastStatmentDate = null;
        if (lastOpeningBalanceDate != null) {
            for (Balance balance : balances) {
                lastStatmentDate = balance.getStatementDate().plusDays(1);
            }
            List<Transaction> transactionList = dataManager.load(Transaction.class)
                    .query("select e from treasury_Transaction e where e.account =:account and e.valueDate =:valueDate  and (e.status = :stat1 or e.status = :stat2) ORDER BY e.valueDate ASC")
                    .parameter("account", crossAccount)
                    .parameter("valueDate", valueDate)
                    .parameter("stat1", CheckerStatus.VALIDATED.name())
                    .parameter("stat2", Status.TEMPORARY.name())
                    .view("transaction-view")
                    .list();
            treasuryFrontOfficeList =
                    transactionList.stream().map(transaction -> mapToTreasuryFrontOffice(transaction))
                            .collect(Collectors.toList());
        }
        List<TreasuryFrontOffice> treasuryFrontOfficeList1 = (getTransactionWithoutValueDate().stream().map(transaction -> mapToTreasuryFrontOffice(transaction))
                .collect(Collectors.toList()));

        for (TreasuryFrontOffice treasuryFrontOffice : treasuryFrontOfficeList1) {
            treasuryFrontOfficeList.add(treasuryFrontOffice);
        }
        /////////////////////////////////////////////////////////////////////////

        TreasuryFrontOffice remTransactionTotreasuryFrontOffice = dataManager.create(TreasuryFrontOffice.class);

        TreasuryFrontOffice dealsOfTreasuryFrontOffice = dataManager.create(TreasuryFrontOffice.class);

        List treasuryFrontOfficeDealsList = dealsService.getDeals(crossAccount, valueDate);
        System.out.println("Deals count = " + treasuryFrontOfficeDealsList.size());
///////////////// e.AMOUNT , e.CURRENCY_CODE , e.VALUE_DATE
        for (Object o : treasuryFrontOfficeDealsList) {
            //test  20-06-2021/*****************************************************************************************************
            Object[] raw = ((Object[]) o);
            TreasuryFrontOffice treasuryFrontOffice = dataManager.create(TreasuryFrontOffice.class);
            Timestamp timestamp = (Timestamp) raw[2];
            treasuryFrontOffice.setCreateTs(timestamp);
            treasuryFrontOffice.setValueDate(timestamp.toLocalDateTime().toLocalDate());
            treasuryFrontOffice.setDepartmentName("Deals in Kondor");
            treasuryFrontOffice.setState("VALIDATED");
            treasuryFrontOffice.setDescription((String) raw[3]);
            treasuryFrontOffice.setDealId((String) raw[4]);
            treasuryFrontOffice.setRate((BigDecimal) raw[6]);
            Currency currency = dataManager.create(Currency.class);
            currency.setCode((String) raw[1]);
            treasuryFrontOffice.setCurrency(currency);
            BigDecimal amount = ((BigDecimal) raw[7] != null ? (BigDecimal) raw[7] : BigDecimal.ZERO).subtract((BigDecimal) raw[8] != null ? (BigDecimal) raw[8] : BigDecimal.ZERO);
            treasuryFrontOffice.setCredit((BigDecimal) raw[7] != null ? (BigDecimal) raw[7] : BigDecimal.ZERO);
            treasuryFrontOffice.setDepit((BigDecimal) raw[8] != null ? (BigDecimal) raw[8] : BigDecimal.ZERO);
            treasuryFrontOffice.setFolderName((String) raw[9]);
            treasuryFrontOffice.setCounterPartName((String) raw[10]);
            treasuryFrontOfficeList.add(treasuryFrontOffice);
        }

        treasuryFrontOfficesDc.getMutableItems().clear();
        treasuryFrontOfficesDc.getMutableItems().addAll(treasuryFrontOfficeList);
        return treasuryFrontOfficeList;
    }

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        loadTransaction();
    }

    public TreasuryFrontOffice mapToTreasuryFrontOffice(Transaction transaction) {
        TreasuryFrontOffice treasuryFrontOffice = dataManager.create(TreasuryFrontOffice.class);
        BigDecimal totalDebit = BigDecimal.ZERO;
        BigDecimal totalCredit = BigDecimal.ZERO;
        treasuryFrontOffice.setValueDate(transaction.getValueDate());
        if (transaction.getValueDate() == null)
            treasuryFrontOffice.setDescription("Transaction without value date");
        treasuryFrontOffice.setCredit((transaction.getCredit() != null ? transaction.getCredit() : BigDecimal.ZERO).add(totalCredit));
        treasuryFrontOffice.setDepit((transaction.getDept() != null ? transaction.getDept() : BigDecimal.ZERO).add(totalDebit));
        treasuryFrontOffice.setDepartmentName(transaction.getGroupName());
        treasuryFrontOffice.setState(transaction.getStatus());
        treasuryFrontOffice.setCurrency(dataManager.create(Currency.class));
        treasuryFrontOffice.getCurrency().setCode(transaction.getAccount().getCurrency().getCode());
        treasuryFrontOffice.setCreateTs(transaction.getCreateTs());
        if (transaction.getValueDate() != null) {
            treasuryFrontOffice.setValueDate(transaction.getValueDate());
        }
        List<Balance> balances = loadOpeningBalancesList(transaction.getAccount(), transaction.getValueDate());
        BigDecimal totalExludedAmount = BigDecimal.ZERO;
        BigDecimal totalUnkownFunds = BigDecimal.ZERO;
        BigDecimal openingBalance = BigDecimal.ZERO;
        for (Balance balance : balances) {
            for (BalancesList balancesList : balance.getBalancesList()) {
                totalExludedAmount = totalExludedAmount.add(balancesList.getExludedAmount() != null ? balancesList.getExludedAmount() : BigDecimal.ZERO);
                totalUnkownFunds = totalUnkownFunds.add(balancesList.getUnkownFunds() != null ? balancesList.getUnkownFunds() : BigDecimal.ZERO);
            }
            openingBalance = balance.getBalance();
        }
//        openingBalanceField.setValue(openingBalance);
//        totalExcludeAmountField.setValue(totalExludedAmount);
//        totalUnkownFundsField.setValue(totalUnkownFunds);

        return treasuryFrontOffice;
    }

    public List<Balance> loadOpeningBalancesList(CrossAccount crossAccount, LocalDate valueDate) {
        List<Balance> balanceList = new ArrayList<>();
        if (crossAccount != null && valueDate != null) {
            balanceList = dataManager.load(Balance.class)
                    .query("select e from treasury_Balance e where e.crossAccount =:crossAccount and e.creationDate =:date and e.status = :state")
                    .parameter("crossAccount", crossAccount)
                    .parameter("date", valueDate)
                    .parameter("state", CheckerStatus.VALIDATED.name())
                    .view("balance-view")
                    .list();
        }
        return balanceList;
    }

}

