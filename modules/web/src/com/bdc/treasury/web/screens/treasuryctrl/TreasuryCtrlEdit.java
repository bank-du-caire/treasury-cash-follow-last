package com.bdc.treasury.web.screens.treasuryctrl;

import com.haulmont.cuba.gui.screen.*;
import com.bdc.treasury.entity.TreasuryCtrl;

@UiController("treasury_TreasuryCtrl.edit")
@UiDescriptor("treasury-ctrl-edit.xml")
@EditedEntityContainer("treasuryCtrlDc")
@LoadDataBeforeShow
public class TreasuryCtrlEdit extends StandardEditor<TreasuryCtrl> {
}