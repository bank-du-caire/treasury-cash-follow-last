package com.bdc.treasury.web.screens.transactionstransfered;

import com.bdc.treasury.entity.*;
import com.bdc.treasury.service.TransactionService;
import com.bdc.treasury.service.TransactionsTransferedService;
import com.bdc.treasury.service.TreasuryFrontOfficeService;
import com.bdc.treasury.service.UserService;
import com.bdc.treasury.web.screens.balance.BalanceBrowse;
import com.haulmont.cuba.core.global.CommitContext;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.ScreenBuilders;
import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.entity.Group;
import com.haulmont.cuba.security.global.UserSession;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@UiController("treasury_TransactionsTransfered.edit")
@UiDescriptor("transactions-transfered-edit.xml")
@EditedEntityContainer("transactionsTransferedDc")
@LoadDataBeforeShow
public class TransactionsTransferedEdit extends StandardEditor<TransactionsTransfered> {
    @Inject
    private LookupField<Currency> fromAccountCurrencyField;
    @Inject
    private DataManager dataManager;
    @Inject
    private CollectionContainer<CrossAccount> fromCrossAccountsDc;
    @Inject
    private LookupField<CrossAccount> fromCrossAccountField;
    @Inject
    private Metadata metadata;

    @Inject
    private DateField<LocalDate> valueDateField;
    @Inject
    private TextField<BigDecimal> transferedBalanceField;
    @Inject
    private LookupField<CrossAccount> toCrossAccountField;
    @Inject
    private Notifications notifications;


    @Inject
    private UserSession userSession;
    @Inject
    private UserService userService;
    @Inject
    TransactionsTransferedService transactionsTransferedService;
    @Inject
    private InstanceContainer<TransactionsTransfered> transactionsTransferedDc;

    public List<Transaction> transactionList = new ArrayList<>();
    @Inject
    private CollectionContainer<Transaction> transactionDc;

    @Inject
    TransactionService transactionService;

    public String status = " ";
    public String globalStatus = "";
    @Inject
    private CollectionContainer<CrossAccount> toCrossAccountsDc;

    private final static String CONTROL_maker_RULE = "control-maker";
    private final static String CONTROL_checker_RULE = "control-checker";
    @Inject
    private LookupField<String> makerStatus;

    public String statusCheck = "";
    @Inject
    private TreasuryFrontOfficeService treasuryFrontOfficeService;

    String currency1 = "";
    String currency2 = "";
    @Inject
    private ScreenValidation screenValidation;
    @Inject
    private Form form;
    @Inject
    private Screens screens;
    @Inject
    private ScreenBuilders screenBuilders;

    @Subscribe
    public void onInit(InitEvent event) {
        List<CrossAccount> accountList = new ArrayList<>();
        fromCrossAccountField.setOptionsList(accountList);
        toCrossAccountField.setOptionsList(accountList);
        List<String> controlMakerList = new ArrayList<>();
        List<String> controlCheckerList = new ArrayList<>();

        controlMakerList.add(Status.SAVED.name());
        controlMakerList.add(Status.SUBMITTED.name());
        controlCheckerList.add(CheckerStatus.VALIDATED.name());
        controlCheckerList.add(CheckerStatus.RETURNED.name());

        if (userService.getUserRoles().contains(CONTROL_maker_RULE)) {
            makerStatus.setOptionsList(controlMakerList);
        } else if (userService.getUserRoles().contains(CONTROL_checker_RULE)) {
            fromCrossAccountField.setEditable(false);
            toCrossAccountField.setEditable(false);
            fromAccountCurrencyField.setEditable(false);
            valueDateField.setEditable(false);
            makerStatus.setOptionsList(controlCheckerList);
            transferedBalanceField.setEditable(false);
        }
    }

    @Subscribe("fromCrossAccountField")
    public void onFromCrossAccountFieldValueChange(HasValue.ValueChangeEvent<CrossAccount> event) {
        if (fromCrossAccountField.getValue() != null && fromCrossAccountField.getValue().getCurrency() != null)
            currency1 = fromCrossAccountField.getValue().getCurrency().getCode();
    }

    @Subscribe("toCrossAccountField")
    public void onToCrossAccountFieldValueChange(HasValue.ValueChangeEvent<CrossAccount> event) {
        if (toCrossAccountField.getValue() != null && toCrossAccountField.getValue().getCurrency() != null)
            currency2 = toCrossAccountField.getValue().getCurrency().getCode();
    }

    @Subscribe
    public void onBeforeCommitChanges(BeforeCommitChangesEvent event) {

    }

/*    @Subscribe
    public void onAfterCommitChanges(AfterCommitChangesEvent event) {

    }*/

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {

        List<String> rolesNames = userService.getUserRoles();
        if (rolesNames.contains(CONTROL_maker_RULE) && transactionsTransferedDc.getItem() != null && transactionsTransferedDc.getItem().getStatus() != null && !transactionsTransferedDc.getItem().getStatus().equals("")) {
            statusCheck = transactionsTransferedDc.getItem().getStatus();
            if (statusCheck.equals("VALIDATED")) {
                makerStatus.setEditable(false);
                fromAccountCurrencyField.setEditable(false);
                fromCrossAccountField.setEditable(false);
                toCrossAccountField.setEditable(false);
                transferedBalanceField.setEditable(false);
                valueDateField.setEditable(false);


            }
        }

        // know status of transaction
        if (transactionsTransferedDc.getItem().getStatus() != null) {
            globalStatus = transactionsTransferedDc.getItem().getStatus();
        }
        TransactionsTransfered transactionsTransfered = (transactionsTransferedDc.getItem() != null ? transactionsTransferedDc.getItem() : null);
        if (transactionsTransfered != null) {
            transactionList = dataManager.load(Transaction.class)
                    .query("select e from treasury_Transaction e where e.transactionsTransfered =:transactionsTransfered")
                    .parameter("transactionsTransfered", transactionsTransfered)
                    .view("transaction-view")
                    .list();
        }
        transactionDc.getMutableItems().clear();
        transactionDc.getMutableItems().addAll(transactionList);
        if (transactionsTransferedDc.getItem().getStatus() != null && transactionsTransferedDc.getItem().getStatus().equals(CheckerStatus.VALIDATED.name())) {
            status = "edit";
        }
//        else {
//            status = "edit";
//        }
        /*if ( transactionsTransferedDc.getItem().getFromCrossAccount() != null
                && transactionsTransferedDc.getItem().getFromCrossAccount().getCurrency() != null) {
            transactionsTransferedDc.getItem().setFromAccountCurrency(transactionsTransferedDc.getItem().getFromCrossAccount().getCurrency());
        }*/
    }

    @Subscribe("fromAccountCurrencyField")
    public void onFromAccountCurrencyFieldValueChange(HasValue.ValueChangeEvent<Currency> event) {
        fromCrossAccountField.clear();
        toCrossAccountField.clear();
        if (fromAccountCurrencyField.getValue() != null && fromAccountCurrencyField.getValue().getName() != null) {
            List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
                    .query("select t from treasury_CrossAccount t where t.currency =:currency and t.status=:status")
                    .parameter("status", CheckerStatus.VALIDATED.name())
                    .parameter("currency", fromAccountCurrencyField.getValue())
                    .view("crossAccount-view")
                    .list();
            fromCrossAccountsDc.getMutableItems().clear();
            fromCrossAccountsDc.getMutableItems().addAll(crossAccountList);
            fromCrossAccountField.setOptionsList(crossAccountList);
            toCrossAccountsDc.getMutableItems().clear();
            toCrossAccountsDc.getMutableItems().addAll(crossAccountList);
            toCrossAccountField.setOptionsList(crossAccountList);


        }
    }


    Screen screen1;

    @Transactional
    @Subscribe("save")
    public void onSaveClick(Button.ClickEvent event) {
        boolean saveAllowed = true;
        LocalDate now = LocalDate.now();
        Group group = userSession.getUser().getGroup();
        ValidationErrors errors = screenValidation.validateUiComponents(form);
        if (!errors.isEmpty()) {
            screenValidation.showValidationErrors(this, errors);
            return;
        }
        if (fromCrossAccountField.getValue().equals(toCrossAccountField.getValue())) {
            notifications.create().withCaption("From Correspondent Account and To Correspondent Account are identical").show();
            saveAllowed = false;
        }
        // edited ************************************************************************************************
       /* if (userService.getUserRoles().contains(CONTROL_checker_RULE)
                && makerStatus.getValue().equals(CheckerStatus.RETURNED.name())
                && globalStatus.equals(CheckerStatus.VALIDATED.name())) {
            notifications.create().withCaption("Not Allowed to return validated Status").show();
            saveAllowed = false;
        }*/
        /*BigDecimal netBalance = treasuryFrontOfficeService.getClosedBalanceForTreasuryCtrl(fromCrossAccountField.getValue(), valueDateField.getValue());
        if (transferedBalanceField.getValue().compareTo(netBalance) > 0) {
            notifications.create().withCaption("The Transferred Balance =("+transferedBalanceField.getValue()+") is bigger than net balance =("+netBalance+") of From Cross account = ' "+fromCrossAccountField.getValue().getName()+" '").show();
            event.preventCommit();
        }*/
        if (!currency1.equals(currency2)) {
            notifications.create().withCaption("The currency of from account not equal  The currency of to account").show();
            saveAllowed = false;
        }
        if (fromAccountCurrencyField.getValue() != null && fromCrossAccountField.getValue() != null
                && !transactionService.checkCurrencyOfAccount(fromCrossAccountField.getValue(), fromAccountCurrencyField.getValue())) {
            saveAllowed = false;
            notifications.create().withCaption("The Currency not equal selected accounts' currency").show();
        }
        if (fromAccountCurrencyField.getValue() != null && toCrossAccountField.getValue() != null
                && !transactionService.checkCurrencyOfAccount(toCrossAccountField.getValue(), fromAccountCurrencyField.getValue())) {
            saveAllowed = false;
            notifications.create().withCaption("The Currency not equal selected accounts' currency").show();
        }
        try {
            if (saveAllowed) {
                dataManager.commit(transactionsTransferedDc.getItem());
                TransactionsTransferedBrowse screen = screens.create(TransactionsTransferedBrowse.class, OpenMode.NEW_TAB);
                screens.removeAll();
                screens.show(screen);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (userService.getUserRoles().contains(CONTROL_checker_RULE) && transactionsTransferedDc.getItem().getStatus() != null
                && transactionsTransferedDc.getItem().getStatus().equals(CheckerStatus.VALIDATED.name())
                && !status.equals("edit")) {
            TransactionsTransfered transactionsTransfered = transactionsTransferedDc.getItem();
            Transaction transaction1 = metadata.create(Transaction.class);
            Transaction transaction2 = metadata.create(Transaction.class);
//        if (status.equals("create")) {

            List<String> rolesNames = userService.getUserRoles();
            transaction1.setAccount(fromCrossAccountField.getValue());
            transaction1.setDept(transferedBalanceField.getValue());
            transaction1.setCredit(BigDecimal.ZERO);
            transaction1.setValueDate(valueDateField.getValue());
            transaction1.setStatus(CheckerStatus.VALIDATED.name());
            transaction1.setGroupName(group.getName());
            transaction1.setTransactionsTransfered(transactionsTransfered);
            transaction1.setCurrency(fromAccountCurrencyField.getValue());
            transaction1.setCreationDate(now);
            transaction1.setGroupID(group.getId());
            dataManager.commit(transaction1);
            //////////////////////
            transaction2.setAccount(toCrossAccountField.getValue());
            transaction2.setDept(BigDecimal.ZERO);
            transaction2.setCredit(transferedBalanceField.getValue());
            transaction2.setValueDate(valueDateField.getValue());
            transaction2.setStatus(CheckerStatus.VALIDATED.name());
            transaction2.setGroupName(group.getName());
            transaction2.setTransactionsTransfered(transactionsTransfered);
            transaction2.setCurrency(fromAccountCurrencyField.getValue());
            transaction2.setCreationDate(now);
            transaction2.setGroupID(group.getId());
            dataManager.commit(transaction2);
        } else if (status.equals("edit")) {
            for (Transaction transaction : transactionList) {
                if (transaction.getDept() != null && transaction.getDept().compareTo(BigDecimal.ZERO) != 0) {
                    transaction.setDept(transferedBalanceField.getValue());
                } else if (transaction.getCredit() != null && transaction.getCredit().compareTo(BigDecimal.ZERO) != 0) {
                    transaction.setCredit(transferedBalanceField.getValue());
                }
                transactionService.updateTransaction(transaction);
            }
        }
        if (transactionsTransferedDc.getItem().getStatus() != null
                && transactionsTransferedDc.getItem().getStatus().equals(CheckerStatus.RETURNED.name())
                && transactionList != null && transactionList.size() > 0) {
            for (Transaction transaction : transactionList) {
                transactionService.deleteTransaction(transaction);
            }
        }
    }
}