package com.bdc.treasury.web.screens.transaction;

import com.bdc.treasury.entity.*;
import com.bdc.treasury.service.*;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.actions.list.CreateAction;
import com.haulmont.cuba.gui.actions.list.EditAction;
import com.haulmont.cuba.gui.actions.list.RefreshAction;
import com.haulmont.cuba.gui.actions.list.RemoveAction;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.gui.screen.LookupComponent;
import com.haulmont.cuba.security.entity.Group;
import com.haulmont.cuba.security.global.UserSession;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@UiController("treasury_Transaction.browse")
@UiDescriptor("transaction-browse.xml")
@LookupComponent("transactionsTable")
@LoadDataBeforeShow
public class TransactionBrowse extends StandardLookup<Transaction> {


    private final static String CHECKER_RULE = "checker";
    private final static String MAKER_RULE = "maker";
//    private final static String CONTROL_RULE = "control";

    private final static String CONTROL_maker_RULE = "control-maker";
    private final static String CONTROL_checker_RULE = "control-checker";
    private final static String FRONT_MAKER_RULE = "front-office-maker";
    private final static String FRONT_CHECKER_RULE = "front-office-checker";
    private final static String INCOMING_RULE = "incoming";
    private final static String Checks_Group_Name = "FCY Checks Collection";
    @Inject
    private DataManager dataManager;

    @Inject
    private UserSession userSession;

    @Inject
    private CollectionContainer<Transaction> transactionsDc;

    @Inject
    private CollectionContainer<Transaction> transactionsDc1;

    @Named("transactionsTable.edit")
    private EditAction<Transaction> transactionsTableEdit;

    @Named("transactionsTable.create")
    private CreateAction<Transaction> transactionsTableCreate;

    @Named("transactionsTable.refresh")
    private RefreshAction transactionsTableRefresh;

    @Named("reportTransactionsTable.refresh")
    private RefreshAction reportTransactionsTableRefresh;

    @Named("validatedTransactionsTable.refresh")
    private RefreshAction validatedTransactionsTableRefresh;
    @Inject
    private UserService userService;

    @Inject
    private LookupField<CrossAccount> crossAccount;

    @Inject
    private LookupField<CrossAccount> validatedCrossAccount;

    @Inject
    private TransactionService transactionService;

    public List<Transaction> transactionFilteredPendingList = new ArrayList<>();
    public List<Transaction> transactionFilteredValidatedList = new ArrayList<>();
    public List<Transaction> transactionFilteredReportedList = new ArrayList<>();
    @Inject
    private DateField<LocalDate> valueDateTransaction;
    @Inject
    private LookupField<Group> groupTransction;
    @Inject
    private TextField<String> ReferenceNumberTransaction;
    @Inject
    private TextField<String> ReferenceNumberValidated;
    @Inject
    private LookupField<Group> groupValidated;
    @Inject
    private DateField<LocalDate> valueDateValidated;
    @Inject
    private GroupTable<Transaction> transactionsTable;
    @Inject
    private Notifications notifications;
    @Inject
    private GroupTable<Transaction> validatedTransactionsTable;
    @Inject
    private DateField<LocalDate> creationDateTransaction;
    @Inject
    private DateField<LocalDate> creationDateValidated;
    @Inject
    private CollectionLoader<Transaction> transactionsDl;
    @Inject
    private CollectionLoader<Transaction> transactionsDl1;
    @Inject
    private Logger log;
    LoadContext loadContext1;

    @Inject
    private TextField<BigDecimal> totalCredit;
    @Inject
    private TextField<BigDecimal> totalDebit;
    @Inject
    private TextField<BigDecimal> netBalance;
    @Inject
    private CollectionContainer<CrossAccount> crossAccountsDc;
    @Inject
    private LookupField<Currency> currency;
    @Inject
    private LookupField<Currency> currencyValidated;
    @Inject
    private TextField<BigDecimal> totalDebitValidated;
    @Inject
    private TextField<BigDecimal> netBalanceValidated;
    @Inject
    private TextField<BigDecimal> totalCreditValidated;
    @Inject
    private ClosedAccountService closedAccountService;
    @Inject
    private CollectionLoader<Transaction> transactionsDl2;
    @Inject
    private GroupTable<Transaction> reportTransactionsTable;
    @Inject
    private DateField<LocalDate> fromValueDateValidated;
    @Inject
    private DateField<LocalDate> toValueDateValidated;
    @Inject
    private CollectionContainer<Transaction> transactionsDc2;
    @Inject
    private LookupField<CrossAccount> reportedCrossAccount;
    @Inject
    private LookupField<Currency> reportedCurrency;
    @Inject
    private TextField<String> reportedReferenceNumber;
    @Inject
    private LookupField<Group> reportedGroup;
    @Inject
    private DateField<LocalDate> reportedCreationDate;



    @Subscribe
    public void onInit(InitEvent event) {
        refreshDataContainers();
        transactionsDl.setMaxResults(10);
        transactionsDl1.setMaxResults(10);
        transactionsDl2.setMaxResults(10);
        transactionFilteredPendingList = loadPendingDataFilterd();
        transactionFilteredValidatedList = loadValidatedDataFiltered();
        transactionFilteredReportedList = loadValidatedDataFiltered();
        List<CrossAccount> accountList = new ArrayList<>();
        crossAccount.setOptionsList(accountList);
        validatedCrossAccount.setOptionsList(accountList);
        reportedCrossAccount.setOptionsList(accountList);

    }

    @Install(to = "transactionsDl2", target = Target.DATA_LOADER)
    private List<Transaction> transactionsDl2LoadDelegate(LoadContext<Transaction> loadContext) {
        loadContext1 = loadContext;
        transactionsDl.setMaxResults(10);
        transactionsDl1.setMaxResults(10);
        transactionsDl2.setMaxResults(10);
        return loadValidatedData(loadContext1);
    }

    @Install(to = "transactionsDl1", target = Target.DATA_LOADER)
    private List<Transaction> transactionsDl1LoadDelegate(LoadContext<Transaction> loadContext) {
        loadContext1 = loadContext;
        transactionsDl.setMaxResults(10);
        transactionsDl1.setMaxResults(10);
        return loadValidatedData(loadContext1);
    }

    @Install(to = "transactionsDl", target = Target.DATA_LOADER)
    private List<Transaction> transactionsDlLoadDelegate(LoadContext<Transaction> loadContext) {
        loadContext1 = loadContext;
        transactionsDl.setMaxResults(10);
        transactionsDl1.setMaxResults(10);
        return loadPendingData(loadContext1);
    }

    public void refreshDataContainers() {
        transactionsTableEdit.setAfterCloseHandler(transaction -> {
//            transactionsDc.getMutableItems().clear();
//            transactionsDc.getMutableItems().addAll(loadPendingData(loadContext1));
            transactionsTableRefresh.execute();
            reportTransactionsTableRefresh.execute();

//            transactionsDc1.getMutableItems().clear();
//            transactionsDc1.getMutableItems().addAll(loadValidatedData(loadContext1));
            validatedTransactionsTableRefresh.execute();
        });
        transactionsTableCreate.setAfterCloseHandler(transaction -> {
//            transactionsDc.getMutableItems().clear();
//            transactionsDc.getMutableItems().addAll(loadPendingData(loadContext1));
            transactionsTableRefresh.execute();
            reportTransactionsTableRefresh.execute();
//            transactionsDc1.getMutableItems().clear();
//            transactionsDc1.getMutableItems().addAll(loadValidatedData(loadContext1));
            validatedTransactionsTableRefresh.execute();
        });
    }


    @Subscribe("reportTransactionsTable.clearFilter")
    public void onReportTransactionsTableClearFilter(Action.ActionPerformedEvent event) {
        fromValueDateValidated.setValue(null);
        toValueDateValidated.setValue(null);
        loadValidatedDataFiltered();
        reportTransactionsTableRefresh.execute();
    }


    public List<Transaction> loadValidatedDataForReport(LocalDate start, LocalDate end) {
        List<String> rolesNames = userService.getUserRoles();
        if (rolesNames.contains(CONTROL_maker_RULE) || rolesNames.contains(CONTROL_checker_RULE)
                || rolesNames.contains(FRONT_MAKER_RULE) || rolesNames.contains(FRONT_CHECKER_RULE)) {
            return dataManager.load(Transaction.class)
                    .query("select t from treasury_Transaction t where (t.status =:stat1) and t.valueDate between :start and :end")
                    .parameter("stat1", CheckerStatus.VALIDATED.name())
                    .parameter("start", start)
                    .parameter("end", end)
                    .view("transaction-view")
                    .list();
        } else {
            return dataManager.load(Transaction.class)
                    .query("select t from treasury_Transaction t where (t.status =:stat1) and t.groupID =:groupId and t.valueDate between :start and :end")
                    .parameter("stat1", CheckerStatus.VALIDATED.name())
                    .parameter("groupId", userSession.getUser().getGroup().getId())
                    .parameter("start", start)
                    .parameter("end", end)
                    .view("transaction-view")
                    .list();
        }

    }


    public List<Transaction> loadValidatedData(LoadContext loadContext) {
        transactionsDl.setMaxResults(10);
        transactionsDl1.setMaxResults(10);
        List<String> rolesNames = userService.getUserRoles();
        if (rolesNames.contains(CONTROL_maker_RULE) || rolesNames.contains(CONTROL_checker_RULE)
                || rolesNames.contains(FRONT_MAKER_RULE) || rolesNames.contains(FRONT_CHECKER_RULE)) {
            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString("select t from treasury_Transaction t where (t.status =:stat1)")
                    .setParameter("stat1", CheckerStatus.VALIDATED.name());
            loadContext.setView("transaction-view");
            return dataManager.loadList(loadContext);
        } else {
            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString("select t from treasury_Transaction t where (t.status =:stat1) and t.groupID =:groupId")
                    .setParameter("stat1", CheckerStatus.VALIDATED.name())
                    .setParameter("groupId", userSession.getUser().getGroup().getId());
            loadContext.setView("transaction-view");
            return dataManager.loadList(loadContext);
        }
    }

    public List<Transaction> loadValidatedDataFiltered() {
        List<String> rolesNames = userService.getUserRoles();
        if (rolesNames.contains(CONTROL_maker_RULE) || rolesNames.contains(CONTROL_checker_RULE)
                || rolesNames.contains(FRONT_MAKER_RULE) || rolesNames.contains(FRONT_CHECKER_RULE)) {
            return dataManager.load(Transaction.class)
                    .query("select t from treasury_Transaction t where (t.status =:stat1) ")
                    .parameter("stat1", CheckerStatus.VALIDATED.name())
                    .view("transaction-view")
                    .list();
        } else {
            return dataManager.load(Transaction.class)
                    .query("select t from treasury_Transaction t where (t.status =:stat1) and t.groupID =:groupId")
                    .parameter("stat1", CheckerStatus.VALIDATED.name())
                    .parameter("groupId", userSession.getUser().getGroup().getId())
                    .view("transaction-view")
                    .list();
        }

    }

    public List<Transaction> loadPendingData(LoadContext loadContext) {
        List<String> rolesNames = userService.getUserRoles();
        List<Transaction> transactionList = new ArrayList<>();
        transactionsDl.setMaxResults(10);
        transactionsDl1.setMaxResults(10);
        if (rolesNames.contains(CHECKER_RULE)) {
            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString("select t from treasury_Transaction t where ((t.status =:stat1 or t.status =:stat2 ) and t.groupID =:groupId) or ( t.status =:stat4 and t.groupID =:groupId )")
                    .setParameter("stat1", Status.SUBMITTED.name())
                    .setParameter("stat2", Status.TEMPORARY.name())
                    .setParameter("stat4",CheckerStatus.EXCEPTION.name())
                    .setParameter("groupId", userSession.getUser().getGroup().getId());
            loadContext.setView("transaction-view");
            transactionList = dataManager.loadList(loadContext);
            transactionList = transactionList.stream()
                    .filter(transaction -> {
                        if (transaction.getStatus().equals(Status.TEMPORARY.name())) {
                            return transactionService.transactionIsExpired(transaction);
                        }
                        return true;
                    }).collect(Collectors.toList());

        } else if (rolesNames.contains(MAKER_RULE)) {
            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString("select t from treasury_Transaction t where (t.status =:stat1 or t.status =:stat2 or t.status =:stat3) and t.groupID =:groupId")
                    .setParameter("stat1", Status.SUBMITTED.name())
                    .setParameter("stat2", CheckerStatus.RETURNED.name())
                    .setParameter("stat3", Status.SAVED.name())
                    .setParameter("groupId", userSession.getUser().getGroup().getId());
            loadContext.setView("transaction-view");
            transactionList = dataManager.loadList(loadContext);
            transactionList = transactionList.stream()
                    .filter(transaction -> {
                        if (transaction.getStatus().equals(Status.TEMPORARY.name())) {
                            return transactionService.transactionIsExpired(transaction);
                        }
                        return true;
                    }).collect(Collectors.toList());
        } else if (rolesNames.contains(CONTROL_maker_RULE)) {
            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString("select t from treasury_Transaction t where (t.status = :stat1  or t.status = :stat2 or t.status = :stat3 ) and t.groupID =:groupId ")
                    .setParameter("stat1", Status.SUBMITTED.name())
                    .setParameter("stat2", CheckerStatus.RETURNED.name())
                    .setParameter("stat3", Status.SAVED.name())
                    .setParameter("groupId", userSession.getUser().getGroup().getId());
            loadContext.setView("transaction-view");
            transactionList = dataManager.loadList(loadContext);
        }
        if (rolesNames.contains(CONTROL_checker_RULE)) {
            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString("select t from treasury_Transaction t where (t.status = :stat1  or t.status = :stat2 or t.status=:stat3 ) and t.groupID =:groupId ")
                    .setParameter("stat1", Status.SUBMITTED.name())
                    .setParameter("stat2", Status.TEMPORARY.name())
                    .setParameter("groupId", userSession.getUser().getGroup().getId())
                    .setParameter("stat3", CheckerStatus.VALIDATED.name());
            loadContext.setView("transaction-view");
            transactionList = dataManager.loadList(loadContext);
        }
        if (rolesNames.contains(FRONT_MAKER_RULE)) {
            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString("select t from treasury_Transaction t where (t.status = :stat2 or t.status=:stat3 or t.status=:stat4) and t.groupID =:groupId ")
                    .setParameter("stat2", Status.SAVED.name())
                    .setParameter("stat3", Status.SUBMITTED.name())
                    .setParameter("stat4", CheckerStatus.RETURNED.name())
                    .setParameter("groupId", userSession.getUser().getGroup().getId());
            loadContext.setView("transaction-view");
            transactionList = dataManager.loadList(loadContext);
        }
        if (rolesNames.contains(FRONT_CHECKER_RULE)) {
            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString("select t from treasury_Transaction t where (t.status=:stat3 and t.groupID =:groupId) or t.status = :stat2 ")
                    .setParameter("stat2", CheckerStatus.EXCEPTION.name())
                    .setParameter("stat3", Status.SUBMITTED.name())
                    .setParameter("groupId", userSession.getUser().getGroup().getId());
            loadContext.setView("transaction-view");
            transactionList = dataManager.loadList(loadContext);
        }
        transactionsDl.setMaxResults(10);
        transactionsDl1.setMaxResults(10);
        return transactionList;
    }

    public List<Transaction> loadPendingDataFilterd() {
        List<String> rolesNames = userService.getUserRoles();
        List<Transaction> transactionList = new ArrayList<>();
        if (rolesNames.contains(CHECKER_RULE)) {
            transactionList = dataManager.load(Transaction.class)
                    .query("select t from treasury_Transaction t where ((t.status =:stat1 or t.status =:stat2 ) and t.groupID =:groupId) or ( t.status =:stat4 and t.groupID =:groupId )")
                    .parameter("stat1", Status.SUBMITTED.name())
                    .parameter("stat2", Status.TEMPORARY.name())
                    .parameter("stat4",CheckerStatus.EXCEPTION.name())
                    .parameter("groupId", userSession.getUser().getGroup().getId())
                    .view("transaction-view")
                    .list();
            transactionList = transactionList.stream()
                    .filter(transaction -> {
                        if (transaction.getStatus().equals(Status.TEMPORARY.name())) {
                            return transactionService.transactionIsExpired(transaction);
                        }
                        return true;
                    }).collect(Collectors.toList());

        } else if (rolesNames.contains(MAKER_RULE)) {
            transactionList = dataManager.load(Transaction.class)
                    .query("select t from treasury_Transaction t where (t.status =:stat1 or t.status =:stat2 or t.status =:stat3) and t.groupID =:groupId ")
                    .parameter("stat1", Status.SUBMITTED.name())
                    .parameter("stat2", CheckerStatus.RETURNED.name())
                    .parameter("stat3", Status.SAVED.name())
                    .parameter("groupId", userSession.getUser().getGroup().getId())
                    .view("transaction-view")
                    .list();
            transactionList = transactionList.stream()
                    .filter(transaction -> {
                        if (transaction.getStatus().equals(Status.TEMPORARY.name())) {
                            return transactionService.transactionIsExpired(transaction);
                        }
                        return true;
                    }).collect(Collectors.toList());
        }
        if (rolesNames.contains(CONTROL_maker_RULE)) {
            transactionList = dataManager.load(Transaction.class)
                    .query("select t from treasury_Transaction t where (t.status = :stat1  or t.status = :stat2 or t.status = :stat3 ) and t.groupID =:groupId ")
                    .parameter("stat1", Status.SUBMITTED.name())
                    .parameter("stat2", CheckerStatus.RETURNED.name())
                    .parameter("stat3", Status.SAVED.name())
                    .parameter("groupId", userSession.getUser().getGroup().getId())
                    .view("transaction-view")
                    .list();
        }
        if (rolesNames.contains(CONTROL_checker_RULE)) {
            transactionList = dataManager.load(Transaction.class)
                    .query("select t from treasury_Transaction t where (t.status = :stat1  or t.status = :stat2 or t.status=:stat3 ) and t.groupID =:groupId")
                    .parameter("stat1", Status.SUBMITTED.name())
                    .parameter("stat2", Status.TEMPORARY.name())
                    .parameter("groupId", userSession.getUser().getGroup().getId())
                    .parameter("stat3", CheckerStatus.VALIDATED.name())
                    .view("transaction-view")
                    .list();
        }
        if (rolesNames.contains(FRONT_MAKER_RULE)) {
            transactionList = dataManager.load(Transaction.class)
                    .query("select t from treasury_Transaction t where  (t.status = :stat2 or t.status=:stat3 or t.status=:stat4)  and t.groupID =:groupId ")
                    .parameter("stat2", Status.SAVED.name())
                    .parameter("stat3", Status.SUBMITTED.name())
                    .parameter("stat4", CheckerStatus.RETURNED.name())
                    .parameter("groupId", userSession.getUser().getGroup().getId())
                    .view("transaction-view")
                    .list();
        }
        if (rolesNames.contains(FRONT_CHECKER_RULE)) {
            transactionList = dataManager.load(Transaction.class)
                    .query("select t from treasury_Transaction t where  (t.status=:stat3 and t.groupID =:groupId) or t.status = :stat2 ")
                    .parameter("stat2", CheckerStatus.EXCEPTION.name())
                    .parameter("stat3", Status.SUBMITTED.name())
                    .parameter("groupId", userSession.getUser().getGroup().getId())
                    .view("transaction-view")
                    .list();
        }

        return transactionList;
    }

    @Subscribe("crossAccount")
    public void onCrossAccountValueChange(HasValue.ValueChangeEvent<CrossAccount> event) {
//        transactionFilteredPendingList = loadPendingDataFilterd();
        transactionFilteredPendingList = transactionFilteredPendingList.stream().filter(transaction1 -> {
            if (crossAccount.getValue() != null && crossAccount.getValue().getName() != null)
                return transaction1.getAccount().getName().equals(crossAccount.getValue().getName())
                        && transaction1.getAccount().getCurrency().equals(currency.getValue());
            return false;
        }).collect(Collectors.toList());
        transactionsDc.getMutableItems().clear();
        transactionsDc.getMutableItems().addAll(transactionFilteredPendingList);

        // total debit & Total Credit
        updateTotalCredit_Debit_net_Transaction(transactionFilteredPendingList);
    }

    public void updateTotalCredit_Debit_net_Transaction(List<Transaction> transactions) {
        if (crossAccount.getValue() != null && crossAccount.getValue().getName() != null
                && valueDateTransaction.getValue() != null) {
            TreasuryCtrl treasuryCtrl = dataManager.create(TreasuryCtrl.class);
//            treasuryCtrl = transactionService.mapToTreasuryCtrl(crossAccount.getValue(), valueDateTransaction.getValue());
            BigDecimal totalcredit = BigDecimal.ZERO;
            BigDecimal totaldebit = BigDecimal.ZERO;
            BigDecimal totalCreditMinusTotalDebit = BigDecimal.ZERO;
            for (Transaction transaction : transactions) {
                totalcredit = totalcredit.add(transaction.getCredit() != null ? transaction.getCredit() : BigDecimal.ZERO);
                totaldebit = totaldebit.add(transaction.getDept() != null ? transaction.getDept() : BigDecimal.ZERO);
            }
            totalCreditMinusTotalDebit = totalcredit.subtract(totaldebit);
            totalCredit.setValue(totalcredit);
            totalDebit.setValue(totaldebit);
            netBalance.setValue(totalCreditMinusTotalDebit);
        }
    }

    public void updateTotalCredit_Debit_net_Validated(List<Transaction> transactions) {
        if (validatedCrossAccount.getValue() != null && validatedCrossAccount.getValue().getName() != null
                && valueDateValidated.getValue() != null) {
//            TreasuryCtrl treasuryCtrl = dataManager.create(TreasuryCtrl.class);
//            treasuryCtrl = transactionService.mapToTreasuryCtrl(validatedCrossAccount.getValue(), valueDateValidated.getValue());
            BigDecimal totalcredit = BigDecimal.ZERO;
            BigDecimal totaldebit = BigDecimal.ZERO;
            BigDecimal totalCreditMinusTotalDebit = BigDecimal.ZERO;
            for (Transaction transaction : transactions) {
                totalcredit = totalcredit.add(transaction.getCredit() != null ? transaction.getCredit() : BigDecimal.ZERO);
                totaldebit = totaldebit.add(transaction.getDept() != null ? transaction.getDept() : BigDecimal.ZERO);
            }
            totalCreditMinusTotalDebit = totalcredit.subtract(totaldebit);
            totalCreditValidated.setValue(totalcredit);
            totalDebitValidated.setValue(totaldebit);
            netBalanceValidated.setValue(totalCreditMinusTotalDebit);
        }
    }


    @Subscribe("groupTransction")
    public void onGroupTransctionValueChange(HasValue.ValueChangeEvent<String> event) {
//        transactionFilteredPendingList = loadPendingDataFilterd();
        transactionFilteredPendingList = transactionFilteredPendingList.stream().filter(transaction1 -> {
            if (groupTransction.getValue() != null) {
                if (transaction1.getGroupName() != null)
                    return transaction1.getGroupName().equals(groupTransction.getValue().getName());
            }
            return false;
        }).collect(Collectors.toList());
        transactionsDc.getMutableItems().clear();
        transactionsDc.getMutableItems().addAll(transactionFilteredPendingList);
    }

    @Subscribe("ReferenceNumberTransaction")
    public void onReferenceNumberTransactionEnterPress(TextInputField.EnterPressEvent event) {
//        transactionFilteredPendingList = loadPendingDataFilterd();
        transactionFilteredPendingList = transactionFilteredPendingList.stream().filter(transaction1 -> {
            if (ReferenceNumberTransaction.getValue() != null) {
                if (transaction1.getReferenceNumber() != null) {
                    return transaction1.getReferenceNumber().equals(ReferenceNumberTransaction.getValue());
                }
            }
            return false;
        }).collect(Collectors.toList());
        transactionsDc.getMutableItems().clear();
        transactionsDc.getMutableItems().addAll(transactionFilteredPendingList);
    }


    @Subscribe("valueDateTransaction")
    public void onValueDateTransactionValueChange(HasValue.ValueChangeEvent<Date> event) {
//        transactionFilteredPendingList = loadPendingDataFilterd();
        transactionFilteredPendingList = transactionFilteredPendingList.stream().filter(transaction1 -> {
            if (transaction1.getValueDate() != null && valueDateTransaction.getValue() != null)
                if (transaction1.getValueDate() != null)
                    return transaction1.getValueDate().equals(valueDateTransaction.getValue());
            return false;
        }).collect(Collectors.toList());
        transactionsDc.getMutableItems().clear();
        transactionsDc.getMutableItems().addAll(transactionFilteredPendingList);

        // total Debit && total credit && net
        updateTotalCredit_Debit_net_Transaction(transactionFilteredPendingList);
    }

    @Subscribe("creationDateTransaction")
    public void onCreationDateTransactionValueChange(HasValue.ValueChangeEvent<LocalDate> event) {
//        transactionFilteredPendingList = loadPendingDataFilterd();
        transactionFilteredPendingList = transactionFilteredPendingList.stream().filter(transaction1 -> {
            if (transaction1.getCreationDate() != null && creationDateTransaction.getValue() != null)
                if (transaction1.getCreationDate() != null)
                    return transaction1.getCreationDate().equals(creationDateTransaction.getValue());
            return false;
        }).collect(Collectors.toList());
        transactionsDc.getMutableItems().clear();
        transactionsDc.getMutableItems().addAll(transactionFilteredPendingList);
    }

    @Subscribe("creationDateValidated")
    public void onCreationDateValidatedValueChange(HasValue.ValueChangeEvent<LocalDate> event) {
//        transactionFilteredValidatedList = loadValidatedDataFiltered();
        transactionFilteredValidatedList = transactionFilteredValidatedList.stream().filter(transaction1 -> {
            if (creationDateValidated.getValue() != null)
                if (transaction1.getCreationDate() != null)
                    return transaction1.getCreationDate().equals(creationDateValidated.getValue());
            return false;
        }).collect(Collectors.toList());
        transactionsDc1.getMutableItems().clear();
        transactionsDc1.getMutableItems().addAll(transactionFilteredValidatedList);
    }


    //filter
    @Subscribe("validatedCrossAccount")
    public void onValidatedCrossAccountValueChange(HasValue.ValueChangeEvent<CrossAccount> event) {
//        transactionFilteredValidatedList = loadValidatedDataFiltered();
        transactionFilteredValidatedList = transactionFilteredValidatedList.stream().filter(transaction -> {
            if (validatedCrossAccount.getValue() != null && validatedCrossAccount.getValue().getName() != null)
                return transaction.getAccount().getName().equals(validatedCrossAccount.getValue().getName())
                        && transaction.getAccount().getCurrency().equals(currencyValidated.getValue());
            return false;
        }).collect(Collectors.toList());
        transactionsDc1.getMutableItems().clear();
        transactionsDc1.getMutableItems().addAll(transactionFilteredValidatedList);
        updateTotalCredit_Debit_net_Validated(transactionFilteredValidatedList);
    }

    @Subscribe("search")
    public void onSearchClick(Button.ClickEvent event) {
        LocalDate startDate = null;
        LocalDate endDate = null;
        if (fromValueDateValidated.getValue() != null && toValueDateValidated.getValue() != null) {
            startDate = fromValueDateValidated.getValue().minusDays(1);
            endDate = toValueDateValidated.getValue().plusDays(1);
            LocalDate finalStartDate = startDate;
            LocalDate finalEndDate = endDate;
            transactionFilteredReportedList = transactionFilteredReportedList.stream().filter(transaction1 -> {
                if (transaction1.getValueDate() != null) {
                    return (transaction1.getValueDate().isAfter(finalStartDate) && transaction1.getValueDate().isBefore(finalEndDate));
                }
                return false;
            }).collect(Collectors.toList());
            transactionsDc2.getMutableItems().clear();
            transactionsDc2.getMutableItems().addAll(transactionFilteredReportedList);
        } else {
            notifications.create().withCaption("Must select  start value date and end value date").show();
        }
    }

    @Subscribe("reportedCrossAccount")
    public void onReportedCrossAccountValueChange(HasValue.ValueChangeEvent<CrossAccount> event) {
        transactionFilteredReportedList = transactionFilteredReportedList.stream().filter(transaction -> {
            if (reportedCrossAccount.getValue() != null && reportedCrossAccount.getValue().getName() != null)
                return transaction.getAccount().getName().equals(reportedCrossAccount.getValue().getName())
                        && transaction.getAccount().getCurrency().equals(reportedCurrency.getValue());
            return false;
        }).collect(Collectors.toList());
        transactionsDc2.getMutableItems().clear();
        transactionsDc2.getMutableItems().addAll(transactionFilteredReportedList);
    }

    @Subscribe("reportedReferenceNumber")
    public void onReportedReferenceNumberValueChange(HasValue.ValueChangeEvent<String> event) {
        transactionFilteredReportedList = transactionFilteredReportedList.stream().filter(transaction1 -> {
            if (reportedReferenceNumber.getValue() != null) {
                if (transaction1.getReferenceNumber() != null) {
                    return transaction1.getReferenceNumber().equals(reportedReferenceNumber.getValue());
                }
            }
            return false;
        }).collect(Collectors.toList());
        transactionsDc2.getMutableItems().clear();
        transactionsDc2.getMutableItems().addAll(transactionFilteredReportedList);
    }

    @Subscribe("reportedGroup")
    public void onReportedGroupValueChange(HasValue.ValueChangeEvent<Group> event) {
        transactionFilteredReportedList = transactionFilteredReportedList.stream().filter(transaction1 -> {
            if (reportedGroup.getValue() != null) {
                if (transaction1.getGroupName() != null)
                    return transaction1.getGroupName().equals(reportedGroup.getValue().getName());
            }
            return false;
        }).collect(Collectors.toList());
        transactionsDc2.getMutableItems().clear();
        transactionsDc2.getMutableItems().addAll(transactionFilteredReportedList);
    }

    @Subscribe("reportedCreationDate")
    public void onReportedCreationDateValueChange(HasValue.ValueChangeEvent<LocalDate> event) {
        transactionFilteredReportedList = transactionFilteredReportedList.stream().filter(transaction1 -> {
            if (reportedCreationDate.getValue() != null)
                if (transaction1.getCreationDate() != null)
                    return transaction1.getCreationDate().equals(reportedCreationDate.getValue());
            return false;
        }).collect(Collectors.toList());
        transactionsDc2.getMutableItems().clear();
        transactionsDc2.getMutableItems().addAll(transactionFilteredReportedList);
    }

    @Subscribe("reportedCurrency")
    public void onReportedCurrencyValueChange(HasValue.ValueChangeEvent<Currency> event) {
        if (reportedCurrency.getValue() != null && reportedCurrency.getValue().getName() != null) {
            List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
                    .query("select t from treasury_CrossAccount t where t.currency =:currency and t.status=:status")
                    .parameter("status", CheckerStatus.VALIDATED.name())
                    .parameter("currency", reportedCurrency.getValue())
                    .view("crossAccount-view")
                    .list();
            crossAccountsDc.getMutableItems().clear();
            crossAccountsDc.getMutableItems().addAll(crossAccountList);
            reportedCrossAccount.setOptionsList(crossAccountList);
        } else {
            List<CrossAccount> crossAccountList = new ArrayList<>();
            reportedCrossAccount.setOptionsList(crossAccountList);
        }
        //////////////////////////

        transactionFilteredReportedList = transactionFilteredReportedList.stream().filter(transaction1 -> {
            if (reportedCurrency.getValue() != null && reportedCurrency.getValue().getName() != null)
                return transaction1.getAccount().getCurrency().getName().equals(reportedCurrency.getValue().getName());
            return false;
        }).collect(Collectors.toList());
        transactionsDc2.getMutableItems().clear();
        transactionsDc2.getMutableItems().addAll(transactionFilteredReportedList);
    }

    @Subscribe("reportTransactionsTable.clearFilter")
    public void onReportTransactionsTableClearFilter1(Action.ActionPerformedEvent event) {
        reportedCrossAccount.setValue(null);
        fromValueDateValidated.setValue(null);
        toValueDateValidated.setValue(null);
        reportedGroup.setValue(null);
        reportedReferenceNumber.setValue(null);
        reportedCreationDate.setValue(null);
        reportedCurrency.setValue(null);
        transactionFilteredReportedList = loadValidatedDataFiltered();
        transactionsTableRefresh.execute();
    }

    @Subscribe("transactionsTable.clearFilter")
    public void onCustomerBasesTableClearFilter(Action.ActionPerformedEvent event) {
        crossAccount.setValue(null);
        valueDateTransaction.setValue(null);
        groupTransction.setValue(null);
        ReferenceNumberTransaction.setValue(null);
        creationDateTransaction.setValue(null);
        currency.setValue(null);
        totalCredit.setValue(null);
        totalDebit.setValue(null);
        netBalance.setValue(null);
        transactionFilteredPendingList = loadPendingDataFilterd();
        transactionsTableRefresh.execute();
    }

    @Subscribe("validatedTransactionsTable.clearFilter")
    public void onValidatedTransactionsTableClearFilter(Action.ActionPerformedEvent event) {
        validatedCrossAccount.setValue(null);
        ReferenceNumberValidated.setValue(null);
        valueDateValidated.setValue(null);
        groupValidated.setValue(null);
        creationDateValidated.setValue(null);
        currencyValidated.setValue(null);
        totalCreditValidated.setValue(null);
        totalDebitValidated.setValue(null);
        netBalanceValidated.setValue(null);
        transactionFilteredValidatedList = loadValidatedDataFiltered();
        validatedTransactionsTableRefresh.execute();
    }

    @Subscribe("ReferenceNumberValidated")
    public void onReferenceNumberValidatedEnterPress(TextInputField.EnterPressEvent event) {
//        transactionFilteredValidatedList = loadValidatedDataFiltered();
        transactionFilteredValidatedList = transactionFilteredValidatedList.stream().filter(transaction1 -> {
            if (ReferenceNumberValidated.getValue() != null) {
                if (transaction1.getReferenceNumber() != null) {
                    return transaction1.getReferenceNumber().equals(ReferenceNumberValidated.getValue());
                }
            }
            return false;
        }).collect(Collectors.toList());
        transactionsDc1.getMutableItems().clear();
        transactionsDc1.getMutableItems().addAll(transactionFilteredValidatedList);
    }

    @Subscribe("groupValidated")
    public void onGroupValidatedValueChange(HasValue.ValueChangeEvent<Group> event) {
//        transactionFilteredValidatedList = loadValidatedDataFiltered();
        transactionFilteredValidatedList = transactionFilteredValidatedList.stream().filter(transaction1 -> {
            if (groupValidated.getValue() != null) {
                if (transaction1.getGroupName() != null)
                    return transaction1.getGroupName().equals(groupValidated.getValue().getName());
            }
            return false;
        }).collect(Collectors.toList());
        transactionsDc1.getMutableItems().clear();
        transactionsDc1.getMutableItems().addAll(transactionFilteredValidatedList);
    }

    @Subscribe("valueDateValidated")
    public void onValueDateValidatedValueChange(HasValue.ValueChangeEvent<LocalDate> event) {
//        transactionFilteredPendingList = loadValidatedDataFiltered();
        transactionFilteredValidatedList = transactionFilteredValidatedList.stream().filter(transaction1 -> {
            if (transaction1.getValueDate() != null && valueDateValidated.getValue() != null)
                return transaction1.getValueDate().equals(valueDateValidated.getValue());
            return false;
        }).collect(Collectors.toList());
        transactionsDc1.getMutableItems().clear();
        transactionsDc1.getMutableItems().addAll(transactionFilteredValidatedList);
        updateTotalCredit_Debit_net_Validated(transactionFilteredValidatedList);
    }

    /*@Subscribe("EXCEPTIONbtn")
    public void onEXCEPTIONbtnClick(Button.ClickEvent event) {
        List<String> rolesNames = userService.getUserRoles();
        if (transactionsTable.getSelected().size() > 0) {
            Set<Transaction> transactions = transactionsTable.getSelected();
            for (Transaction transaction : transactions) {
                if (transaction.getValueDate() != null && closedAccountService.checkOfClosedCrossAccount(transaction.getAccount(), transaction.getValueDate(), CloseAccountState.CLOSED.name())) {
                    notifications.create().withCaption("This account ( " + transaction.getAccount().getName() + " ) is closed with value date = " + transaction.getValueDate()).show();
                } else {
                    LocalDate now = LocalDate.now();
                    LocalDate tomorrow = now.plusDays(1);
                    if (rolesNames.contains(CHECKER_RULE)) {
                        if (transaction.getValueDate().equals(now)
                                || transaction.getValueDate().equals(tomorrow)) {
                            transaction.setStatus(CheckerStatus.EXCEPTION.name());
                            transactionsDc.getItem().setStatus(CheckerStatus.EXCEPTION.name());
                            dataManager.commit(transaction);
                        }
                    }
                    if (transaction.getValueDate().compareTo(tomorrow) > 0) {
                        notifications.create().withCaption("Not allowed to except this transaction").show();
                    }
                }
            }
            transactionsTableRefresh.execute();
            validatedTransactionsTableRefresh.execute();
        } else {
            message();
        }
    }*/

    @Subscribe("VALIDATEDbtn")
    public void onVALIDATEDbtnClick(Button.ClickEvent event) {
        Group group = userSession.getUser().getGroup();
        List<String> rolesNames = userService.getUserRoles();
        if (transactionsTable.getSelected().size() > 0) {
            Set<Transaction> transactions = transactionsTable.getSelected();
            for (Transaction transaction : transactions) {
                if (transaction.getValueDate() != null &&
                        closedAccountService.checkOfClosedCrossAccount(transaction.getAccount(), transaction.getValueDate(), CloseAccountState.CLOSED.name(),group)) {
                    notifications.create().withCaption("This account ( " + transaction.getAccount().getName() + " ) is closed with value date = " + transaction.getValueDate()).show();
                } else {
                    LocalDate now = LocalDate.now();
                    LocalDate tomorrow = now.plusDays(1);
                    if (rolesNames.contains(CHECKER_RULE) && !userService.getUserRoles().contains(INCOMING_RULE)
                            && !userSession.getUser().getGroup().getName().equals(Checks_Group_Name)) {
                        if (!transaction.getValueDate().equals(now)
                                && !transaction.getValueDate().equals(tomorrow)) {
                            transaction.setStatus(CheckerStatus.VALIDATED.name());
                            transactionsDc.getItem().setStatus(CheckerStatus.VALIDATED.name());
                            dataManager.commit(transaction);
                        } else {
                            notifications.create().withCaption("Not allowed to validate this transaction").show();
                        }
                    }
                    if (rolesNames.contains((CONTROL_checker_RULE)) ||
                            rolesNames.contains(FRONT_CHECKER_RULE) ||
                            userService.getUserRoles().contains(INCOMING_RULE) ||
                            userSession.getUser().getGroup().getName().equals(Checks_Group_Name)) {
                        transaction.setStatus(CheckerStatus.VALIDATED.name());
                        transactionsDc.getItem().setStatus(CheckerStatus.VALIDATED.name());
                        dataManager.commit(transaction);
                    }
                }
            }
            transactionsTableRefresh.execute();
            validatedTransactionsTableRefresh.execute();
        } else {
            message();
        }
    }

    @Subscribe("RETURNEDbtn")
    public void onRETURNEDbtnClick(Button.ClickEvent event) {
        Group group = userSession.getUser().getGroup();
        List<String> rolesNames = userService.getUserRoles();
        if (transactionsTable.getSelected().size() > 0) {
            Set<Transaction> transactions = transactionsTable.getSelected();
            for (Transaction transaction : transactions) {

                if (transaction.getValueDate() != null && closedAccountService.checkOfClosedCrossAccount(transaction.getAccount(), transaction.getValueDate(), CloseAccountState.CLOSED.name(),group)) {
                    notifications.create().withCaption("This account ( " + transaction.getAccount().getName() + " ) is closed with value date = " + transaction.getValueDate()).show();
                } else {
                    transaction.setStatus(CheckerStatus.RETURNED.name());
                    transactionsDc.getItem(transaction).setStatus(CheckerStatus.RETURNED.name());
                    dataManager.commit(transaction);
                }
            }
            transactionsTableRefresh.execute();
            validatedTransactionsTableRefresh.execute();
        } else {
            message();
        }
    }

    @Subscribe("RETURNEDValidatebtn")
    public void onRETURNEDValidatebtnClick(Button.ClickEvent event) {
        Group group = userSession.getUser().getGroup();
        List<String> rolesNames = userService.getUserRoles();
        if (validatedTransactionsTable.getSelected().size() > 0) {
            Set<Transaction> transactions = validatedTransactionsTable.getSelected();
            for (Transaction transaction : transactions) {
                if (transaction.getValueDate() != null && closedAccountService.checkOfClosedCrossAccount(transaction.getAccount(), transaction.getValueDate(), CloseAccountState.CLOSED.name(),group)) {
                    notifications.create().withCaption("This account ( " + transaction.getAccount().getName() + " ) is closed with value date = " + transaction.getValueDate()).show();
                } else {
                    if (!rolesNames.contains(CONTROL_checker_RULE) && transaction.getStatus().equals(CheckerStatus.VALIDATED.name())) {
                        notifications.create().withCaption("Not allowed to return validated Transaction").show();
                    } else {
                        transaction.setStatus(CheckerStatus.RETURNED.name());
                        transactionsDc1.getItem(transaction).setStatus(CheckerStatus.RETURNED.name());
                        dataManager.commit(transaction);
                    }
                }
            }
            transactionsTableRefresh.execute();
            validatedTransactionsTableRefresh.execute();
        } else {
            message();
        }
    }

    @Subscribe("SUBMITTEDbtn")
    public void onSUBMITTEDbtnClick(Button.ClickEvent event) {
        Group group = userSession.getUser().getGroup();
        if (transactionsTable.getSelected().size() > 0) {
            Set<Transaction> transactions = transactionsTable.getSelected();
            for (Transaction transaction : transactions) {
                if (!transaction.getStatus().equals(Status.SUBMITTED.name())) {
                    if (transaction.getValueDate() != null && closedAccountService.checkOfClosedCrossAccount(transaction.getAccount(), transaction.getValueDate(), CloseAccountState.CLOSED.name(),group)) {
                        notifications.create().withCaption("This account ( " + transaction.getAccount().getName() + " ) is closed with value date = " + transaction.getValueDate()).show();
                    } else {
                        transaction.setStatus(Status.SUBMITTED.name());
                        transactionsTable.setSelected(transaction);
                        transactionsDc.getItem(transaction).setStatus(Status.SUBMITTED.name());
                        dataManager.commit(transaction);
                    }
                }
            }
            transactionsTableRefresh.execute();
            validatedTransactionsTableRefresh.execute();
        } else {
            message();
        }
    }

    public void message() {
        notifications.create().withCaption("You should select one row at least").show();
    }

    @Named("transactionsTable.remove")
    private RemoveAction customersTableRemove;


    @Subscribe("transactionsTable.remove")
    public void onTransactionsTransferedsTableRemove(Action.ActionPerformedEvent event) {
        List<String> rolesNames = userService.getUserRoles();
        Transaction transaction = transactionsTable.getSingleSelected();
        if (rolesNames.contains(CONTROL_checker_RULE) && rolesNames.contains(CONTROL_maker_RULE) && transaction.getGroupName().equals("Treasury Control")) {
            customersTableRemove.execute();
        } else if (rolesNames.contains(CONTROL_checker_RULE) && rolesNames.contains(CONTROL_maker_RULE) && !transaction.getGroupName().equals("Treasury Control")) {
            notifications.create().withCaption("Not allowed to delete this transaction").show();
        } else if (transactionsTable.getSingleSelected() != null && !transaction.getStatus().equals(Status.SUBMITTED.name())) {
            customersTableRemove.execute();
        } else if (transactionsTable.getSingleSelected() != null && transaction.getStatus().equals(Status.SUBMITTED.name())) {
            notifications.create().withCaption("Not allowed to remove SUBMITTED transaction").show();

        } else {
            customersTableRemove.execute();
        }


    }

    @Subscribe("currency")
    public void onCurrencyValueChange(HasValue.ValueChangeEvent<Currency> event) throws ParseException {
        if (currency.getValue() != null && currency.getValue().getName() != null) {
            List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
                    .query("select t from treasury_CrossAccount t where t.currency =:currency and t.status=:status")
                    .parameter("status", CheckerStatus.VALIDATED.name())
                    .parameter("currency", currency.getValue())
                    .view("crossAccount-view")
                    .list();
            crossAccountsDc.getMutableItems().clear();
            crossAccountsDc.getMutableItems().addAll(crossAccountList);
            crossAccount.setOptionsList(crossAccountList);
        } else {
            List<CrossAccount> crossAccountList = new ArrayList<>();
            crossAccount.setOptionsList(crossAccountList);
        }
        //////////////////////////

        transactionFilteredPendingList = transactionFilteredPendingList.stream().filter(transaction1 -> {
            if (currency.getValue() != null && currency.getValue().getName() != null)
                return transaction1.getAccount().getCurrency().getName().equals(currency.getValue().getName());
            return false;
        }).collect(Collectors.toList());
        transactionsDc.getMutableItems().clear();
        transactionsDc.getMutableItems().addAll(transactionFilteredPendingList);
    }

    @Subscribe("currencyValidated")
    public void onCurrencyValidatedValueChange(HasValue.ValueChangeEvent<Currency> event) throws ParseException {
        if (currencyValidated.getValue() != null && currencyValidated.getValue().getName() != null) {
            List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
                    .query("select t from treasury_CrossAccount t where t.currency =:currency and t.status=:status")
                    .parameter("status", CheckerStatus.VALIDATED.name())
                    .parameter("currency", currencyValidated.getValue())
                    .view("crossAccount-view")
                    .list();
            crossAccountsDc.getMutableItems().clear();
            crossAccountsDc.getMutableItems().addAll(crossAccountList);
            validatedCrossAccount.setOptionsList(crossAccountList);
        } else {
            List<CrossAccount> crossAccountList = new ArrayList<>();
            validatedCrossAccount.setOptionsList(crossAccountList);
        }
        //////////////////////////

        transactionFilteredValidatedList = transactionFilteredValidatedList.stream().filter(transaction1 -> {
            if (currencyValidated.getValue() != null && currencyValidated.getValue().getName() != null)
                return transaction1.getAccount().getCurrency().getName().equals(currencyValidated.getValue().getName());
            return false;
        }).collect(Collectors.toList());
        transactionsDc1.getMutableItems().clear();
        transactionsDc1.getMutableItems().addAll(transactionFilteredValidatedList);
    }
}