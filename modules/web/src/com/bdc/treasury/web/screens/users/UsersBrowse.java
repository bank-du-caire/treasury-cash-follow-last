package com.bdc.treasury.web.screens.users;

import com.bdc.treasury.entity.Status;
import com.bdc.treasury.service.BaseService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.components.GroupTable;
import com.haulmont.cuba.gui.components.Table;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.treasury.entity.Users;
import com.haulmont.cuba.security.entity.User;
import com.haulmont.cuba.security.entity.UserRole;
import com.haulmont.cuba.security.global.UserSession;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@UiController("treasury_Users.browse")
@UiDescriptor("users-browse.xml")
@LookupComponent("usersesTable")
@LoadDataBeforeShow
public class UsersBrowse extends StandardLookup<Users> {
    @Inject
    private GroupTable<Users> usersesTable;
    @Inject
    private Button rejectBTn;
    @Inject
    private Button approveBtn;
    @Inject
    private Button removeBtn;
    @Inject
    private DataManager dataManager;
    @Inject
    private Logger log;
    @Inject
    private BaseService baseService;
    @Inject
    private Metadata metadata;
    @Inject
    private UserSession userSession;
    @Inject
    private CollectionContainer<Users> usersesDc;
    @Inject
    private Notifications notifications;
    @Inject
    private Button disableBtn;
    @Inject
    private Button enableBtn;
    @Inject
    private Screens screens;


    public void mapUserdata(User secUser, Users user) {

        List<com.bdc.treasury.entity.UserRole> userRolesList = baseService.loadUserRoles(user);
        List<com.haulmont.cuba.security.entity.UserRole> secUserRolesList = new ArrayList<com.haulmont.cuba.security.entity.UserRole>();
        switch (user.getStatus()) {
            case CREATE_PENDING:
                secUser.setId(user.getId());
                secUser.setLogin(user.getName());
                secUser.setGroup(user.getGroup());
                if (userRolesList != null) {
                    secUserRolesList = userRolesList.stream().map(userRole -> {
                        com.haulmont.cuba.security.entity.UserRole secUserRole = metadata.create(com.haulmont.cuba.security.entity.UserRole.class);
                        secUserRole.setRole(userRole.getRole());
                        secUserRole.setRoleName(userRole.getRoleName());
                        secUserRole.setUser(secUser);
                        return secUserRole;

                    }).collect(Collectors.toList());
                    baseService.addEditSecUser(secUser, secUserRolesList);
                    secUser.setUserRoles(secUserRolesList);
                }
                break;
            case UPDATE_PENDING:
                User secUser1 = new User();
                secUser1.setId(secUser.getId());

                secUser1.setLogin(user.getName());
                secUser1.setGroup(user.getGroup());
                if (userRolesList != null) {
                    log.info("awl el if");
                    secUserRolesList = userRolesList.stream().map(userRole -> {
                        com.haulmont.cuba.security.entity.UserRole secUserRole = metadata.create(com.haulmont.cuba.security.entity.UserRole.class);
                        secUserRole.setRole(userRole.getRole());
                        secUserRole.setRoleName(userRole.getRoleName());
                        secUserRole.setUser(secUser);
                        log.info("role" + userRole.getRoleName());
                        return secUserRole;
                    }).collect(Collectors.toList());
                    baseService.updateSecUserAndRoles(secUser1, secUserRolesList);
                }
                break;
        }

    }


    @Subscribe("approveBtn")
    public void onApproveBtnClick(Button.ClickEvent event) {
        if (usersesTable.getSingleSelected() != null) {
            if (userSession.getUser().getLogin().equals(usersesTable.getSingleSelected().getName())) {
                notifications.create().withCaption("You can't approve or reject an action on your user").show();
            } else {

                Users user = dataManager.load(Users.class).id(Objects.requireNonNull(usersesTable.getSingleSelected()).getUuid()).view("users-view").one();
                User secUserUpdate_delete;
                Status status = user.getStatus();
                switch (status) {
                    case CREATE_PENDING:
                        User secUser = dataManager.create(User.class);
                        mapUserdata(secUser, user);
                        try {
                            user.setStatus(Status.CREATE_APPROVED);
                            user.setActive(true);
                            dataManager.commit(user);
                            getScreenData().loadAll();
                        } catch (Exception e) {
                            log.error("Error", e);
                        }
                        break;
                    case UPDATE_PENDING:
                        try {
                            secUserUpdate_delete = dataManager.load(User.class).id(user.getId()).one();
                            mapUserdata(secUserUpdate_delete, user);
                            user.setStatus(Status.UPDATE_APPROVED);
                            dataManager.commit(user);
                            getScreenData().loadAll();
                        } catch (Exception e) {
                            log.error("Error", e);
                        }
                        break;

                    case DELETE_PENDING:
                        try {
                            secUserUpdate_delete = dataManager.load(User.class).id(user.getId()).one();
                            //secUserUpdate_delete = baseService.getSecUser(user.getId());
                            dataManager.remove(secUserUpdate_delete);
                            dataManager.remove(user);
                            getScreenData().loadAll();
                        } catch (Exception e) {
                            log.error("Error", e);
                        }
                        break;
                }
            }
        }
    }

    @Subscribe("rejectBTn")
    public void onRejectBTnClick(Button.ClickEvent event) {
        if (usersesTable.getSingleSelected() != null) {
            if (userSession.getUser().getLogin().equals(usersesTable.getSingleSelected().getName())) {
                notifications.create().withCaption("You can't approve or reject an action on your user").show();
            } else {

                if (usersesTable.getSingleSelected() != null) {
                    Users user = usersesTable.getSingleSelected();
                    if (user.getStatus() == Status.CREATE_PENDING) {
                        user.setStatus(Status.CREATE_REJECTED);
                    } else if (user.getStatus() == Status.UPDATE_PENDING) {
                        user.setStatus(Status.UPDATE_REJECTED);
                    } else if (user.getStatus() == Status.DELETE_PENDING) {
                        user.setStatus(Status.DELETE_REJECTED);
                    }
                    dataManager.commit(user);
                    getScreenData().loadAll();
                }
            }
        }
    }

    @Subscribe("removeBtn")
    public void onRemoveBtnClick(Button.ClickEvent event) {
        if (userSession.getUser().getLogin().equals(usersesTable.getSingleSelected().getName())) {
            notifications.create().withCaption("You can't remove your user").show();
        } else {
            if (Objects.requireNonNull(usersesTable.getSingleSelected()).getStatus() != Status.DELETE_PENDING) {
                Users user = usersesTable.getSingleSelected();
                user.setStatus(Status.DELETE_PENDING);
                dataManager.commit(user);
                getScreenData().loadAll();
            }
        }
    }


    @Subscribe("usersesTable")
    public void onUsersesTableSelection(Table.SelectionEvent<Users> event) {
        removeBtn.setEnabled(true);
        rejectBTn.setEnabled(true);
        approveBtn.setEnabled(true);
        if (usersesTable.getSingleSelected() != null && !usersesTable.getSingleSelected().getActive() &&
                !usersesTable.getSingleSelected().getStatus().equals(Status.CREATE_PENDING) &&
                !usersesTable.getSingleSelected().getStatus().equals(Status.CREATE_REJECTED)) {
            enableBtn.setEnabled(true);
        } else {
            enableBtn.setEnabled(false);
        }
        if (usersesTable.getSingleSelected() != null && usersesTable.getSingleSelected().getActive() &&
                !usersesTable.getSingleSelected().getStatus().equals(Status.CREATE_PENDING) &&
                !usersesTable.getSingleSelected().getStatus().equals(Status.CREATE_REJECTED)) {
            disableBtn.setEnabled(true);
        } else {
            disableBtn.setEnabled(false);
        }
    }

    @Subscribe("enableBtn")
    public void onEnableBtnClick(Button.ClickEvent event) {
        if (usersesTable.getSingleSelected() != null) {
            try {
                usersesTable.getSingleSelected().setActive(true);
                User secUserEnable = dataManager.load(User.class).id(usersesTable.getSingleSelected().getId()).one();
                secUserEnable.setActive(true);
                dataManager.commit(secUserEnable);
                dataManager.commit(usersesTable.getSingleSelected());
                getScreenData().loadAll();
                screens.removeAll();
                screens.create(UsersBrowse.class, OpenMode.THIS_TAB).show();
                notifications.create().withCaption(usersesTable.getSingleSelected().getName() + " user is enabled");
            } catch (Exception e) {
                log.error("Error", e);
            }
        }
    }

    @Subscribe("disableBtn")
    public void onDisableBtnClick(Button.ClickEvent event) {
        if (usersesTable.getSingleSelected() != null) {
            try {
                usersesTable.getSingleSelected().setActive(false);
                User secUserEnable = dataManager.load(User.class).id(usersesTable.getSingleSelected().getId()).one();
                secUserEnable.setActive(false);
                dataManager.commit(secUserEnable);
                dataManager.commit(usersesTable.getSingleSelected());
                getScreenData().loadAll();
                screens.removeAll();
                screens.create(UsersBrowse.class, OpenMode.THIS_TAB).show();
                notifications.create().withCaption(usersesTable.getSingleSelected().getName() + " user is disabled");
            } catch (Exception e) {
                log.error("Error", e);
            }
        }

    }

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        for (Users users : usersesDc.getMutableItems()) {
            List<Date> lastlogin = new ArrayList<>();
            lastlogin = baseService.getLastLogoutDate(users.getUuid());
            if (lastlogin.size() > 0) {
                users.setLastLoginDate(lastlogin.get(0));
            }
        }
        usersesTable.refresh();

    }
}