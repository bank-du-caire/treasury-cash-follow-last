package com.bdc.treasury.web.screens.transactionstransfered;

import com.bdc.treasury.entity.*;
import com.bdc.treasury.service.TransactionService;
import com.bdc.treasury.service.UserService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.actions.list.CreateAction;
import com.haulmont.cuba.gui.actions.list.EditAction;
import com.haulmont.cuba.gui.actions.list.RefreshAction;
import com.haulmont.cuba.gui.actions.list.RemoveAction;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.gui.screen.LookupComponent;
import com.haulmont.cuba.security.entity.Group;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@UiController("treasury_TransactionsTransfered.browse")
@UiDescriptor("transactions-transfered-browse.xml")
@LookupComponent("transactionsTransferedsTable")
@LoadDataBeforeShow
public class TransactionsTransferedBrowse extends StandardLookup<TransactionsTransfered> {
    @Inject
    private GroupTable<TransactionsTransfered> transactionsTransferedsTable;
    @Inject
    private DataManager dataManager;
    @Inject
    TransactionService transactionService;
    @Named("transactionsTransferedsTable.remove")
    private RemoveAction customersTableRemove;
    @Inject
    private CollectionLoader<TransactionsTransfered> transactionsTransferedsDl;
    private final static String CONTROL_maker_RULE = "control-maker";
    private final static String CONTROL_checker_RULE = "control-checker";
    @Inject
    private UserService userService;
    LoadContext loadContext1;

    @Named("transactionsTransferedsTable.edit")
    private EditAction<Transaction> transactionsTableEdit;

    @Named("transactionsTransferedsTable.create")
    private CreateAction<Transaction> transactionsTableCreate;

    @Named("transactionsTransferedsTable.refresh")
    private RefreshAction transactionsTableRefresh;
    @Inject
    private CollectionContainer<TransactionsTransfered> transactionsTransferedsDc;
    @Inject
    private Notifications notifications;
    @Inject
    private UserSession userSession;
    @Inject
    private Metadata metadata;
    public List<TransactionsTransfered> transactionsTransferedFilteredList = new ArrayList<>();
    @Inject
    private LookupField<CrossAccount> fromAccount;
    @Inject
    private LookupField<CrossAccount> toAccount;
    @Inject
    private DateField<Date> valueDateField;
    @Inject
    private DateField<Date> creationDateField;
    @Inject
    private LookupField<Currency> currency;
    @Inject
    private CollectionContainer<CrossAccount> toCrossAccountsDc;
    @Inject
    private CollectionContainer<CrossAccount> fromCrossAccountsDc;
    //public List<TransactionsTransfered> transactionsTransferedFilteredCheckerList = new ArrayList<>();


    @Subscribe("transactionsTransferedsTable.remove")
    public void onTransactionsTransferedsTableRemove(Action.ActionPerformedEvent event) {
        TransactionsTransfered transactionsTransfered = transactionsTransferedsTable.getSingleSelected();
        if (transactionsTransfered.getStatus() != null && !transactionsTransfered.getStatus().equals(CheckerStatus.VALIDATED.name())) {
            List<Transaction> transactionList = dataManager.load(Transaction.class)
                    .query("select e from treasury_Transaction e where e.transactionsTransfered =:transactionsTransfered")
                    .parameter("transactionsTransfered", transactionsTransfered)
                    .view("transaction-view")
                    .list();
            for (Transaction transaction : transactionList) {
                transactionService.deleteTransaction(transaction);
            }
        }
        customersTableRemove.execute();

    }

    @Subscribe
    public void onInit(InitEvent event) {
        customersTableRemove.setConfirmation(true);
        customersTableRemove.setConfirmationTitle("Removing customer...");
        customersTableRemove.setConfirmationMessage("Do you really want to remove the customer?");
        transactionsTransferedsDl.setMaxResults(10);
        transactionsTransferedFilteredList = loadPendingFilteredData();
        List<CrossAccount> accountList = new ArrayList<>();
        toAccount.setOptionsList(accountList);
        fromAccount.setOptionsList(accountList);
    }

    public void refreshDataContainers() {
        transactionsTableEdit.setAfterCloseHandler(transaction -> {
            transactionsTableRefresh.execute();
        });
        transactionsTableCreate.setAfterCloseHandler(transaction -> {
            transactionsTableRefresh.execute();
        });
    }

    @Install(to = "transactionsTransferedsDl", target = Target.DATA_LOADER)
    private List<TransactionsTransfered> transactionsTransferedsDlLoadDelegate(LoadContext<TransactionsTransfered> loadContext) {
        loadContext1 = loadContext;
        return loadPendingData(loadContext1);
    }

    public List<TransactionsTransfered> loadPendingData(LoadContext loadContext) {
        List<String> rolesNames = userService.getUserRoles();
        List<TransactionsTransfered> transactionList = new ArrayList<>();
        if (rolesNames.contains(CONTROL_maker_RULE)) {
            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString("select t from treasury_TransactionsTransfered t where (t.status = :stat1 or t.status = :stat2 or t.status = :stat3 or t.status =:stat4)")
                    .setParameter("stat1", Status.SUBMITTED.name())
                    .setParameter("stat2", CheckerStatus.RETURNED.name())
                    .setParameter("stat4", CheckerStatus.VALIDATED.name())
                    .setParameter("stat3", Status.SAVED.name());
            transactionList = dataManager.loadList(loadContext);
        }
        if (rolesNames.contains(CONTROL_checker_RULE)) {
            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString("select t from treasury_TransactionsTransfered t where (t.status = :stat1 ) or t.status=:stat3")
                    .setParameter("stat1", Status.SUBMITTED.name())
                    .setParameter("stat3", CheckerStatus.VALIDATED.name());
            transactionList = dataManager.loadList(loadContext);
        }
        transactionsTransferedsDl.setMaxResults(10);
        return transactionList;
    }

    public List<TransactionsTransfered> loadPendingFilteredData() {
        List<String> rolesNames = userService.getUserRoles();
        List<TransactionsTransfered> transactionsTransferedList = new ArrayList<>();
        if (rolesNames.contains(CONTROL_maker_RULE)) {
            transactionsTransferedList = dataManager.load(TransactionsTransfered.class)
                    .query("select t from treasury_TransactionsTransfered t where (t.status = :stat1 or t.status = :stat2 or t.status = :stat3 or t.status =:stat4)")
                    .parameter("stat1", Status.SUBMITTED.name())
                    .parameter("stat2", CheckerStatus.RETURNED.name())
                    .parameter("stat4", CheckerStatus.VALIDATED.name())
                    .parameter("stat3", Status.SAVED.name())
                    .view("transactionsTransfered-view")
                    .list();
        }
        if (rolesNames.contains(CONTROL_checker_RULE)) {
            transactionsTransferedList = dataManager.load(TransactionsTransfered.class)
                    .query("select t from treasury_TransactionsTransfered t where (t.status = :stat1 ) or t.status=:stat3")
                    .parameter("stat1", Status.SUBMITTED.name())
                    .parameter("stat3", CheckerStatus.VALIDATED.name())
                    .view("transactionsTransfered-view")
                    .list();

        }
        return transactionsTransferedList;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    @Subscribe("VALIDATEDbtn")
    public void onVALIDATEDbtnClick(Button.ClickEvent event) {
        LocalDate now = LocalDate.now();
        if (transactionsTransferedsTable.getSelected().size() > 0) {
            Set<TransactionsTransfered> transactions = transactionsTransferedsTable.getSelected();
            for (TransactionsTransfered transaction : transactions) {
                if (!transaction.getStatus().equals(CheckerStatus.VALIDATED.name())) {
                    Transaction transaction1 = metadata.create(Transaction.class);
                    Transaction transaction2 = metadata.create(Transaction.class);
//        if (status.equals("create")) {
                    Group group = userSession.getUser().getGroup();
                    List<String> rolesNames = userService.getUserRoles();
                    transaction1.setAccount(transaction.getFromCrossAccount());
                    transaction1.setDept(transaction.getTransferedBalance());
                    transaction1.setCredit(BigDecimal.ZERO);
                    transaction1.setValueDate(transaction.getValueDate());
                    transaction1.setStatus(CheckerStatus.VALIDATED.name());
                    transaction1.setGroupName(group.getName());
                    transaction1.setTransactionsTransfered(transaction);
                    transaction1.setCurrency(transaction.getFromAccountCurrency());
                    transaction1.setCreationDate(now);
                    transaction1.setGroupID(group.getId());
                    dataManager.commit(transaction1);
                    //////////////////////
                    transaction2.setAccount(transaction.getToCrossAccount());
                    transaction2.setDept(BigDecimal.ZERO);
                    transaction2.setCredit(transaction.getTransferedBalance());
                    transaction2.setValueDate(transaction.getValueDate());
                    transaction2.setStatus(CheckerStatus.VALIDATED.name());
                    transaction2.setGroupName(group.getName());
                    transaction2.setTransactionsTransfered(transaction);
                    transaction2.setCurrency(transaction.getFromAccountCurrency());
                    transaction2.setCreationDate(now);
                    transaction2.setGroupID(group.getId());
                    dataManager.commit(transaction2);
                    // update table
                    transaction.setStatus(CheckerStatus.VALIDATED.name());
                    transactionsTransferedsDc.getItem().setStatus(CheckerStatus.VALIDATED.name());
                    dataManager.commit(transaction);
                }
            }
            transactionsTableRefresh.execute();

        } else {
            message();
        }
    }

    public List<Transaction> transactionList = new ArrayList<>();

    @Subscribe("RETURNEDbtn")
    public void onRETURNEDbtnClick(Button.ClickEvent event) {
        if (transactionsTransferedsTable.getSelected().size() > 0) {
            Set<TransactionsTransfered> transactions = transactionsTransferedsTable.getSelected();
            for (TransactionsTransfered transactionsTransfered : transactions) {
                transactionList = dataManager.load(Transaction.class)
                        .query("select e from treasury_Transaction e where e.transactionsTransfered =:transactionsTransfered")
                        .parameter("transactionsTransfered", transactionsTransfered)
                        .view("transaction-view")
                        .list();
                if (transactionList != null && transactionList.size() > 0) {
                    for (Transaction transaction : transactionList) {
                        transactionService.deleteTransaction(transaction);
                    }
                }
                transactionsTransfered.setStatus(CheckerStatus.RETURNED.name());
                transactionsTransferedsDc.getItem(transactionsTransfered).setStatus(CheckerStatus.RETURNED.name());
                dataManager.commit(transactionsTransfered);
            }
            transactionsTableRefresh.execute();
            transactionsTableRefresh.execute();
        } else {
            message();
        }
    }

    @Subscribe("SUBMITTEDbtn")
    public void onSUBMITTEDbtnClick(Button.ClickEvent event) {
        if (transactionsTransferedsTable.getSelected().size() > 0) {
            Set<TransactionsTransfered> transactions = transactionsTransferedsTable.getSelected();
            for (TransactionsTransfered transaction : transactions) {
                if (!transaction.getStatus().equals(Status.SUBMITTED.name())) {
                    if (!transaction.getStatus().equals(CheckerStatus.VALIDATED.name())) {
                        transaction.setStatus(Status.SUBMITTED.name());
                        transactionsTransferedsTable.setSelected(transaction);
                        transactionsTransferedsDc.getItem(transaction).setStatus(Status.SUBMITTED.name());
                        dataManager.commit(transaction);
                    } else {
                        notifications.create().withCaption("not allowed to submit validated transaction").show();
                    }
                }
            }
            transactionsTableRefresh.execute();
            transactionsTableRefresh.execute();
        } else {
            message();
        }
    }

    @Subscribe("toAccount")
    public void onToAccountValueChange(HasValue.ValueChangeEvent<CrossAccount> event) {
        transactionsTransferedFilteredList = transactionsTransferedFilteredList.stream().filter(transaction1 -> {
            if (toAccount.getValue() != null && toAccount.getValue().getName() != null)
                return transaction1.getToCrossAccount().getName().equals(toAccount.getValue().getName())
                        && transaction1.getToCrossAccount().getCurrency().equals(currency.getValue());
            return false;
        }).collect(Collectors.toList());
        transactionsTransferedsDc.getMutableItems().clear();
        transactionsTransferedsDc.getMutableItems().addAll(transactionsTransferedFilteredList);
    }

    @Subscribe("fromAccount")
    public void onFromAccountValueChange(HasValue.ValueChangeEvent<CrossAccount> event) {
        transactionsTransferedFilteredList = transactionsTransferedFilteredList.stream().filter(transaction1 -> {
            if (fromAccount.getValue() != null && fromAccount.getValue().getName() != null)
                return transaction1.getFromCrossAccount().getName().equals(fromAccount.getValue().getName())
                        && transaction1.getFromCrossAccount().getCurrency().equals(currency.getValue());
            return false;
        }).collect(Collectors.toList());
        transactionsTransferedsDc.getMutableItems().clear();
        transactionsTransferedsDc.getMutableItems().addAll(transactionsTransferedFilteredList);
    }

    @Subscribe("valueDateField")
    public void onValueDateFieldValueChange(HasValue.ValueChangeEvent<java.sql.Date> event) {
        transactionsTransferedFilteredList = transactionsTransferedFilteredList.stream().filter(transaction1 -> {
            if (valueDateField.getValue() != null)
                return transaction1.getValueDate().equals(valueDateField.getValue());
            // && transaction1.getAccount().getCurrency().equals(currency.getValue())
            return false;
        }).collect(Collectors.toList());
        transactionsTransferedsDc.getMutableItems().clear();
        transactionsTransferedsDc.getMutableItems().addAll(transactionsTransferedFilteredList);
    }

    @Subscribe("creationDateField")
    public void onCreationDateFieldValueChange(HasValue.ValueChangeEvent<Date> event) {
        transactionsTransferedFilteredList = transactionsTransferedFilteredList.stream().filter(transaction1 -> {
            if (creationDateField.getValue() != null) {
                Date creatoinDate = transaction1.getCreateTs();
                ZoneId z = ZoneId.systemDefault();
                Instant instant = creatoinDate.toInstant();
                LocalDate localDate = instant.atZone(z).toLocalDate();
                System.out.println(localDate);
                return localDate.equals(creationDateField.getValue());
            }
            // && transaction1.getAccount().getCurrency().equals(currency.getValue())
            return false;
        }).collect(Collectors.toList());
        transactionsTransferedsDc.getMutableItems().clear();
        transactionsTransferedsDc.getMutableItems().addAll(transactionsTransferedFilteredList);
    }

    @Subscribe("transactionsTransferedsTable.clearFilter")
    public void onTransactionsTransferedsTableClearFilter(Action.ActionPerformedEvent event) {
        toAccount.setValue(null);
        fromAccount.setValue(null);
        creationDateField.setValue(null);
        valueDateField.setValue(null);
        currency.setValue(null);
        refreshDataContainers();
        transactionsTransferedFilteredList = loadPendingFilteredData();
        transactionsTableRefresh.execute();
    }


    @Subscribe("currency")
    public void onCurrencyValueChange(HasValue.ValueChangeEvent<Currency> event) throws ParseException {
        transactionsTransferedFilteredList = transactionsTransferedFilteredList.stream().filter(transaction1 -> {
            if (currency.getValue() != null && currency.getValue().getName() != null)
                return transaction1.getFromCrossAccount().getCurrency().getName().equals(currency.getValue().getName());
            /*|| transaction1.getToCrossAccount().getCurrency().getName().equals(currency.getValue().getName());*/
            return false;
        }).collect(Collectors.toList());
        transactionsTransferedsDc.getMutableItems().clear();
        transactionsTransferedsDc.getMutableItems().addAll(transactionsTransferedFilteredList);

        if (currency.getValue() != null && currency.getValue().getName() != null) {
            List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
                    .query("select t from treasury_CrossAccount t where t.currency =:currency and t.status=:status")
                    .parameter("status", CheckerStatus.VALIDATED.name())
                    .parameter("currency", currency.getValue())
                    .view("crossAccount-view")
                    .list();
            fromCrossAccountsDc.getMutableItems().clear();
            fromCrossAccountsDc.getMutableItems().addAll(crossAccountList);
            fromAccount.setOptionsList(crossAccountList);
            toCrossAccountsDc.getMutableItems().clear();
            toCrossAccountsDc.getMutableItems().addAll(crossAccountList);
            toAccount.setOptionsList(crossAccountList);
        } else {
            List<CrossAccount> crossAccountList = new ArrayList<>();
            fromAccount.setOptionsList(crossAccountList);
            toAccount.setOptionsList(crossAccountList);
        }
    }

    public void message() {
        notifications.create().withCaption("You should select one row at least").show();
    }

}