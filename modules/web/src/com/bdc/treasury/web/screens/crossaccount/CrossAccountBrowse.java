package com.bdc.treasury.web.screens.crossaccount;

import com.bdc.treasury.entity.*;
import com.bdc.treasury.service.UserService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.actions.list.CreateAction;
import com.haulmont.cuba.gui.actions.list.EditAction;
import com.haulmont.cuba.gui.actions.list.RefreshAction;
import com.haulmont.cuba.gui.actions.list.RemoveAction;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.data.CollectionDatasource;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.gui.screen.LookupComponent;
import com.haulmont.cuba.gui.xml.layout.ComponentsFactory;
import com.vaadin.ui.Alignment;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@UiController("treasury_CrossAccount.browse")
@UiDescriptor("cross-account-browse.xml")
@LookupComponent("crossAccountsTable")
@LoadDataBeforeShow
public class CrossAccountBrowse extends StandardLookup<CrossAccount> {


    @Inject
    private LookupField<Currency> currency;
    @Inject
    private LookupField<CrossAccount> crossAccount;
    @Inject
    private CollectionLoader<CrossAccount> crossAccountsDl;

    @Inject
    @Named("crossAccountsTable.refresh")
    private RefreshAction crossAccountsTable1;

    @Named("crossAccountsTable.create")
    private CreateAction<CrossAccount> crossAccountsTableCreate;
    @Named("crossAccountsTable.edit")
    private EditAction<CrossAccount> crossAccountsTableEdit;

    private final static String ReconciliationRoleMaker = "Reconciliation-Role-Maker";
    private final static String ReconciliationRoleChecker = "Reconciliation-Role-Checker";
    @Inject
    private UserService userService;
    @Inject
    private DataManager dataManager;

    public List<CrossAccount> CrossAccountMakerFilteredList = new ArrayList<>();
    public List<CrossAccount> CrossAccountFilteredCheckerList = new ArrayList<>();
    public List<CrossAccount> CrossAccountFilteredList = new ArrayList<>();
    @Inject
    private CollectionContainer<CrossAccount> crossAccountsDc;
    @Inject
    private Notifications notifications;
    @Inject
    private GroupTable<CrossAccount> crossAccountsTable;

    @Subscribe("crossAccountsTable.clearFilter")
    public void onCustomerBasesTableClearFilter(Action.ActionPerformedEvent event) {
        crossAccount.setValue(null);
        currency.setValue(null);
        refreshDataContainers();
        CrossAccountMakerFilteredList = getCrossAccountReconilationMaker();
        CrossAccountFilteredCheckerList = getCrossAccountReconilationChecker();
        CrossAccountFilteredList= getValidatedCrossAccount();
        crossAccountsTable1.execute();
    }

    @Subscribe
    public void onInit(InitEvent event) {
        refreshDataContainers();
        crossAccountsDl.setMaxResults(10);
        CrossAccountMakerFilteredList = getCrossAccountReconilationMaker();
        CrossAccountFilteredCheckerList = getCrossAccountReconilationChecker();
        CrossAccountFilteredList = getValidatedCrossAccount();
        crossAccount.setOptionsList(new ArrayList<>());
    }

    @Subscribe("crossAccount")
    public void onCrossAccountValueChange(HasValue.ValueChangeEvent<CrossAccount> event) {
        if (!userService.getUserRoles().contains(ReconciliationRoleMaker) && !userService.getUserRoles().contains(ReconciliationRoleChecker)) {
            CrossAccountFilteredList = CrossAccountFilteredList.stream().filter(crossAccount1 -> {
                if (crossAccount.getValue() != null && crossAccount.getValue().getName() != null)
                    return crossAccount1.getName().equals(crossAccount.getValue().getName())
                            && crossAccount1.getCurrency().getName().equals(currency.getValue().getName());
                return false;
            }).collect(Collectors.toList());
            crossAccountsDc.getMutableItems().clear();
            crossAccountsDc.getMutableItems().addAll(CrossAccountFilteredList);
        } else if (userService.getUserRoles().contains(ReconciliationRoleMaker)) {
            CrossAccountMakerFilteredList = CrossAccountMakerFilteredList.stream().filter(crossAccount1 -> {
                if (crossAccount.getValue() != null && crossAccount.getValue().getName() != null)
                    return crossAccount1.getName().equals(crossAccount.getValue().getName())
                            && crossAccount1.getCurrency().getName().equals(currency.getValue().getName());
                return false;
            }).collect(Collectors.toList());
            crossAccountsDc.getMutableItems().clear();
            crossAccountsDc.getMutableItems().addAll(CrossAccountMakerFilteredList);
        } else if (userService.getUserRoles().contains(ReconciliationRoleChecker)) {
            CrossAccountFilteredCheckerList = CrossAccountFilteredCheckerList.stream().filter(crossAccount1 -> {
                if (crossAccount.getValue() != null && crossAccount.getValue().getName() != null)
                    return crossAccount1.getName().equals(crossAccount.getValue().getName())
                            && crossAccount1.getCurrency().getName().equals(currency.getValue().getName());
                return false;
            }).collect(Collectors.toList());
            crossAccountsDc.getMutableItems().clear();
            crossAccountsDc.getMutableItems().addAll(CrossAccountFilteredCheckerList);
        }
    }


    @Subscribe("currency")
    public void onCurrencyValueChange(HasValue.ValueChangeEvent<Currency> event) throws ParseException {
        if (currency.getValue() != null && currency.getValue().getName() != null) {
            List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
                    .query("select t from treasury_CrossAccount t where t.currency =:currency and t.status=:status")
                    .parameter("status", CheckerStatus.VALIDATED.name())
                    .parameter("currency", currency.getValue())
                    .view("crossAccount-view")
                    .list();
            crossAccountsDc.getMutableItems().clear();
            crossAccountsDc.getMutableItems().addAll(crossAccountList);
            crossAccount.setOptionsList(crossAccountList);
        } else {
            List<CrossAccount> crossAccountList = new ArrayList<>();
            crossAccount.setOptionsList(crossAccountList);
        }
        if (!userService.getUserRoles().contains(ReconciliationRoleMaker) && !userService.getUserRoles().contains(ReconciliationRoleChecker)) {
            CrossAccountFilteredList = CrossAccountFilteredList.stream().filter(crossAccount -> {
                if (currency.getValue() != null && currency.getValue().getName() != null)
                    return crossAccount.getCurrency().getName().equals(currency.getValue().getName());
                return false;
            }).collect(Collectors.toList());
            crossAccountsDc.getMutableItems().clear();
            crossAccountsDc.getMutableItems().addAll(CrossAccountFilteredList);
        } else if (userService.getUserRoles().contains(ReconciliationRoleMaker)) {
            CrossAccountMakerFilteredList = CrossAccountMakerFilteredList.stream().filter(crossAccount -> {
                if (currency.getValue() != null && currency.getValue().getName() != null)
                    return crossAccount.getCurrency().getName().equals(currency.getValue().getName());
                return false;
            }).collect(Collectors.toList());
            crossAccountsDc.getMutableItems().clear();
            crossAccountsDc.getMutableItems().addAll(CrossAccountMakerFilteredList);
        } else if (userService.getUserRoles().contains(ReconciliationRoleChecker)) {
            CrossAccountFilteredCheckerList = CrossAccountFilteredCheckerList.stream().filter(crossAccount -> {
                if (currency.getValue() != null && currency.getValue().getName() != null)
                    return crossAccount.getCurrency().getName().equals(currency.getValue().getName());
                return false;
            }).collect(Collectors.toList());
            crossAccountsDc.getMutableItems().clear();
            crossAccountsDc.getMutableItems().addAll(CrossAccountFilteredCheckerList);
        }


    }


    @Install(to = "crossAccountsDl", target = Target.DATA_LOADER)
    private List<CrossAccount> crossAccountsDlLoadDelegate(LoadContext<CrossAccount> loadContext) {
        if (!userService.getUserRoles().contains(ReconciliationRoleMaker) && !userService.getUserRoles().contains(ReconciliationRoleChecker)) {
            return getValidatedCrossAccount();
        } else if (userService.getUserRoles().contains(ReconciliationRoleMaker)) {
            return getCrossAccountReconilationMaker();
        } else if (userService.getUserRoles().contains(ReconciliationRoleChecker)) {
            return getCrossAccountReconilationChecker();
        }

        return new ArrayList<>();
    }

    public List<CrossAccount> getValidatedCrossAccount() {
        List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
                .query("select t from treasury_CrossAccount t where  t.status=:status")
                .parameter("status", CheckerStatus.VALIDATED.name())
                .view("crossAccount-view")
                .list();
        return crossAccountList;

    }

    public List<CrossAccount> getCrossAccountReconilationMaker() {
        List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
                .query("select t from treasury_CrossAccount t ")
                .view("crossAccount-view")
                .list();
        return crossAccountList;

    }

    public List<CrossAccount> getCrossAccountReconilationChecker() {
        List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
                .query("select t from treasury_CrossAccount t where  t.status=:status or t.status=:status2")
                .parameter("status", CheckerStatus.VALIDATED.name())
                .parameter("status2", Status.SUBMITTED.name())
                .view("crossAccount-view")
                .list();
        return crossAccountList;

    }

    public void refreshDataContainers() {
        crossAccountsTableEdit.setAfterCloseHandler(transaction -> {
            crossAccountsTable1.execute();
        });
        crossAccountsTableCreate.setAfterCloseHandler(transaction -> {
            crossAccountsTable1.execute();

        });
    }
    @Named("crossAccountsTable.remove")
    private RemoveAction crossAccountsTableRemove;

    @Subscribe("crossAccountsTable.remove")
    public void onCrossAccountsTableRemove(Action.ActionPerformedEvent event) {
        CrossAccount crossAccount = crossAccountsTable.getSingleSelected();
        if (crossAccount.getStatus() != null && crossAccount.getStatus().equals(CheckerStatus.VALIDATED.name())) {
            notifications.create().withCaption("Not Allowed To Remove Validated Correspondent Account").show();
        } else {
            crossAccountsTableRemove.execute();
        }
    }



}