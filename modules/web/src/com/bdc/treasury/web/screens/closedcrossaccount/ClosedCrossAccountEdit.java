package com.bdc.treasury.web.screens.closedcrossaccount;

import com.bdc.treasury.entity.CloseAccountState;
import com.bdc.treasury.entity.CrossAccount;
import com.bdc.treasury.service.ClosedAccountService;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.model.CollectionPropertyContainer;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.treasury.entity.ClosedCrossAccount;
import com.haulmont.cuba.security.entity.Group;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import java.time.LocalDate;

@UiController("treasury_ClosedCrossAccount.edit")
@UiDescriptor("closed-cross-account-edit.xml")
@EditedEntityContainer("closedCrossAccountDc")
@LoadDataBeforeShow
public class ClosedCrossAccountEdit extends StandardEditor<ClosedCrossAccount> {
    @Inject
    private Notifications notifications;
    public String eventState = "";

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        if (closedCrossAccountDc.getItem().getState() == null
                && closedCrossAccountDc.getItem().getValueDate() == null) {
            eventState = "create";
        } else {
            eventState = "edit";
        }
    }

    @Inject
    private ClosedAccountService closedAccountService;
    @Inject
    private UserSession userSession;

    @Subscribe
    public void onBeforeCommitChanges(BeforeCommitChangesEvent event) {
        Group group = userSession.getUser().getGroup();
        if (closedCrossAccountDc.getItem().getCrossAccount() == null ||
                closedCrossAccountDc.getItem().getCrossAccount().size() == 0) {
            event.preventCommit();
            notifications.create().withCaption("You should select Correspondent Account").show();
        }
        if (closedCrossAccountDc.getItem().getGroups() == null ||
                closedCrossAccountDc.getItem().getGroups().size() == 0) {
            event.preventCommit();
            notifications.create().withCaption("You should select Department").show();
        }
        if (eventState.equals("create") && closedCrossAccountDc.getItem().getCrossAccount() != null && closedCrossAccountDc.getItem().getCrossAccount().size() > 0
                && closedCrossAccountDc.getItem().getValueDate() != null) {
            for (CrossAccount crossAccount : closedCrossAccountDc.getItem().getCrossAccount())
                if (closedAccountService.checkOfClosedCrossAccount(crossAccount, closedCrossAccountDc.getItem().getValueDate(), CloseAccountState.CLOSED.name(),group)) {
                    event.preventCommit();
                    notifications.create().withCaption("This account ( " + crossAccount.getName() + " ) is already added to closed list with value date = " + closedCrossAccountDc.getItem().getValueDate()).show();
                }

        }
        for (Group group1 : closedCrossAccountDc.getItem().getGroups())
            if (group1.getName().equals("Reconciliation")) {
                event.preventCommit();
                notifications.create().withCaption("Not Allowed to Close Reconciliation Department").show();
            }
    }

    @Inject
    private InstanceContainer<ClosedCrossAccount> closedCrossAccountDc;

   /* @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        LocalDate now = LocalDate.now();
        closedCrossAccountDc.getItem().setValueDate(now);
    }*/
}