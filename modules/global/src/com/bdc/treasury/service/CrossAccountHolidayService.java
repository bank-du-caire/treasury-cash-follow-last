package com.bdc.treasury.service;

import com.bdc.treasury.entity.CrossAccount;

import java.time.LocalDate;

public interface CrossAccountHolidayService {
    String NAME = "treasury_CrossAccountHolidayService";
    public boolean checkHolidaysOfCrossAccount(CrossAccount crossAccount, LocalDate valueDate);
}