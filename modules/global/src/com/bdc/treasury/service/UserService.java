package com.bdc.treasury.service;

import com.haulmont.cuba.security.entity.User;

import java.util.List;

public interface UserService {
    String NAME = "treasury_UserService";

    public List<String> getUserRoles();
    public User getUser();
}