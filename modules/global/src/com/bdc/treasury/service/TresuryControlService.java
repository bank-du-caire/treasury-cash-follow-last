package com.bdc.treasury.service;

import com.bdc.treasury.entity.CrossAccount;
import com.bdc.treasury.entity.TreasuryCtrl;

import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.List;

public interface TresuryControlService {
    String NAME = "treasury_TresuryControlService";
    public List<TreasuryCtrl> getTreasuryCtrls(LocalDate Date) throws ParseException;

    public List<CrossAccount> getCorrespondentAcount() throws ParseException;

    public boolean validateExistOpeningBalance(CrossAccount crossAccount, LocalDate date);
    public Timestamp getLastCreationDateOfOpeningBalance(CrossAccount crossAccount, LocalDate date);
}