package com.bdc.treasury.service;

import com.bdc.treasury.entity.Currency;

public interface CrossAccountService {
    String NAME = "treasury_CrossAccountService";
    public boolean currencyExist(Currency currency);
}