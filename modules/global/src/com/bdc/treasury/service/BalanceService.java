package com.bdc.treasury.service;

import com.bdc.treasury.entity.BalancesList;
import com.bdc.treasury.entity.CrossAccount;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface BalanceService {
    String NAME = "treasury_BalanceService";
    public Date getLastDateOfOpenBalance(CrossAccount crossAccount, UUID groupId);
    public List<CrossAccount> getCorrespondentAcount();
    public void updateBalanceList(BalancesList balancesList);
}