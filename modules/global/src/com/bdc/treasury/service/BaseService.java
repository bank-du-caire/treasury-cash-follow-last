package com.bdc.treasury.service;

import com.bdc.treasury.entity.UserRole;
import com.bdc.treasury.entity.Users;
import com.haulmont.cuba.security.entity.User;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface BaseService {
    String NAME = "treasury_BaseService";


    public List<UserRole> loadUserRoles(Users user);

    Users addEditUser(Users user, List<UserRole> UserRoles);

    User addEditSecUser(User user, List<com.haulmont.cuba.security.entity.UserRole> UserRoles);

    List<com.haulmont.cuba.security.entity.UserRole> CheckSecUserRoleDel(User user, List<com.haulmont.cuba.security.entity.UserRole> UserRoles);

    com.haulmont.cuba.security.entity.User getSecUser(UUID userId);

    User updateSecUserAndRoles(User user, List<com.haulmont.cuba.security.entity.UserRole> UserRoles);

    List<UserRole> CheckuserRoleDel(Users user, List<UserRole> UserRoles);

    Users updateUserAndRoles(Users user, List<UserRole> UserRoles);

    List<Date> getLastLogoutDate(UUID id);
}