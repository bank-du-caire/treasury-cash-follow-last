package com.bdc.treasury.service;

import com.bdc.treasury.entity.CrossAccount;
import com.haulmont.cuba.security.entity.Group;

import java.time.LocalDate;

public interface ClosedAccountService {
    String NAME = "treasury_ClosedAccountService";
    public boolean checkOfClosedCrossAccount(CrossAccount crossAccount, LocalDate valueDate , String state, Group group);
}