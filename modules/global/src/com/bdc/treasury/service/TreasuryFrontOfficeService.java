package com.bdc.treasury.service;

import com.bdc.treasury.entity.CrossAccount;
import com.bdc.treasury.entity.Transaction;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public interface TreasuryFrontOfficeService {
    String NAME = "treasury_TreasuryFrontOfficeService";
    public BigDecimal getClosedBalance(CrossAccount crossAccount , LocalDate date);
    public BigDecimal getClosedBalanceTest(CrossAccount crossAccount , LocalDate date);
    public BigDecimal getClosedBalanceForTreasuryCtrl(CrossAccount crossAccount , LocalDate date);
    public BigDecimal getOpeningBalance(CrossAccount crossAccount , LocalDate date);
    public List<Transaction> getTransactionList(CrossAccount crossAccount,LocalDate startDate,LocalDate endDate);
    public List<LocalDate> getPreValueDateTransactionList(CrossAccount crossAccount, LocalDate date);
    public List getPreValueDateRemmitanceList(CrossAccount crossAccount,LocalDate localDate);
    public List getPreValueDateKondorList(CrossAccount crossAccount,LocalDate localDate);
}