package com.bdc.treasury.service;

import com.bdc.treasury.entity.Transaction;
import com.bdc.treasury.entity.TransactionsTransfered;

public interface TransactionsTransferedService {
    String NAME = "treasury_TransactionsTransferedService";
    public void updateTransaction(TransactionsTransfered transaction);
}