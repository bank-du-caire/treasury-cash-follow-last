package com.bdc.treasury.service;

import com.bdc.treasury.entity.CrossAccount;
import com.bdc.treasury.entity.Currency;
import com.bdc.treasury.entity.Transaction;
import com.bdc.treasury.entity.TreasuryCtrl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface TransactionService {
    String NAME = "treasury_TransactionService";

    public Boolean transactionIsExpired(Transaction transaction);

    public void updateTransaction(Transaction transaction);

    public void deleteTransaction(Transaction transaction);

    public List<Transaction>getTransactionCreationDate(Date date);

    public List getRemmitanceList();

    public List getRemmitanceListValidated();

    public void updateRemittanceStatus(BigDecimal id);

    public void deleteRemittanceStatus(BigDecimal id);

    public List getTransactionList(CrossAccount crossAccount, LocalDate startDate, LocalDate endDate);
    public List getTransactionListTest(CrossAccount crossAccount, LocalDate startDate, LocalDate endDate);
    public TreasuryCtrl mapToTreasuryCtrl(CrossAccount crossAccount, LocalDate date);

    public boolean checkCurrencyOfAccount(CrossAccount crossAccount, Currency currency);
}