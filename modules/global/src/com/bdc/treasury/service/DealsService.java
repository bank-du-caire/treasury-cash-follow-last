package com.bdc.treasury.service;

import com.bdc.treasury.entity.CrossAccount;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

public interface DealsService {
    String NAME = "treasury_DealsService";
    public List getDeals(CrossAccount crossAccount, LocalDate startDate, LocalDate endDate);
    public List getDeals(CrossAccount crossAccount, LocalDate startDate);
    public List getDealsForTreasuryCTRL(CrossAccount crossAccount, LocalDate startDate);
    public List getDealsWithinDuration(CrossAccount crossAccount, LocalDate startDate, LocalDate endDate);
    public List getDealsWithinDurationTest(CrossAccount crossAccount, LocalDate startDate, LocalDate endDate);
    public List<Timestamp> getLastValueDateOfDeals(CrossAccount crossAccount, LocalDate startDate);
}