package com.bdc.treasury.entity;

import com.haulmont.chile.core.annotations.MetaProperty;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Table(name = "TREASURY_TRANSACTION")
@Entity(name = "treasury_Transaction")
@NamePattern("%s|version")
public class Transaction extends StandardEntity {
    private static final long serialVersionUID = -6227899507969818039L;

    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "ACCOUNT_ID")
    private CrossAccount account;

    @Column(name = "CREATION_DATE")
    private LocalDate creationDate;

    @NotNull
    @Column(name = "STATUS", nullable = false)
    private String status;


    @Column(name = "GROUP_ID")
    private UUID groupID;

    @Column(name = "REM_ID")
    private BigDecimal remId;

    @Column(name = "GROUP_NAME")
    private String groupName;

    public BigDecimal getRemId() {
        return remId;
    }

    public void setRemId(BigDecimal remId) {
        this.remId = remId;
    }




    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "CURRENCY_ID")
    private Currency currency;


    @Column(name = "DEPT")
    private BigDecimal dept;

    @Column(name = "CREDIT")
    private BigDecimal credit;

    @Column(name = "VALUE_DATE")
    private LocalDate valueDate;

    @Column(name = "REFERENCE_NUMBER")
    private String referenceNumber;

    @Column(name = "DESCRIPTION")
    private String description;

    @OneToMany(mappedBy = "transaction")
    private List<Attachement> attachement;

    public List<Attachement> getAttachement() {
        return attachement;
    }

    public void setAttachement(List<Attachement> attachement) {
        this.attachement = attachement;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


//    @Column(name = "without_Value_Date")
//    private boolean withoutValueDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transactions_Transfered")
    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    private TransactionsTransfered transactionsTransfered;

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

//    public boolean isWithoutValueDate() {
//        return withoutValueDate;
//    }
//
//    public void setWithoutValueDate(boolean withoutValueDate) {
//        this.withoutValueDate = withoutValueDate;
//    }

    public TransactionsTransfered getTransactionsTransfered() {
        return transactionsTransfered;
    }

    public void setTransactionsTransfered(TransactionsTransfered transactionsTransfered) {
        this.transactionsTransfered = transactionsTransfered;
    }


    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void setValueDate(LocalDate valueDate) {
        this.valueDate = valueDate;
    }

    public LocalDate getValueDate() {
        return valueDate;
    }


    public UUID getGroupID() {
        return groupID;
    }

    public void setGroupID(UUID groupID) {
        this.groupID = groupID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public BigDecimal getCredit() {
        return credit;
    }

    public void setCredit(BigDecimal credit) {
        this.credit = credit;
    }

    public BigDecimal getDept() {
        return dept;
    }

    public void setDept(BigDecimal dept) {
        this.dept = dept;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public CrossAccount getAccount() {
        return account;
    }

    public void setAccount(CrossAccount account) {
        this.account = account;
    }
}