package com.bdc.treasury.entity;

import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.security.entity.Group;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Table(name = "TREASURY_CLOSED_CROSS_ACCOUNT")
@Entity(name = "treasury_ClosedCrossAccount")
public class ClosedCrossAccount extends StandardEntity {
    private static final long serialVersionUID = -2000726360526672962L;

    @JoinTable(name = "CLOSED_ACCT_CROSS_ACCT_LINK",
            joinColumns = @JoinColumn(name = "CLOSED_CROSS_ACCOUNT_ID"),
            inverseJoinColumns = @JoinColumn(name = "CROSS_ACCOUNT_ID"))
    @ManyToMany
    private List<CrossAccount> crossAccount;


    @JoinTable(name = "CLOSED_CROSS_ACC_GROUP_LINK",
            joinColumns = @JoinColumn(name = "CLOSED_CROSS_ACCOUNT_ID"),
            inverseJoinColumns = @JoinColumn(name = "GROUP_ID"))
    @ManyToMany
    @NotNull(message = "You should choose department")
    private List<Group> groups;

    @NotNull
    @Column(name = "VALUE_DATE", nullable = false)
    private LocalDate valueDate;

    @NotNull
    @Column(name = "STATE", nullable = false)
    private String state;

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }




    public CloseAccountState getState() {
        return state == null ? null : CloseAccountState.fromId(state);
    }

    public void setState(CloseAccountState state) {
        this.state = state == null ? null : state.getId();
    }

    public LocalDate getValueDate() {
        return valueDate;
    }

    public void setValueDate(LocalDate valueDate) {
        this.valueDate = valueDate;
    }

    public List<CrossAccount> getCrossAccount() {
        return crossAccount;
    }

    public void setCrossAccount(List<CrossAccount> crossAccount) {
        this.crossAccount = crossAccount;
    }
}