package com.bdc.treasury.entity;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum CloseAccountState implements EnumClass<String> {

    CLOSED("CLOSED"),
    OPENED("OPENED");

    private String id;

    CloseAccountState(String value) {
        this.id = value;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static CloseAccountState fromId(String id) {
        for (CloseAccountState at : CloseAccountState.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}