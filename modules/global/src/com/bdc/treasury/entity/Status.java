package com.bdc.treasury.entity;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum Status implements EnumClass<String> {

    SAVED("SAVED"),
    TEMPORARY("TEMPORARY"),
    SUBMITTED("SUBMITTED"),
    CREATE_PENDING("Pending create request"),
    CREATE_APPROVED("Create request approved"),
    CREATE_REJECTED("Create request rejected"),
    UPDATE_PENDING("Pending update request"),
    UPDATE_APPROVED("Update request approved"),
    UPDATE_REJECTED("Update request rejected"),
    DELETE_PENDING("Pending delete request"),
    DELETE_APPROVED("Update delete approved"),
    DELETE_REJECTED("Delete request rejected");

    private String id;

    Status(String value) {
        this.id = value;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static Status fromId(String id) {
        for (Status at : Status.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}