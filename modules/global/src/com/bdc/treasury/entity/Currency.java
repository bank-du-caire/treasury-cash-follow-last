package com.bdc.treasury.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.global.DeletePolicy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "TREASURY_CURRENCY")
@Entity(name = "treasury_Currency")
@NamePattern("%s|name")
public class Currency extends StandardEntity {
    private static final long serialVersionUID = -8268986828158999763L;

    @Column(name = "NAME")
    @NotNull(message = "this field required")
    private String name;

    @NotNull(message = "this filed is required")
    @Column(name = "CODE", nullable = false, unique = true)
    private String code;

    @Column(name = "SYMBOL", nullable = false, unique = true)
    private String symbol;

    @OneToMany(mappedBy = "currency")
    @OnDelete(DeletePolicy.CASCADE)
    private java.util.List<CrossAccount> crossAccounts;

    public void setCrossAccounts(java.util.List<CrossAccount> crossAccounts) {
        this.crossAccounts = crossAccounts;
    }

    public java.util.List<CrossAccount> getCrossAccounts() {
        return crossAccounts;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}