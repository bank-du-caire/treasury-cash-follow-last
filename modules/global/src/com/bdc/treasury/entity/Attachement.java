package com.bdc.treasury.entity;

import com.haulmont.cuba.core.entity.FileDescriptor;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.*;

@Table(name = "TREASURY_ATTACHEMENT")
@Entity(name = "treasury_Attachement")
public class Attachement extends StandardEntity {
    private static final long serialVersionUID = -8339048918190966987L;

    private @JoinColumn(name = "FILE_ID_ID")
    @OneToOne
    FileDescriptor fileId;

    @Column(name = "FILE_NAME")
    private String fileName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transaction_ID")
    private Transaction transaction;

    @Column(name = "transaction_Code")
    private String transactionCode;

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setFileId(FileDescriptor fileId) {
        this.fileId = fileId;
    }

    public FileDescriptor getFileId() {
        return fileId;
    }


}