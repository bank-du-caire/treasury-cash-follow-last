package com.bdc.treasury.entity;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum DepitCreditState implements EnumClass<String> {

    DEBIT("DEBIT"),
    CREDIT("CREDIT");

    private String id;

    DepitCreditState(String value) {
        this.id = value;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static DepitCreditState fromId(String id) {
        for (DepitCreditState at : DepitCreditState.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}