package com.bdc.treasury.entity;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum CheckerStatus implements EnumClass<String>  {

    VALIDATED("VALIDATED"),
    RETURNED("RETURNED"),
    EXCEPTION("EXCEPTION");

    private String id;

    CheckerStatus(String value) {
        this.id = value;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static CheckerStatus fromId(String id) {
        for (CheckerStatus at : CheckerStatus.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}