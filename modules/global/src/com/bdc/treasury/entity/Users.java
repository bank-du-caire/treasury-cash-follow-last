package com.bdc.treasury.entity;

import com.haulmont.chile.core.annotations.MetaProperty;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.entity.annotation.OnDeleteInverse;
import com.haulmont.cuba.core.global.DeletePolicy;
import com.haulmont.cuba.security.entity.Group;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Table(name = "TREASURY_USERS")
@Entity(name = "treasury_Users")
public class Users extends StandardEntity {
    private static final long serialVersionUID = -6027569962482006673L;
    @NotNull
    @Column(name = "LOGIN", nullable = false, unique = true)
    protected String name;


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_LOGIN_DATE")
    private Date lastLoginDate;

    @OnDeleteInverse(DeletePolicy.UNLINK)
    @OnDelete(DeletePolicy.UNLINK)
    @OneToMany(mappedBy = "users")
    protected List<UserRole> roles;

    @Column(name = "STATUS")
    protected String status;

    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    @NotNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "GROUP_ID")
    protected Group group;

    @Column(name = "ACTIVE")
    private Boolean active = false;

    @Column(name = "ALL_ROLES_IN_STRING", length = 1000)
    @MetaProperty(related = {"roles"})
    private String allRolesInString;

    public void setRoles(List<UserRole> roles) {
        this.roles = roles;
    }

    public List<UserRole> getRoles() {
        return roles;
    }

    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }


    public void setStatus(Status status) {
        this.status = status == null ? null : status.getId();
    }

    public Status getStatus() {
        return status == null ? null : Status.fromId(status);
    }


    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getAllRolesInString() {
        if(this.name!=null) {
            allRolesInString  = roles.get(0).roleName+"";

            for (int i=1;i<roles.size();i++){
                allRolesInString = allRolesInString + "," + " "+roles.get(i).roleName;

            }

        }
        return allRolesInString;
    }
}