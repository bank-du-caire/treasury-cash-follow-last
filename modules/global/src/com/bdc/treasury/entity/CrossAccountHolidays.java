package com.bdc.treasury.entity;

import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

@Table(name = "TRE_CROSS_ACCOUNT_HOLIDAYS")
@Entity(name = "treasury_CrossAccountHolidays")
public class CrossAccountHolidays extends StandardEntity {
    private static final long serialVersionUID = -1922733773478302389L;

    @Column(name = "DAY_")
    @Pattern(message = "Invalid Day", regexp = "\\b([1-9]|[12][0-9]|3[01])\\b")
    @NotNull(message = "Please Set Day")
    private String day;

    @Column(name = "MONTH_")
    @Pattern(message = "Invalid Month", regexp = "1[0-2]|[1-9]")
    @NotNull(message = "Please Set Month")
    private String month;

    @JoinTable(name = "TR_CROS_ACC_HDAY_CROS_ACC_LINK",
            joinColumns = @JoinColumn(name = "CROSS_ACCOUNT_HOLIDAYS_ID"),
            inverseJoinColumns = @JoinColumn(name = "CROSS_ACCOUNT_ID"))
    @ManyToMany
    @NotNull(message = "Please set Crosspondent Account")
    private List<CrossAccount> crossAccount;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "STATUS")
    private String status;

    public List<CrossAccount> getCrossAccount() {
        return crossAccount;
    }

    public void setCrossAccount(List<CrossAccount> crossAccount) {
        this.crossAccount = crossAccount;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}