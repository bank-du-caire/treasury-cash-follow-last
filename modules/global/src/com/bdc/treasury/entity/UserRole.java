package com.bdc.treasury.entity;

import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.entity.annotation.OnDeleteInverse;
import com.haulmont.cuba.core.global.DeletePolicy;
import com.haulmont.cuba.security.entity.Role;

import javax.persistence.*;
import java.util.UUID;

@Table(name = "TREASURY_USER_ROLE")
@Entity(name = "treasury_UserRole")
public class UserRole extends StandardEntity {
    private static final long serialVersionUID = 6374431817213708719L;
    @ManyToOne
    @JoinColumn(name = "ROLE_ID", insertable = false, updatable = false)
    protected Role role;

    @Column(name = "USERS_ID")
    protected UUID usersId;

    @Column(name = "ROLE_ID")
    protected UUID roleId;

    @Column(name = "ROLE_NAME")
    protected String roleName;


    @OnDeleteInverse(DeletePolicy.UNLINK)
    @OnDelete(DeletePolicy.UNLINK)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USERS_ID", insertable = false, updatable = false)
    protected Users users;

    public UUID getRoleId() {
        return roleId;
    }

    public void setRoleId(UUID roleId) {
        this.roleId = roleId;
    }

    public UUID getUsersId() {
        return usersId;
    }

    public void setUsersId(UUID usersId) {
        this.usersId = usersId;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Users getUsers() {
        return users;
    }


    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Role getRole() {
        return role;
    }
}