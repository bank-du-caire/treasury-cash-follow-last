-- begin TRE_CROSS_ACCOUNT_HOLIDAYS
create table TRE_CROSS_ACCOUNT_HOLIDAYS (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    DAY_ varchar2(255 char),
    MONTH_ varchar2(255 char),
    STATUS varchar2(255 char),
    --
    primary key (ID)
)^
-- end TRE_CROSS_ACCOUNT_HOLIDAYS
-- begin TREASURY_TREASURY_CTRL
create table TREASURY_TREASURY_CTRL (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    CORRESPONDENT_NAME_ID varchar2(32),
    BALANCE number(19, 2),
    TOTAL_DEBIT number(19, 2),
    TOTAL_CREDIT number(19, 2),
    NOTE varchar2(255 char),
    diff_Debit_Credit number(19, 2),
    --
    primary key (ID)
)^
-- end TREASURY_TREASURY_CTRL
-- begin TREASURY_CLOSED_CROSS_ACCOUNT
create table TREASURY_CLOSED_CROSS_ACCOUNT (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    VALUE_DATE date not null,
    STATE varchar2(50 char) not null,
    --
    primary key (ID)
)^
-- end TREASURY_CLOSED_CROSS_ACCOUNT
-- begin TREASURY_BALANCES_LIST
create table TREASURY_BALANCES_LIST (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    EXLUDED_AMOUNT number(19, 2),
    EXLUDED_AMOUNT_DISCRIPTION varchar2(255 char),
    UNKOWN_FUNDS number(19, 2),
    UNKOWN_FUNDS_DISCRIPTION varchar2(255 char),
    OPENINGBALANCE_ID varchar2(32),
    --
    primary key (ID)
)^
-- end TREASURY_BALANCES_LIST
-- begin TREASURY_ATTACHEMENT
create table TREASURY_ATTACHEMENT (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    FILE_ID_ID varchar2(32),
    FILE_NAME varchar2(255 char),
    transaction_ID varchar2(32),
    transaction_Code varchar2(255 char),
    --
    primary key (ID)
)^
-- end TREASURY_ATTACHEMENT
-- begin TRANSACTIONS_TRANSFERED
create table TRANSACTIONS_TRANSFERED (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    CURRENCY_ID varchar2(32) not null,
    FROM_CROSS_ACCOUNT_ID varchar2(32),
    RATE number(19, 2),
    TO_CROSS_ACCOUNT_ID varchar2(32),
    TRANSFERED_BALANCE number(19, 2) not null,
    VALUE_DATE date not null,
    STATUS varchar2(255 char),
    --
    primary key (ID)
)^
-- end TRANSACTIONS_TRANSFERED
-- begin TREASURY_CROSS_ACCOUNT
create table TREASURY_CROSS_ACCOUNT (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    CURRENCY_ID varchar2(32),
    SHORT_NAME varchar2(255 char),
    CPTY_ID number(10),
    MINMUM_BALANCE number(19, 2),
    IS_MAIN_ACCOUNT char(1),
    NAME varchar2(255 char),
    NUMBER_ number(10),
    BIC varchar2(255 char),
    CORP_GL varchar2(255 char),
    RET_GL varchar2(255 char),
    CREATION_DATE date,
    STATUS varchar2(255 char) not null,
    --
    primary key (ID)
)^
-- end TREASURY_CROSS_ACCOUNT
-- begin TREASURY_USER_ROLE
create table TREASURY_USER_ROLE (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    USERS_ID varchar2(32),
    ROLE_ID varchar2(32),
    ROLE_NAME varchar2(255 char),
    --
    primary key (ID)
)^
-- end TREASURY_USER_ROLE
-- begin TREASURY_CURRENCY
create table TREASURY_CURRENCY (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    NAME varchar2(255 char),
    CODE varchar2(255 char) not null,
    SYMBOL varchar2(255 char) not null,
    --
    primary key (ID)
)^
-- end TREASURY_CURRENCY
-- begin TREASURY_USERS
create table TREASURY_USERS (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    LOGIN varchar2(255 char) not null,
    LAST_LOGIN_DATE timestamp,
    STATUS varchar2(50 char),
    GROUP_ID varchar2(32) not null,
    ACTIVE char(1),
    ALL_ROLES_IN_STRING varchar2(1000 char),
    --
    primary key (ID)
)^
-- end TREASURY_USERS
-- begin TREASURY_TRANSACTION
create table TREASURY_TRANSACTION (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    ACCOUNT_ID varchar2(32) not null,
    CREATION_DATE date,
    STATUS varchar2(255 char) not null,
    GROUP_ID varchar2(32),
    REM_ID number(19, 2),
    GROUP_NAME varchar2(255 char),
    CURRENCY_ID varchar2(32) not null,
    DEPT number(19, 2),
    CREDIT number(19, 2),
    VALUE_DATE date,
    REFERENCE_NUMBER varchar2(255 char),
    DESCRIPTION varchar2(255 char),
    transactions_Transfered varchar2(32),
    --
    primary key (ID)
)^
-- end TREASURY_TRANSACTION
-- begin TREASURY_TREASURY_FRONT_OFFICE
create table TREASURY_TREASURY_FRONT_OFFICE (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    CURRENCY_ID varchar2(32),
    DESCRIPTION varchar2(255 char),
    DEPIT number(19, 2),
    RATE number(19, 2),
    CREDIT number(19, 2),
    STATE varchar2(255 char),
    TOTAL_EXCLUDE number(19, 2),
    TOTAL_UNKOWN_FUNDS number(19, 2),
    ACCOUNT_ID varchar2(32) not null,
    VALUE_DATE date,
    TRANSACTION_NAME varchar2(255 char),
    FINAL_BALANCE number(19, 2),
    OPENING_BALANCE number(19, 2),
    DEAL_ID varchar2(255 char),
    CLOSING number(19, 2),
    --
    primary key (ID)
)^
-- end TREASURY_TREASURY_FRONT_OFFICE
-- begin TREASURY_BALANCE
create table TREASURY_BALANCE (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    CROSS_ACCOUNT_ID varchar2(32) not null,
    STATEMENT_DATE date,
    CLOSING_DATE date,
    DEPIT_CREDIT_VALUE number(19, 2),
    CREATION_DATE date,
    GROUP_ID varchar2(32),
    BALANCE number(19, 2),
    EXLUDED_AMOUNT number(19, 2),
    DEPT number(19, 2),
    CREDIT number(19, 2),
    UNKOWN_FUNDS number(19, 2),
    CURRENCY_ID varchar2(32) not null,
    STATUS varchar2(255 char) not null,
    DEPIT_CREDIT_STATE varchar2(255 char) not null,
    DESCRIPTION varchar2(255 char),
    --
    primary key (ID)
)^
-- end TREASURY_BALANCE
-- begin CLOSED_CROSS_ACC_GROUP_LINK
create table CLOSED_CROSS_ACC_GROUP_LINK (
    CLOSED_CROSS_ACCOUNT_ID varchar2(32),
    GROUP_ID varchar2(32),
    primary key (CLOSED_CROSS_ACCOUNT_ID, GROUP_ID)
)^
-- end CLOSED_CROSS_ACC_GROUP_LINK
-- begin CLOSED_ACCT_CROSS_ACCT_LINK
create table CLOSED_ACCT_CROSS_ACCT_LINK (
    CROSS_ACCOUNT_ID varchar2(32),
    CLOSED_CROSS_ACCOUNT_ID varchar2(32),
    primary key (CROSS_ACCOUNT_ID, CLOSED_CROSS_ACCOUNT_ID)
)^
-- end CLOSED_ACCT_CROSS_ACCT_LINK
-- begin TR_CROS_ACC_HDAY_CROS_ACC_LINK
create table TR_CROS_ACC_HDAY_CROS_ACC_LINK (
    CROSS_ACCOUNT_ID varchar2(32),
    CROSS_ACCOUNT_HOLIDAYS_ID varchar2(32),
    primary key (CROSS_ACCOUNT_ID, CROSS_ACCOUNT_HOLIDAYS_ID)
)^
-- end TR_CROS_ACC_HDAY_CROS_ACC_LINK
