package com.bdc.treasury.service;

import com.bdc.treasury.entity.CheckerStatus;
import com.bdc.treasury.entity.CrossAccount;
import com.bdc.treasury.entity.CrossAccountHolidays;
import com.haulmont.cuba.core.global.DataManager;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.List;

@Service(CrossAccountHolidayService.NAME)
public class CrossAccountHolidayServiceBean implements CrossAccountHolidayService {

    @Inject
    private DataManager dataManager;

    @Override
    public boolean checkHolidaysOfCrossAccount(CrossAccount crossAccount, LocalDate valueDate) {
        String day = String.valueOf(valueDate.getDayOfMonth());
        String month = String.valueOf(valueDate.getMonthValue());
        List<CrossAccountHolidays> crossAccountHolidaysList = dataManager.load(CrossAccountHolidays.class)
                .query("select e from treasury_CrossAccountHolidays e where e.crossAccount =:crossAccount and e.day =:day and e.month =:month and e.status =:status")
                .parameter("crossAccount", crossAccount)
                .parameter("day", day)
                .parameter("month", month)
                .parameter("status", CheckerStatus.VALIDATED.name())
                .view("crossAccountHolidays-view")
                .list();

        if (crossAccountHolidaysList.size() == 0) {
            return false;
        } else {
            return true;
        }

    }
}