package com.bdc.treasury.service;

import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.security.entity.User;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Service(UserService.NAME)
public class UserServiceBean implements UserService {

    @Inject
    private UserSessionSource userSessionSource;

    @Override
    public List<String> getUserRoles() {
        return userSessionSource.getUserSession().getUser()
                .getUserRoles()
                .stream()
                .filter(userRole -> userRole.getRole() != null)
                .map(userRole -> userRole.getRole().getName())
                .collect(Collectors.toList());
    }

    @Override
    public User getUser() {
        return userSessionSource.getUserSession().getUser();
    }
}