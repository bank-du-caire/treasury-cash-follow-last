package com.bdc.treasury.service;

import com.bdc.treasury.entity.UserRole;
import com.bdc.treasury.entity.Users;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Query;
import com.haulmont.cuba.core.Transaction;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.security.entity.Role;
import com.haulmont.cuba.security.entity.User;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service(BaseService.NAME)
public class BaseServiceBean implements BaseService {
    @Inject
    private Persistence persistence;
    @Inject
    private Logger log;
    @Inject
    private DataManager dataManager;

    public List<UserRole> loadUserRoles(Users user) {
        Transaction tx = persistence.createTransaction();
        List<UserRole> resultList = null;
        try {
            EntityManager em = persistence.getEntityManager();
            Query f = em.createQuery();
            StringBuilder query = new StringBuilder();
            query.append(" select e from treasury_UserRole e where e.usersId =:usersID");
            resultList = em.createQuery(query.toString())
                    .setParameter("usersID", user.getId())
                    .getResultList();
            //log.info("resultList of roles to be deleted is   "+resultList.toString());
            if (resultList != null && resultList.size() != 0 && resultList.get(0) != null)
                return resultList;
        } catch (Exception e) {
            e.printStackTrace();
            resultList = null;
        } finally {
            tx.end();
        }
        return resultList;
    }

    @Override
    public Users addEditUser(Users user, List<UserRole> UserRoles) {
        log.info("creating user role ");
        Users tempUser = null;
        UserRole userRol = null;
        try {
            tempUser = dataManager.commit(user);
            log.info("user committed *************************************");
            for ( UserRole role : UserRoles) {
                dataManager.commit(role);
                log.info("user role committed  ************************************* ");
            }
        } catch (Exception e) {
            e.getStackTrace();
            e.printStackTrace();
        }
        return tempUser;
    }
    @Override
    public User addEditSecUser(User user, List<com.haulmont.cuba.security.entity.UserRole> UserRoles) {
        log.info("creating and updating roles user role ");
        Transaction tx = persistence.createTransaction();
        User tempUser = null;
        try {

            EntityManager em = persistence.getEntityManager();
            em.merge(user);
            log.info("Sec user merged *************************************");
            List<Role> roles = dataManager.load(Role.class).query(" select e.role from sec$UserRole e  where e.user.id =?1 and e.role is not null", user.getId()).list();
            List<com.haulmont.cuba.security.entity.UserRole> newUserRoles = UserRoles.stream().filter(userRole -> roles.stream().noneMatch(role -> userRole.getRole().getId().equals(role.getId()))).collect(Collectors.toList());
            log.info(" newUserRoles  *************************************" + newUserRoles.stream().map(com.haulmont.cuba.security.entity.UserRole::getRoleName).collect(Collectors.toList()).toString());


            newUserRoles.stream().forEach(e3 -> em.persist(e3));
            log.info("Sec UserRoles merged *************************************");

            tempUser = user;

            tx.commit();
            log.info("transaction has been committed  ************************************");

        } catch (Exception e) {
            e.getStackTrace();
            e.printStackTrace();
            tx.close();
        }
        tx.commit();
        return tempUser;
    }

    @Override
    public List<com.haulmont.cuba.security.entity.UserRole> CheckSecUserRoleDel(User user, List<com.haulmont.cuba.security.entity.UserRole> UserRoles) {
        Transaction tx = persistence.createTransaction();
        List<com.haulmont.cuba.security.entity.UserRole> resultList = null;
        try {
            EntityManager em = persistence.getEntityManager();
            Query f = em.createQuery();
            StringBuilder query = new StringBuilder();
            List rolesIds = UserRoles.stream().map(e -> e.getRole().getId()).collect(Collectors.toList());
            query.append(" select e from sec$UserRole e where e.user.id =:USERS_ID and e.role is not null and " +
                    " e.role.id not in :userRoles ");
            log.info(" list of user roles IDs " + rolesIds.toString());
            resultList = em.createQuery(query.toString())
                    .setParameter("USERS_ID", user.getId())
                    .setParameter("userRoles", rolesIds)
                    .getResultList();
            log.info("resultList of roles to be deleted is   " + resultList.toString());
            if (resultList != null && resultList.size() != 0 && resultList.get(0) != null) {
                return resultList;
            }
        } catch (Exception e) {
            e.printStackTrace();
            resultList = null;
        } finally {
            tx.end();
        }
        return resultList;
    }

    @Override
    public User getSecUser(UUID userId) {
        Transaction tx = persistence.createTransaction();
        com.haulmont.cuba.security.entity.User user = dataManager.create(User.class);
        try {
            EntityManager em = persistence.getEntityManager();
            Query f = em.createQuery();
            StringBuilder query = new StringBuilder();
            query.append(" select e from sec$User e where e.id =:USERS_ID ");
            user = (User) em.createQuery(query.toString())
                    .setParameter("USERS_ID", userId)
                    .getFirstResult();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            tx.end();
        }
        return user;
    }

    @Override
    public User updateSecUserAndRoles(User user, List<com.haulmont.cuba.security.entity.UserRole> UserRoles) {
        User tempUser = null;
        List<com.haulmont.cuba.security.entity.UserRole> deletedUserRoles = null;
        try {
            deletedUserRoles = CheckSecUserRoleDel(user, UserRoles);
            if (deletedUserRoles != null) {
                log.info("delete role");
                deletedUserRoles.stream().forEach(e -> dataManager.remove(e));
            }
            tempUser = this.addEditSecUser(user, UserRoles);
        } catch (Exception e) {
            e.getStackTrace();
            e.printStackTrace();
        }
        return tempUser;
    }

    @Override
    public List< UserRole> CheckuserRoleDel(Users user, List< UserRole> UserRoles) {
        Transaction tx = persistence.createTransaction();
        List< UserRole> resultList = null;
        try {

            EntityManager em = persistence.getEntityManager();
            Query f = em.createQuery();
            StringBuilder query = new StringBuilder();
            List suerRolesIds = UserRoles.stream().map(e -> e.getId()).collect(Collectors.toList());
            query.append(" select e from treasury_UserRole e where e.usersId =:usersID and e.id not in :userRoles ");

            resultList = em.createQuery(query.toString())
                    .setParameter("usersID", user.getId())
                    .setParameter("userRoles", suerRolesIds)
                    .getResultList();
            log.info("resultList of roles to be deleted is   " + resultList.toString());

            if (resultList != null && resultList.size() != 0 && resultList.get(0) != null) {
                return resultList;
            }
        } catch (Exception e) {
            e.printStackTrace();
            resultList = null;
        } finally {
            tx.end();
        }
        return resultList;
    }

    @Override
    public Users updateUserAndRoles(Users user, List<UserRole> UserRoles) {
        Users tempUser = null;
        List< UserRole> deletedUserRoles = null;
        try {
            deletedUserRoles = CheckuserRoleDel(user, UserRoles);
            if (deletedUserRoles != null) {
                deletedUserRoles.stream().forEach(e -> dataManager.remove(e));
            }
            tempUser = this.addEditUser(user, UserRoles);
        } catch (Exception e) {
            e.getStackTrace();
            e.printStackTrace();
        }
        return tempUser;
    }

    @Override
    public List<Date> getLastLogoutDate(UUID id) {
        Transaction tx = persistence.createTransaction();
        try {
            String ID = id.toString().replace("-", "");
            List<Date> lastlogout = new ArrayList<>();
            EntityManager em = persistence.getEntityManager();
            Query query = em.createNativeQuery("SELECT max(C.STARTED_TS) FROM SEC_SESSION_LOG C WHERE (C.USER_ID = ?1) ");
            query.setParameter(1, ID);
            lastlogout = query.getResultList();
            return lastlogout;
        } finally {
            tx.end();
        }
    }
}