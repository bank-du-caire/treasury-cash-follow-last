package com.bdc.treasury.service;

import com.bdc.treasury.entity.*;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Transaction;
import com.haulmont.cuba.core.global.DataManager;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Service(OpeningBalanceValidationService.NAME)
public class OpeningBalanceValidationServiceBean implements OpeningBalanceValidationService {

    @Inject
    private DataManager dataManager;
    @Inject
    private Persistence persistence;

    @Override
    public boolean validateOpenBalanceExist(CrossAccount crossAccount, LocalDate creationDate) {
        List<Balance> balanceList = dataManager.load(Balance.class)
                .query("select e from treasury_Balance e where e.crossAccount =:crossAccount and e.creationDate =:creationDate and e.status =:status")
                .parameter("crossAccount", crossAccount)
                .parameter("creationDate", creationDate)
                .parameter("status", CheckerStatus.VALIDATED.name())
              //  .view("balance-view")
                .list();
        if (balanceList.size() == 0) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void updateBalance(Balance balance) {
        Transaction tx = persistence.createTransaction();
        try {
            EntityManager em = persistence.getEntityManager();
            em.merge(balance);
        } catch (Exception e) {
            e.getStackTrace();
        }
        tx.commit();
        tx.end();
    }
}