package com.bdc.treasury.service;

import com.bdc.treasury.entity.CrossAccount;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Query;
import com.haulmont.cuba.core.Transaction;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service(DealsService.NAME)
public class DealsServiceBean implements DealsService {

    @Inject
    private Persistence persistence;

    @Override
    public List getDeals(CrossAccount crossAccount, LocalDate startDate, LocalDate endDate) {
        Transaction tx = persistence.createTransaction();
        List transactionList = new ArrayList<>();
        try {
            Integer CPTY_ID = (crossAccount.getCptyId() != null ? crossAccount.getCptyId() : 0);
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            query.append("select e.VALUE_DATE , SUM(CASE WHEN e.AMOUNT > 0 THEN e.AMOUNT ELSE 0 END) POSITIVE_BALANCE , SUM(CASE WHEN e.AMOUNT < 0 THEN e.AMOUNT ELSE 0 END) NEGATIVE_BALANCE from ROOT.DEALS e  where e.CURRENCY_CODE like ?1 and e.VALUE_DATE BETWEEN ?2 AND ?3  GROUP BY e.VALUE_DATE");
            transactionList = em.createNativeQuery(query.toString())
                    .setParameter(1, crossAccount.getCurrency().getCode())
                    .setParameter(2, startDate)
                    .setParameter(3, endDate)
                    .getResultList();

        } finally {
            tx.end();

        }


        tx.end();

        return transactionList;

    }

    @Override
    public List getDeals(CrossAccount crossAccount, LocalDate startDate) {
        Transaction tx = persistence.createTransaction();
        List transactionList = new ArrayList<>();
        try {
            Integer CPTY_ID = (crossAccount.getCptyId() != null ? crossAccount.getCptyId() : 0);
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            //(CASE WHEN d.TRANSACTION_TYPE = ?10 THEN d.AMOUNT ELSE 0 END) CREDIT\n" +
            //                    "    ,(CASE WHEN d.TRANSACTION_TYPE = ?11 THEN d.AMOUNT ELSE 0 END) DEPT\n"

           /* query.append("select e.AMOUNT , e.CURRENCY_CODE , e.VALUE_DATE ,e.DESCRIPTION , to_char(e.DEAL_ID),e.CURRENCY_CODE,e.RATE,(CASE WHEN e.TRANSACTION_TYPE = ?3 THEN e.AMOUNT ELSE 0 END) CREDIT,(CASE WHEN e.TRANSACTION_TYPE = ?4 THEN e.AMOUNT ELSE 0 END) DEPT from DEALS e where e.CURRENCY_CODE like ?1 and trunc(e.VALUE_DATE) = ?2 ");
            transactionList = em.createNativeQuery(query.toString())
                    .setParameter(1, crossAccount.getCurrency().getCode())
                    .setParameter(2, startDate)
                    .setParameter(3,"Credit")
                    .setParameter(4,"Debit")
                    .getResultList();*/
            query.append("select kd.AMOUNT , kd.CURRENCY_CODE , kd.VALUE_DATE ,kd.DESCRIPTION , to_char(kd.DEAL_ID),kd.CURRENCY_CODE,kd.RATE,(CASE WHEN kd.TRANSACTION_TYPE = ?3 THEN kd.AMOUNT ELSE 0 END) CREDIT,(CASE WHEN kd.TRANSACTION_TYPE = ?4 THEN kd.AMOUNT ELSE 0 END) DEPT ,kd.FOLDER_NAME , kd.COUNTER_PART_NAME from kondor_deals kd where kd.CURRENCY_CODE like ?1 and trunc(kd.VALUE_DATE) =?2  and " +
                    "kd.ID in ( select kd1.ID from Kondor_deals kd1 where kd1.DEAL_ID = kd.DEAL_ID and kd1.CAPTURE_DATE =( select max(kd2.CAPTURE_DATE) from Kondor_deals kd2\n" +
                    "\n" +
                    "where kd2.DEAL_ID =kd1.DEAL_ID and kd2.TRANSACTION_TYPE = kd1.TRANSACTION_TYPE and kd2.PAYMENT_INTERNAL_STATUS !='C' and kd2.VALIDATION_STATUS ='Y' and kd2.EVENT_ACTION !='D' and kd2.COUNTER_PART_NAME !='CAIRO INTERNATIONAL BANK- UGANDA' ) )\n" +
                    "\n" +
                    "and kd.ACCOUNT_SHORT_NAME = ?5 \n" +
                    "and kd.PAYMENT_INTERNAL_STATUS !='C'\n" +
                    "\n" +
                    "and kd.VALIDATION_STATUS ='Y'\n" +
                    "and kd.COUNTER_PART_NAME !='CAIRO INTERNATIONAL BANK- UGANDA'" +
                    "and kd.EVENT_ACTION !='D'");
            transactionList = em.createNativeQuery(query.toString())
                    .setParameter(1, crossAccount.getCurrency().getCode())
                    .setParameter(2, startDate)
                    .setParameter(3, "Credit")
                    .setParameter(4, "Debit")
                    .setParameter(5, crossAccount.getShortName())
                    .getResultList();

        } finally {
            tx.end();

        }


        tx.end();

        return transactionList;

    }


    @Override
    public List getDealsForTreasuryCTRL(CrossAccount crossAccount, LocalDate startDate) {
        Transaction tx = persistence.createTransaction();
        List transactionList = new ArrayList<>();
        try {
            Integer CPTY_ID = (crossAccount.getCptyId() != null ? crossAccount.getCptyId() : 0);
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            //(CASE WHEN d.TRANSACTION_TYPE = ?10 THEN d.AMOUNT ELSE 0 END) CREDIT\n" +
            //                    "    ,(CASE WHEN d.TRANSACTION_TYPE = ?11 THEN d.AMOUNT ELSE 0 END) DEPT\n"

           /* query.append("select e.AMOUNT , e.CURRENCY_CODE , e.VALUE_DATE ,e.DESCRIPTION , to_char(e.DEAL_ID),e.CURRENCY_CODE,e.RATE,(CASE WHEN e.TRANSACTION_TYPE = ?3 THEN e.AMOUNT ELSE 0 END) CREDIT,(CASE WHEN e.TRANSACTION_TYPE = ?4 THEN e.AMOUNT ELSE 0 END) DEPT from DEALS e where e.CURRENCY_CODE like ?1 and trunc(e.VALUE_DATE) = ?2 ");
            transactionList = em.createNativeQuery(query.toString())
                    .setParameter(1, crossAccount.getCurrency().getCode())
                    .setParameter(2, startDate)
                    .setParameter(3,"Credit")
                    .setParameter(4,"Debit")
                    .getResultList();*/
            query.append("select kd.AMOUNT , kd.CURRENCY_CODE , kd.VALUE_DATE ,kd.DESCRIPTION , to_char(kd.DEAL_ID),kd.CURRENCY_CODE,kd.RATE,(CASE WHEN kd.TRANSACTION_TYPE = ?3 THEN kd.AMOUNT ELSE 0 END) CREDIT,(CASE WHEN kd.TRANSACTION_TYPE = ?4 THEN kd.AMOUNT ELSE 0 END) DEPT ,kd.FOLDER_NAME , kd.COUNTER_PART_NAME from kondor_deals kd where kd.CURRENCY_CODE like ?1 " +
                    "and trunc(kd.VALUE_DATE) =?2  and " +
                    "kd.ID in ( select kd1.ID from Kondor_deals kd1 where kd1.DEAL_ID = kd.DEAL_ID and kd1.CAPTURE_DATE =( select max(kd2.CAPTURE_DATE) from Kondor_deals kd2\n" +
                    "\n" +
                    "where kd2.DEAL_ID =kd1.DEAL_ID and kd2.TRANSACTION_TYPE = kd1.TRANSACTION_TYPE and kd2.VALIDATION_STATUS ='Y' and kd2.EVENT_ACTION !='D' and kd2.COUNTER_PART_NAME !='CAIRO INTERNATIONAL BANK- UGANDA' ) )\n" +
                    "\n" +
                    "and kd.ACCOUNT_SHORT_NAME = ?5 \n" +
                    "and kd.PAYMENT_INTERNAL_STATUS !='C'\n" +
                    "\n" +
                    "and kd.VALIDATION_STATUS ='Y'\n" +
                    "and kd.EVENT_ACTION !='D'" +
                    "and kd.COUNTER_PART_NAME !='CAIRO INTERNATIONAL BANK- UGANDA'" +
                    "and kd.FOLDER_NAME in ('E_FX_IB','TBILLS_HTC','TBILL_FVOC','TBILL_FVTP')");
            transactionList = em.createNativeQuery(query.toString())
                    .setParameter(1, crossAccount.getCurrency().getCode())
                    .setParameter(2, startDate)
                    .setParameter(3, "Credit")
                    .setParameter(4, "Debit")
                    .setParameter(5, crossAccount.getShortName())
                    .getResultList();

        } finally {
            tx.end();

        }


        tx.end();

        return transactionList;

    }

    @Override
    public List getDealsWithinDurationTest(CrossAccount crossAccount, LocalDate startDate, LocalDate endDate) {
        Transaction tx = persistence.createTransaction();
        List transactionList = new ArrayList<>();
        try {
            Integer CPTY_ID = (crossAccount.getCptyId() != null ? crossAccount.getCptyId() : 0);
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            query.append("select kd.AMOUNT , kd.CURRENCY_CODE , kd.VALUE_DATE ,kd.DESCRIPTION , to_char(kd.DEAL_ID),kd.CURRENCY_CODE,kd.RATE,(CASE WHEN kd.TRANSACTION_TYPE = ?3 THEN kd.AMOUNT ELSE 0 END) CREDIT,(CASE WHEN kd.TRANSACTION_TYPE = ?4 THEN kd.AMOUNT ELSE 0 END) DEPT from kondor_deals kd where kd.CURRENCY_CODE like ?1 and trunc(kd.VALUE_DATE) BETWEEN ?2 and ?5 and " +
                    "kd.ID in ( select kd1.ID from Kondor_deals kd1 where kd1.DEAL_ID = kd.DEAL_ID and kd1.CAPTURE_DATE =( select max(kd2.CAPTURE_DATE) from Kondor_deals kd2\n" +
                    "\n" +
                    "where kd2.DEAL_ID =kd1.DEAL_ID and kd2.TRANSACTION_TYPE = kd1.TRANSACTION_TYPE and kd2.PAYMENT_INTERNAL_STATUS !='C' and kd2.VALIDATION_STATUS ='Y' and kd2.EVENT_ACTION !='D' and kd2.COUNTER_PART_NAME !='CAIRO INTERNATIONAL BANK- UGANDA' ) )\n" +
                    "\n" +
                    "and kd.PAYMENT_INTERNAL_STATUS !='C'\n" +
                    "\n" +
                    "and kd.VALIDATION_STATUS ='Y'\n" +
                    "and kd.COUNTER_PART_NAME !='CAIRO INTERNATIONAL BANK- UGANDA'" +
                    "and kd.ACCOUNT_SHORT_NAME = ?12 \n" +
                    "and kd.EVENT_ACTION !='D'");
            transactionList = em.createNativeQuery(query.toString())
                    .setParameter(1, crossAccount.getCurrency().getCode())
                    .setParameter(2, startDate)
                    .setParameter(5, endDate)
                    .setParameter(3, "Credit")
                    .setParameter(4, "Debit")
                    .setParameter(12, crossAccount.getShortName())
                    .getResultList();

        } finally {
            tx.end();

        }


        tx.end();

        return transactionList;

    }

    @Override
    public List getDealsWithinDuration(CrossAccount crossAccount, LocalDate startDate, LocalDate endDate) {
        Transaction tx = persistence.createTransaction();
        List transactionList = new ArrayList<>();
        try {
            Integer CPTY_ID = (crossAccount.getCptyId() != null ? crossAccount.getCptyId() : 0);
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            //(CASE WHEN d.TRANSACTION_TYPE = ?10 THEN d.AMOUNT ELSE 0 END) CREDIT\n" +
            //                    "    ,(CASE WHEN d.TRANSACTION_TYPE = ?11 THEN d.AMOUNT ELSE 0 END) DEPT\n"

            query.append("select e.AMOUNT , e.CURRENCY_CODE , e.VALUE_DATE ,e.DESCRIPTION , to_char(e.DEAL_ID),e.CURRENCY_CODE,e.RATE,(CASE WHEN e.TRANSACTION_TYPE = ?3 THEN e.AMOUNT ELSE 0 END) CREDIT,(CASE WHEN e.TRANSACTION_TYPE = ?4 THEN e.AMOUNT ELSE 0 END) DEPT from DEALS e where e.CURRENCY_CODE like ?1 and trunc(e.VALUE_DATE) BETWEEN ?2 and ?5 ");
            transactionList = em.createNativeQuery(query.toString())
                    .setParameter(1, crossAccount.getCurrency().getCode())
                    .setParameter(2, startDate)
                    .setParameter(5, endDate)
                    .setParameter(3, "Credit")
                    .setParameter(4, "Debit")
                    .getResultList();

        } finally {
            tx.end();

        }


        tx.end();

        return transactionList;

    }

    @Override
    public List<Timestamp> getLastValueDateOfDeals(CrossAccount crossAccount, LocalDate startDate) {
        Transaction tx = persistence.createTransaction();
        List<Timestamp> transactionList = new ArrayList<>();
        try {
            Integer CPTY_ID = (crossAccount.getCptyId() != null ? crossAccount.getCptyId() : 0);
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            query.append("select e.VALUE_DATE from DEALS e where e.CURRENCY_CODE like ?1 and e.VALUE_DATE < ?2  order by e.VALUE_DATE DESC");
            transactionList = em.createNativeQuery(query.toString())
                    .setParameter(1, crossAccount.getCurrency().getCode())
                    .setParameter(2, startDate)
                    .setMaxResults(30)
                    .getResultList();

        } finally {
            tx.end();

        }


        tx.end();

        return transactionList;

    }
}