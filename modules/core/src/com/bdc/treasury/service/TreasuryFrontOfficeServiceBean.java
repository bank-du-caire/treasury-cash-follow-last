package com.bdc.treasury.service;

import com.bdc.treasury.entity.*;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Query;
import com.haulmont.cuba.core.global.DataManager;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service(TreasuryFrontOfficeService.NAME)
public class TreasuryFrontOfficeServiceBean implements TreasuryFrontOfficeService {

    @Inject
    private DataManager dataManager;
    @Inject
    private Persistence persistence;
    @Inject
    private RemTransactionService remTransactionService;
    @Inject
    private DealsService dealsService;

    @Override
    public BigDecimal getClosedBalanceForTreasuryCtrl(CrossAccount crossAccount, LocalDate date) {
        // First step --> Get last date creation of opening balance for this account
        // validate Exist opening balance for this date

        Timestamp lastOpeningBalanceDateTS = null;
        LocalDate lastOpeningBalanceDate = null;
        if (!validateExistOpeningBalance(crossAccount, date)) {
            lastOpeningBalanceDateTS = getLastCreationDateOfOpeningBalance(crossAccount, date);
            if (lastOpeningBalanceDateTS != null) {
                lastOpeningBalanceDate = (lastOpeningBalanceDateTS.toLocalDateTime().toLocalDate());
            }
        } else {
            lastOpeningBalanceDate = date;
        }
        if (lastOpeningBalanceDate != null) {
            List<Balance> balances = loadOpeningBalancesList(crossAccount, lastOpeningBalanceDate);
            /*if (!validateExistOpeningBalance(crossAccount, date))
                lastOpeningBalanceDate = lastOpeningBalanceDate.plusDays(1);*/
            LocalDate lastStatmentDate = null;
            for (Balance balance : balances) {
                lastStatmentDate = balance.getStatementDate().plusDays(1);
            }
            if (lastStatmentDate != null) {
                List<Transaction> transactions = loadTransactionList(crossAccount, lastStatmentDate, date);
                BigDecimal minimumBalance = crossAccount.getMinimumBalance();
                BigDecimal totalBalance = BigDecimal.ZERO;
                BigDecimal transactionsBalance = BigDecimal.ZERO;
                LocalDate now = LocalDate.now();
                // To Do filter transaction with date from UI // by from date and to date
                List<Transaction> transactionList = new ArrayList<>();
                //get transaction list without Value date
//            transactionList = dataManager.load(Transaction.class)
//                    .query("select e from treasury_Transaction e where e.account =:account and  e.valueDate is null and e.deleteTs is null ")
//                    .parameter("account", crossAccount)
//                    .view("transaction-view")
//                    .list();
                // transaction without value date
                transactionList = dataManager.load(Transaction.class)
                        .query("select e from treasury_Transaction e where e.account =:account and  e.valueDate is null and e.deleteTs is null and (e.status = :stat1 or e.status = :stat2) ")
                        .parameter("account", crossAccount)
                        .parameter("stat1", CheckerStatus.VALIDATED.name())
                        .parameter("stat2", Status.TEMPORARY.name())
                        .view("transaction-view")
                        .list();
                for (Transaction transaction : transactionList) {
                    transactions.add(transaction);
                }
                for (Transaction transaction : transactions) {
                    transactionsBalance = transactionsBalance.add(transaction.getCredit() != null ? transaction.getCredit() : BigDecimal.ZERO).subtract(transaction.getDept() != null ? transaction.getDept() : BigDecimal.ZERO);
                }
                for (Balance balance : balances) {
                    BigDecimal totalExludedAmount = BigDecimal.ZERO;
                    BigDecimal totalUnkownFunds = BigDecimal.ZERO;
                    for (BalancesList balancesList : balance.getBalancesList()) {
                        totalExludedAmount = totalExludedAmount.add(balancesList.getExludedAmount() != null ? balancesList.getExludedAmount() : BigDecimal.ZERO);
                        totalUnkownFunds = totalUnkownFunds.add(balancesList.getUnkownFunds() != null ? balancesList.getUnkownFunds() : BigDecimal.ZERO);
                    }
                    // balance.getBalance() = Opening balance;
                    BigDecimal openBalance = balance.getBalance();
                    totalBalance = (openBalance != null ? openBalance : BigDecimal.ZERO).subtract(totalUnkownFunds != null ? totalUnkownFunds : BigDecimal.ZERO).subtract(totalExludedAmount != null ? totalExludedAmount : BigDecimal.ZERO);
                }
                //  calculate Remmitance and Kondor total balance  ///
                TreasuryFrontOffice remTransactionTotreasuryFrontOffice = dataManager.create(TreasuryFrontOffice.class);
                //lastOpeningBalanceDate.minusDays(1);

            /*List treasuryFrontOfficeRemitanceList = remTransactionService.getRemitanceTransactionWithinDuration(crossAccount, lastOpeningBalanceDate, date);
            for (Object o : treasuryFrontOfficeRemitanceList) {
                Object[] raw = ((Object[]) o);
                //0 id
                //1

                // 20-06-2021 Test // comment//**********************************************************************************************
               // transactionsBalance = transactionsBalance.add((BigDecimal) raw[0] != null ? (BigDecimal) raw[0] : BigDecimal.ZERO);
            }*/

                // لليوم السابق( رصيد إقفال الخزانة (إجمالى العمليات التى تمت على Kondor
                BigDecimal ClosedTreasuryBalance = BigDecimal.ZERO;
            /*List<Timestamp> lastValueDateOfDealsList = dealsService.getLastValueDateOfDeals(crossAccount, date);
            if (lastValueDateOfDealsList.size() > 0) {
                Timestamp timestamp = lastValueDateOfDealsList.get(0);
                LocalDate lastValueDate = timestamp.toLocalDateTime().toLocalDate();
                List dealsOfLastDayList = dealsService.getDeals(crossAccount, lastValueDate);
                for (Object o : dealsOfLastDayList) {
                    Object[] raw = ((Object[]) o);
                    ClosedTreasuryBalance = ClosedTreasuryBalance.add((BigDecimal) raw[7] != null ? (BigDecimal) raw[7] : BigDecimal.ZERO).subtract((BigDecimal) raw[8] != null ? (BigDecimal) raw[8] : BigDecimal.ZERO);
                }
            }*/
                List treasuryFrontOfficeDealsList = dealsService.getDealsForTreasuryCTRL(crossAccount, lastStatmentDate);
                for (Object o : treasuryFrontOfficeDealsList) {
                    Object[] raw = ((Object[]) o);
                    transactionsBalance = transactionsBalance.add((BigDecimal) raw[7] != null ? (BigDecimal) raw[7] : BigDecimal.ZERO).subtract((BigDecimal) raw[8] != null ? (BigDecimal) raw[8] : BigDecimal.ZERO);
                }
                if (balances.size() > 0) {
                    totalBalance = totalBalance.add(transactionsBalance);
                    totalBalance = totalBalance.subtract(ClosedTreasuryBalance.abs());
                    totalBalance = totalBalance.subtract((minimumBalance != null ? minimumBalance : BigDecimal.ZERO));
                } else {
                    totalBalance = null;
                }
                return totalBalance;
            }
        }
        return BigDecimal.ZERO;
    }

    @Override
    public BigDecimal getClosedBalanceTest(CrossAccount crossAccount, LocalDate date) {
        // First step --> Get last date creation of opening balance for this account
        BigDecimal minimumBalance = crossAccount.getMinimumBalance();
        Timestamp lastOpeningBalanceDateTS = null;
        LocalDate lastOpeningBalanceDate = null;
        if (!validateExistOpeningBalance(crossAccount, date)) {
            lastOpeningBalanceDateTS = getLastCreationDateOfOpeningBalance(crossAccount, date);
            if (lastOpeningBalanceDateTS != null) {
                lastOpeningBalanceDate = (lastOpeningBalanceDateTS.toLocalDateTime().toLocalDate());
            }
        } else {
            lastOpeningBalanceDate = date;
        }
        if (lastOpeningBalanceDateTS != null) {
            lastOpeningBalanceDate = (lastOpeningBalanceDateTS.toLocalDateTime().toLocalDate());
        }
        if (lastOpeningBalanceDate != null) {
            List<Balance> balances = loadOpeningBalancesList(crossAccount, lastOpeningBalanceDate);
            /*if (!validateExistOpeningBalance(crossAccount, date))
                lastOpeningBalanceDate = lastOpeningBalanceDate.plusDays(1);*/
            LocalDate lastStatmentDate = null;
            for (Balance balance : balances) {
                if (balance.getStatementDate() != null)
                    lastStatmentDate = balance.getStatementDate().plusDays(1);
            }
            if (lastStatmentDate != null) {
                List<Transaction> transactions = loadTransactionList(crossAccount, lastStatmentDate, date);
                BigDecimal totalBalance = BigDecimal.ZERO;
                BigDecimal transactionsBalance = BigDecimal.ZERO;
                // To Do filter transaction with date from UI // by from date and to date
                List<Transaction> transactionList = new ArrayList<>();
                //get transaction list without Value date
                transactionList = dataManager.load(Transaction.class)
                        .query("select e from treasury_Transaction e where e.account =:account and  e.valueDate is null and e.deleteTs is null and (e.status = :stat1 or e.status = :stat2) ")
                        .parameter("account", crossAccount)
                        .parameter("stat1", CheckerStatus.VALIDATED.name())
                        .parameter("stat2", Status.TEMPORARY.name())
                        .view("transaction-view")
                        .list();
                for (Transaction transaction : transactionList) {
                    transactions.add(transaction);
                }
                for (Transaction transaction : transactions) {
                    transactionsBalance = transactionsBalance.add(transaction.getCredit() != null ? transaction.getCredit() : BigDecimal.ZERO).subtract(transaction.getDept() != null ? transaction.getDept() : BigDecimal.ZERO);
                }
                for (Balance balance : balances) {
                    BigDecimal totalExludedAmount = BigDecimal.ZERO;
                    BigDecimal totalUnkownFunds = BigDecimal.ZERO;
                    for (BalancesList balancesList : balance.getBalancesList()) {
                        totalExludedAmount = totalExludedAmount.add(balancesList.getExludedAmount() != null ? balancesList.getExludedAmount() : BigDecimal.ZERO);
                        totalUnkownFunds = totalUnkownFunds.add(balancesList.getUnkownFunds() != null ? balancesList.getUnkownFunds() : BigDecimal.ZERO);
                    }
                    // balance.getBalance() = Opening balance;
                    BigDecimal openBalance = balance.getBalance();
                    totalBalance = (openBalance != null ? openBalance : BigDecimal.ZERO).subtract(totalUnkownFunds != null ? totalUnkownFunds : BigDecimal.ZERO).subtract(totalExludedAmount != null ? totalExludedAmount : BigDecimal.ZERO);
                }
                //  calculate Remmitance and Kondor total balance  ///
                //lastOpeningBalanceDate = lastOpeningBalanceDate.minusDays(1);
                List treasuryFrontOfficeDealsList = dealsService.getDealsWithinDurationTest(crossAccount, lastStatmentDate, date);
                for (Object o : treasuryFrontOfficeDealsList) {
                    Object[] raw = ((Object[]) o);
                    transactionsBalance = transactionsBalance.add((BigDecimal) raw[7] != null ? (BigDecimal) raw[7] : BigDecimal.ZERO).subtract((BigDecimal) raw[8] != null ? (BigDecimal) raw[8] : BigDecimal.ZERO);
                }

                if (balances.size() > 0) {
                    totalBalance = totalBalance.add(transactionsBalance).subtract((minimumBalance != null ? minimumBalance : BigDecimal.ZERO));
                } else {
                    totalBalance = null;
                }
                return totalBalance;
            }
        }
        return BigDecimal.ZERO;
    }

    @Override
    public BigDecimal getClosedBalance(CrossAccount crossAccount, LocalDate date) {
        // First step --> Get last date creation of opening balance for this account
        BigDecimal minimumBalance = crossAccount.getMinimumBalance();
        Timestamp lastOpeningBalanceDateTS = null;
        LocalDate lastOpeningBalanceDate = null;
        if (!validateExistOpeningBalance(crossAccount, date)) {
            lastOpeningBalanceDateTS = getLastCreationDateOfOpeningBalance(crossAccount, date);
            if (lastOpeningBalanceDateTS != null) {
                lastOpeningBalanceDate = (lastOpeningBalanceDateTS.toLocalDateTime().toLocalDate());
            }
        } else {
            lastOpeningBalanceDate = date;
        }
        if (lastOpeningBalanceDateTS != null) {
            lastOpeningBalanceDate = (lastOpeningBalanceDateTS.toLocalDateTime().toLocalDate());
        }
        if (lastOpeningBalanceDate != null) {
            List<Balance> balances = loadOpeningBalancesList(crossAccount, lastOpeningBalanceDate);
            /*if (!validateExistOpeningBalance(crossAccount, date))
                lastOpeningBalanceDate = lastOpeningBalanceDate.plusDays(1);*/
            LocalDate lastStatmentDate = null;
            for (Balance balance : balances) {
                lastStatmentDate = balance.getStatementDate().plusDays(1);
            }
            if (lastStatmentDate != null) {
                List<Transaction> transactions = loadTransactionList(crossAccount, lastStatmentDate, date);
                BigDecimal totalBalance = BigDecimal.ZERO;
                BigDecimal transactionsBalance = BigDecimal.ZERO;

                // To Do filter transaction with date from UI // by from date and to date
                List<Transaction> transactionList = new ArrayList<>();
                //get transaction list without Value date
//            transactionList = dataManager.load(Transaction.class)
//                    .query("select e from treasury_Transaction e where e.account =:account and  e.valueDate is null and e.deleteTs is null ")
//                    .parameter("account", crossAccount)
//                    .view("transaction-view")
//                    .list();
                transactionList = dataManager.load(Transaction.class)
                        .query("select e from treasury_Transaction e where e.account =:account and  e.valueDate is null and e.deleteTs is null and (e.status = :stat1 or e.status = :stat2) ")
                        .parameter("account", crossAccount)
                        .parameter("stat1", CheckerStatus.VALIDATED.name())
                        .parameter("stat2", Status.TEMPORARY.name())
                        .view("transaction-view")
                        .list();

                for (Transaction transaction : transactionList) {
                    transactions.add(transaction);
                }
                for (Transaction transaction : transactions) {
                    transactionsBalance = transactionsBalance.add(transaction.getCredit() != null ? transaction.getCredit() : BigDecimal.ZERO).subtract(transaction.getDept() != null ? transaction.getDept() : BigDecimal.ZERO);
                }
                for (Balance balance : balances) {
                    BigDecimal totalExludedAmount = BigDecimal.ZERO;
                    BigDecimal totalUnkownFunds = BigDecimal.ZERO;
                    for (BalancesList balancesList : balance.getBalancesList()) {
                        totalExludedAmount = totalExludedAmount.add(balancesList.getExludedAmount() != null ? balancesList.getExludedAmount() : BigDecimal.ZERO);
                        totalUnkownFunds = totalUnkownFunds.add(balancesList.getUnkownFunds() != null ? balancesList.getUnkownFunds() : BigDecimal.ZERO);
                    }
                    // balance.getBalance() = Opening balance;
                    BigDecimal openBalance = balance.getBalance();
                    totalBalance = (openBalance != null ? openBalance : BigDecimal.ZERO).subtract(totalUnkownFunds != null ? totalUnkownFunds : BigDecimal.ZERO).subtract(totalExludedAmount != null ? totalExludedAmount : BigDecimal.ZERO);
                }
                //  calculate Remmitance and Kondor total balance  ///
                //lastOpeningBalanceDate = lastOpeningBalanceDate.minusDays(1);
                TreasuryFrontOffice remTransactionTotreasuryFrontOffice = dataManager.create(TreasuryFrontOffice.class);
                /*List treasuryFrontOfficeRemitanceList = remTransactionService.getRemitanceTransactionWithinDuration(crossAccount, lastStatmentDate, date);
                for (Object o : treasuryFrontOfficeRemitanceList) {
                    Object[] raw = ((Object[]) o);
                    //0 id
                    //1
                    //test  20-06-2021/*****************************************************************************************************
                    //transactionsBalance = transactionsBalance.add((BigDecimal) raw[0] != null ? (BigDecimal) raw[0] : BigDecimal.ZERO);
                }*/

                List treasuryFrontOfficeDealsList = dealsService.getDealsWithinDuration(crossAccount, lastStatmentDate, date);
                for (Object o : treasuryFrontOfficeDealsList) {
                    Object[] raw = ((Object[]) o);
                    //0 id
                    //1
                    //remTransactionTotreasuryFrontOffice.setAccount((CrossAccount) raw[0]);
                    //ClosedTreasuryBalance.add((BigDecimal) raw[7] != null ? (BigDecimal) raw[7] : BigDecimal.ZERO).subtract((BigDecimal) raw[8] != null ? (BigDecimal) raw[8] : BigDecimal.ZERO);
                    //test  20-06-2021/*****************************************************************************************************
                    transactionsBalance = transactionsBalance.add((BigDecimal) raw[7] != null ? (BigDecimal) raw[7] : BigDecimal.ZERO).subtract((BigDecimal) raw[8] != null ? (BigDecimal) raw[8] : BigDecimal.ZERO);
                }

                if (balances.size() > 0) {
                    totalBalance = totalBalance.add(transactionsBalance).subtract((minimumBalance != null ? minimumBalance : BigDecimal.ZERO));
                } else {
                    totalBalance = null;
                }
                return totalBalance;
            }
        }
        return BigDecimal.ZERO;
    }

    @Override
    public BigDecimal getOpeningBalance(CrossAccount crossAccount, LocalDate date) {
        List<Transaction> transactions = new ArrayList<>();
        List<Balance> balances = new ArrayList<>();
        List treasuryFrontOfficeRemitanceList = new ArrayList();
        List treasuryFrontOfficeDealsList = new ArrayList();
        // last value date with closed Balance
        transactions = loadTransactionListForOoeningBalance(crossAccount, date);
        // transaction without valueDate
        List<Transaction> transactionList = new ArrayList<>();
        transactionList = dataManager.load(Transaction.class)
                .query("select e from treasury_Transaction e where e.account =:account and  e.valueDate is null and e.deleteTs is null ")
                .parameter("account", crossAccount)
                .view("transaction-view")
                .list();
        for (Transaction transaction : transactionList) {
            transactions.add(transaction);
        }
        balances = loadOpeningBalancesList(crossAccount, date);
        treasuryFrontOfficeRemitanceList = remTransactionService.getRemitanceTransaction(crossAccount, date);
        treasuryFrontOfficeDealsList = dealsService.getDeals(crossAccount, date);

        //BigDecimal minimumBalance = crossAccount.getMinimumBalance();
        BigDecimal totalBalance = BigDecimal.ZERO;
        BigDecimal transactionsBalance = BigDecimal.ZERO;
        LocalDate now = LocalDate.now();
        // To Do filter transaction with date from UI // by from date and to date
        for (Transaction transaction : transactions) {
            transactionsBalance = transactionsBalance.add(transaction.getCredit() != null ? transaction.getCredit() : BigDecimal.ZERO).subtract(transaction.getDept() != null ? transaction.getDept() : BigDecimal.ZERO);
        }
        for (Balance balance : balances) {
            BigDecimal totalExludedAmount = BigDecimal.ZERO;
            BigDecimal totalUnkownFunds = BigDecimal.ZERO;
            for (BalancesList balancesList : balance.getBalancesList()) {
                totalExludedAmount = totalExludedAmount.add(balancesList.getExludedAmount() != null ? balancesList.getExludedAmount() : BigDecimal.ZERO);
                totalUnkownFunds = totalUnkownFunds.add(balancesList.getUnkownFunds() != null ? balancesList.getUnkownFunds() : BigDecimal.ZERO);
            }
            // balance.getBalance() = Opening balance;
            BigDecimal openBalance = balance.getBalance();
            totalBalance = (openBalance != null ? openBalance : BigDecimal.ZERO).subtract(totalUnkownFunds != null ? totalUnkownFunds : BigDecimal.ZERO).subtract(totalExludedAmount != null ? totalExludedAmount : BigDecimal.ZERO);
        }
        //  calculate Remmitance and Kondor total balance  ///
        TreasuryFrontOffice remTransactionTotreasuryFrontOffice = dataManager.create(TreasuryFrontOffice.class);
        for (Object o : treasuryFrontOfficeRemitanceList) {
            Object[] raw = ((Object[]) o);
            transactionsBalance = transactionsBalance.add((BigDecimal) raw[0] != null ? (BigDecimal) raw[0] : BigDecimal.ZERO);
        }
        for (Object o : treasuryFrontOfficeDealsList) {
            Object[] raw = ((Object[]) o);

            transactionsBalance = transactionsBalance.add((BigDecimal) raw[7] != null ? (BigDecimal) raw[7] : BigDecimal.ZERO).subtract((BigDecimal) raw[8] != null ? (BigDecimal) raw[8] : BigDecimal.ZERO);
        }
        totalBalance = totalBalance.subtract(transactionsBalance);

        return totalBalance;
    }

    @Override
    public List<Transaction> getTransactionList(CrossAccount crossAccount, LocalDate startDate, LocalDate endDate) {
        return null;
    }

    public List<Transaction> loadTransactionListForOoeningBalance(CrossAccount crossAccount, LocalDate date) {
        List<Transaction> transactionList = new ArrayList<>();
        if (crossAccount != null && date != null) {
            transactionList = dataManager.load(Transaction.class)
                    .query("select e from treasury_Transaction e where e.account =:account and e.valueDate =:startDate  and (e.status = :stat1 or e.status = :stat2)")
                    .parameter("account", crossAccount)
                    .parameter("startDate", date)
                    .parameter("stat1", CheckerStatus.VALIDATED.name())
                    .parameter("stat2", Status.TEMPORARY.name())
                    .view("transaction-view")
                    .list();
        }
        return transactionList;
    }

    public List<Transaction> loadTransactionList(CrossAccount crossAccount, LocalDate date, LocalDate endDate) {
        List<Transaction> transactionList = new ArrayList<>();
        if (crossAccount != null && date != null) {
            transactionList = dataManager.load(Transaction.class)
                    .query("select e from treasury_Transaction e where e.account =:account and e.valueDate between :startDate and :endDate and (e.status = :stat1 or e.status = :stat2)")
                    .parameter("account", crossAccount)
                    .parameter("startDate", date)
                    .parameter("endDate", endDate)
                    .parameter("stat1", CheckerStatus.VALIDATED.name())
                    .parameter("stat2", Status.TEMPORARY.name())
                    .view("transaction-view")
                    .list();
        }
        return transactionList;
    }

    @Override
    public List<LocalDate> getPreValueDateTransactionList(CrossAccount crossAccount, LocalDate date) {
        List<LocalDate> transactionValueDateList = new ArrayList<>();
        com.haulmont.cuba.core.Transaction tx = persistence.createTransaction();
        EntityManager em = persistence.getEntityManager();
        Query f = em.createQuery();
        StringBuilder query = new StringBuilder();
        query.append(" select e.valueDate from treasury_Transaction e where e.account =:account and e.valueDate <:date and (e.status = :stat1 or e.status = :stat2)  ORDER BY e.valueDate DESC");
        try {
            transactionValueDateList = em.createQuery(query.toString()
            ).setParameter("account", crossAccount)
                    .setParameter("date", date)
                    .setParameter("stat1", CheckerStatus.VALIDATED.name())
                    .setParameter("stat2", Status.TEMPORARY.name())
                    .setMaxResults(1)
                    .getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.end();


        return transactionValueDateList;
    }

    @Override
    public List getPreValueDateRemmitanceList(CrossAccount crossAccount, LocalDate localDate) {
        com.haulmont.cuba.core.Transaction tx = persistence.createTransaction();
        List valueDateList = new ArrayList<>();
        try {
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            query.append("select e.VALUE_DATE  from REMMITANCE_TRANSACTIONS e where e.CURRENCY like ?1 and trunc(e.VALUE_DATE) < ?2  and e.STATUS = ?4");
            valueDateList = em.createNativeQuery(query.toString())
                    .setParameter(1, crossAccount.getCurrency().getCode())
                    .setParameter(2, localDate)
                    .setParameter(4, "VALIDATED")
                    .setMaxResults(1)
                    .getResultList();
        } finally {
            tx.end();

        }
        return valueDateList;
    }

    @Override
    public List getPreValueDateKondorList(CrossAccount crossAccount, LocalDate localDate) {
        com.haulmont.cuba.core.Transaction tx = persistence.createTransaction();
        List valueDateList = new ArrayList<>();
        try {
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            query.append("select max(kd.VALUE_DATE)  from KONDOR_DEALS kd where kd.CURRENCY_CODE like ?1 and trunc(kd.VALUE_DATE) < ?2 " +
                    "AND kd.ID in ( select kd1.ID from Kondor_deals kd1 where kd1.DEAL_ID = kd.DEAL_ID and kd1.CAPTURE_DATE =( select max(kd2.CAPTURE_DATE) from Kondor_deals kd2\n" +
                    "\n" +
                    "where kd2.DEAL_ID =kd1.DEAL_ID and kd2.TRANSACTION_TYPE = kd1.TRANSACTION_TYPE ) )" +
                    "\n" +
                    "and kd.PAYMENT_INTERNAL_STATUS !='C'" +
                    "\n" +
                    "and kd.VALIDATION_STATUS ='Y'" +
                    "\n" +
                    "and kd.VALIDATION_STATUS ='Y'" +
                    "\n" +
                    " and kd.ACCOUNT_SHORT_NAME = ?12  order by  kd.VALUE_DATE desc ");

            valueDateList = em.createNativeQuery(query.toString())
                    .setParameter(1, crossAccount.getCurrency().getCode())
                    .setParameter(2, localDate)
                    .setParameter(12, crossAccount.getShortName())
                    .setMaxResults(1)
                    .getResultList();
        } finally {
            tx.end();

        }
        return valueDateList;
    }

    public Timestamp getLastCreationDateOfOpeningBalance(CrossAccount crossAccount, LocalDate date) {
        com.haulmont.cuba.core.Transaction tx = persistence.createTransaction();
        Timestamp lastOpeningDate = null;
        try {
            String id = crossAccount.getUuid().toString().replace("-", "");
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            //select MAX(c.creationDate) from treasury_Balance c where c.crossAccount =:crossAccount  and c.status = :state group by c.creationDate ")
            // GROUP BY e.CREATION_DATE
            query.append("select MAX(e.CREATION_DATE) from TREASURY_BALANCE e  where e.CROSS_ACCOUNT_ID like ?1 and e.STATUS like ?2 and e.CREATION_DATE <=?3");
            lastOpeningDate = (Timestamp) em.createNativeQuery(query.toString())
                    .setParameter(1, id)
                    .setParameter(2, CheckerStatus.VALIDATED.name())
                    .setParameter(3, date)
                    .getFirstResult();

        } finally {
            tx.end();
        }
        return lastOpeningDate;
    }

    public List<Balance> loadOpeningBalancesList(CrossAccount crossAccount, LocalDate date) {
        List<Balance> balanceList = new ArrayList<>();
        if (crossAccount != null && date != null) {
            balanceList = dataManager.load(Balance.class)
                    .query("select e from treasury_Balance e where e.crossAccount =:crossAccount and e.creationDate =:date and e.status = :state")
                    .parameter("crossAccount", crossAccount)
                    .parameter("date", date)
                    .parameter("state", CheckerStatus.VALIDATED.name())
                    .view("balance-view")
                    .list();
        }
        return balanceList;
    }

    public boolean validateExistOpeningBalance(CrossAccount crossAccount, LocalDate date) {
        List<Balance> balanceList = new ArrayList<>();
        if (crossAccount != null && date != null) {
            balanceList = dataManager.load(Balance.class)
                    .query("select e from treasury_Balance e where e.crossAccount =:crossAccount and e.creationDate =:date and e.status = :state")
                    .parameter("crossAccount", crossAccount)
                    .parameter("date", date)
                    .parameter("state", CheckerStatus.VALIDATED.name())
                    .view("balance-view")
                    .list();
        }
        return (balanceList.size() > 0);
    }
}