package com.bdc.treasury.service;

import com.bdc.treasury.entity.ClosedCrossAccount;
import com.bdc.treasury.entity.CrossAccount;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.security.entity.Group;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.List;

@Service(ClosedAccountService.NAME)
public class ClosedAccountServiceBean implements ClosedAccountService {

    @Inject
    private DataManager dataManager;

    @Override
    public boolean checkOfClosedCrossAccount(CrossAccount crossAccount, LocalDate valueDate, String status, Group group) {
        List<ClosedCrossAccount> closedCrossAccountList = dataManager.load(ClosedCrossAccount.class)
                .query("select e from treasury_ClosedCrossAccount e where e.groups =:group and e.crossAccount =:crossAccount and e.valueDate =:valueDate and e.state =:status")
                .parameter("crossAccount", crossAccount)
                .parameter("valueDate", valueDate)
                .parameter("status", status)
                .parameter("group", group)
                .view("closedCrossAccount-view")
                .list();

        if (closedCrossAccountList.size() == 0) {
            return false;
        } else {
            return true;
        }
    }
}