package com.bdc.treasury.service;

import com.bdc.treasury.entity.CrossAccount;
import com.bdc.treasury.entity.Currency;
import com.haulmont.cuba.core.global.DataManager;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service(CrossAccountService.NAME)
public class CrossAccountServiceBean implements CrossAccountService {

    @Inject
    private DataManager dataManager;

    @Override
    public boolean currencyExist(Currency currency) {
        List<CrossAccount> currencyList = dataManager.load(CrossAccount.class)
                .query("select e from treasury_CrossAccount e  where e.currency =:currency and e.isMainAccount=1")
                .parameter("currency", currency)
                .view("crossAccount-view")
                .list();

        if (currencyList.size() > 0) {
            return true;
        } else {
            return false;
        }
    }
}