package com.bdc.treasury.service;

import com.bdc.treasury.core.Utils;
import com.bdc.treasury.entity.Balance;
import com.bdc.treasury.entity.CheckerStatus;
import com.bdc.treasury.entity.CrossAccount;
import com.bdc.treasury.entity.TreasuryCtrl;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Query;
import com.haulmont.cuba.core.global.DataManager;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service(TresuryControlService.NAME)
public class TresuryControlServiceBean implements TresuryControlService {

    @Inject
    private Persistence persistence;

    @Inject
    private DataManager dataManager;
    @Inject
    Utils utils;

    @Override
    public List<TreasuryCtrl> getTreasuryCtrls(LocalDate startDate) throws ParseException {
        List<CrossAccount> accounts = getCorrespondentAcount();

        return accounts.stream()
                .map(crossAccount -> utils.mapToTreasuryCtrl(crossAccount, startDate))
                .collect(Collectors.toList());
    }
    @Override
    public List<CrossAccount> getCorrespondentAcount() throws ParseException {
        return dataManager.load(CrossAccount.class)
                .query("select e from treasury_CrossAccount e  where e.status=:status")
                .parameter("status", CheckerStatus.VALIDATED.name())
                .view("crossAccount-view")
                .list();
    }

    @Override
    public boolean validateExistOpeningBalance(CrossAccount crossAccount, LocalDate date) {
        List<Balance> balanceList = new ArrayList<>();
        if (crossAccount != null && date != null) {
            balanceList = dataManager.load(Balance.class)
                    .query("select e from treasury_Balance e where e.crossAccount =:crossAccount and e.creationDate =:date and e.status = :state")
                    .parameter("crossAccount", crossAccount)
                    .parameter("date", date)
                    .parameter("state", CheckerStatus.VALIDATED.name())
                    .view("balance-view")
                    .list();
        }
        return (balanceList.size() > 0);
    }
    @Override
    public Timestamp getLastCreationDateOfOpeningBalance(CrossAccount crossAccount, LocalDate date) {
        com.haulmont.cuba.core.Transaction tx = persistence.createTransaction();
        Timestamp lastOpeningDate = null;
        try {
            String id = crossAccount.getUuid().toString().replace("-", "");
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            //select MAX(c.creationDate) from treasury_Balance c where c.crossAccount =:crossAccount  and c.status = :state group by c.creationDate ")
            // GROUP BY e.CREATION_DATE
            query.append("select MAX(e.CREATION_DATE) from TREASURY_BALANCE e  where e.CROSS_ACCOUNT_ID like ?1 and e.STATUS like ?2 and e.CREATION_DATE <=?3");
            lastOpeningDate = (Timestamp) em.createNativeQuery(query.toString())
                    .setParameter(1, id)
                    .setParameter(2, CheckerStatus.VALIDATED.name())
                    .setParameter(3, date)
                    .getFirstResult();

        } finally {
            tx.end();
        }
        return lastOpeningDate;
    }
}