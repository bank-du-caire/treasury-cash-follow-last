package com.bdc.treasury.service;

import com.bdc.treasury.entity.*;
import com.bdc.treasury.entity.Currency;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Query;
import com.haulmont.cuba.core.global.DataManager;
import org.eclipse.persistence.jpa.jpql.parser.DateTime;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Service(TransactionService.NAME)
public class TransactionServiceBean implements TransactionService {

    private final static Long TEMP_TIME = 5L; // 5 minutes
    @Inject
    private Persistence persistence;
    private LocalDate startOfDay;
    private LocalDate endofday;
    @Inject
    private TreasuryFrontOfficeService treasuryFrontOfficeService;
    @Inject
    private DataManager dataManager;
    @Inject
    private RemTransactionService remTransactionService;
    @Inject
    private DealsService dealsService;

    @Override
    public Boolean transactionIsExpired(Transaction transaction) {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        Date currentDate = new Date();
        Date updateTs = transaction.getUpdateTs();
        LocalDateTime localDateTimeUpdateTs = new Timestamp(updateTs.getTime()).toLocalDateTime();
        LocalDateTime currentLocalDateTime = LocalDateTime.now();
        int currentlDate = currentLocalDateTime.getMinute();
        int TimeUpdate = localDateTimeUpdateTs.getMinute();
        int x = currentLocalDateTime.getMinute() - localDateTimeUpdateTs.getMinute();
        System.out.println(currentLocalDateTime.getMinute() - localDateTimeUpdateTs.getMinute());
        //check same date
        boolean condition1 = fmt.format(transaction.getUpdateTs()).equals(fmt.format(currentDate));
        boolean condition2 = currentLocalDateTime.getHour() == localDateTimeUpdateTs.getHour();
        boolean condition3 = currentLocalDateTime.getMinute() - localDateTimeUpdateTs.getMinute() <= TEMP_TIME;
        return (condition1 && condition2 && condition3);
    }

    @Override
    public void updateTransaction(Transaction transaction) {
        com.haulmont.cuba.core.Transaction tx = persistence.createTransaction();
        try {
            EntityManager em = persistence.getEntityManager();
            em.merge(transaction);
        } catch (Exception e) {
            e.getStackTrace();
        }
        tx.commit();
        tx.end();
    }

    @Override
    public void deleteTransaction(Transaction transaction) {
        com.haulmont.cuba.core.Transaction tx = persistence.createTransaction();
        try {
            EntityManager em = persistence.getEntityManager();
            em.remove(transaction);
        } catch (Exception e) {
            e.getStackTrace();
        }
        tx.commit();
        tx.end();
    }

    @Override
    public List<Transaction> getTransactionCreationDate(Date date) {
        List<Transaction> transactionList = new ArrayList<>();
//        startOfDay = LocalDate.from(date.atStartOfDay());
//        endofday = LocalDate.from(date.atTime(23, 59, 59, 999999));


        com.haulmont.cuba.core.Transaction tx = persistence.createTransaction();
        try {
            EntityManager em = persistence.getEntityManager();
            transactionList = em.createQuery("select t from treasury_Transaction t where FUNC('TRUNC', t.createTs) < FUNC('TRUNC', :createTs) and t.deleteTs IS NULL", Transaction.class)
                    .setParameter("createTs", date)
                    .getResultList();
        } catch (Exception e) {
            e.getStackTrace();
        }
        tx.end();

        return transactionList;
    }

    @Override
    public List getRemmitanceList() {
        com.haulmont.cuba.core.Transaction tx = persistence.createTransaction();
        List transactionList = new ArrayList<>();
        try {
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            //query.append("select e.VALUE_DATE , sum(e.CREDIT) , sum(e.DEPT) from TREASURY_TRANSACTION e where e.ACCOUNT_ID like ?1 and e.VALUE_DATE between ?2 and ?3 and (e.STATUS like ?4 or e.STATUS like ?5) GROUP BY e.VALUE_DATE order by e.VALUE_DATE ASC");
            query.append("select r.ID , r.CURRENCY , r.VALUE_DATE ,\n" +
                    "(CASE WHEN r.AMOUNT > 0 THEN r.AMOUNT ELSE 0 END) CREDIT ,\n" +
                    "(CASE WHEN r.AMOUNT < 0 THEN r.AMOUNT ELSE 0 END) DEPT , r.STATUS , a.NAME " +
                    "from REMMITANCE_TRANSACTIONS r \n" +
                    ",TREASURY_CROSS_ACCOUNT a\n" +
                    ",TREASURY_CURRENCY c\n" +
                    "where \n" +
                    "c.CODE =r.CURRENCY and\n" +
                    "a.CURRENCY_ID=c.ID and\n" +
                    "a.IS_MAIN_ACCOUNT = 1 and\n" +
                    "r.STATUS is null ");
            transactionList = em.createNativeQuery(query.toString()).getResultList();
        } catch (Exception e) {
            e.getStackTrace();
        }
        tx.end();
        return transactionList;
    }

    @Override
    public List getRemmitanceListValidated() {
        com.haulmont.cuba.core.Transaction tx = persistence.createTransaction();
        List transactionList = new ArrayList<>();
        try {
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            //query.append("select e.VALUE_DATE , sum(e.CREDIT) , sum(e.DEPT) from TREASURY_TRANSACTION e where e.ACCOUNT_ID like ?1 and e.VALUE_DATE between ?2 and ?3 and (e.STATUS like ?4 or e.STATUS like ?5) GROUP BY e.VALUE_DATE order by e.VALUE_DATE ASC");
            query.append("select r.ID , r.CURRENCY , r.VALUE_DATE ,\n" +
                    "(CASE WHEN r.AMOUNT > 0 THEN r.AMOUNT ELSE 0 END) CREDIT ,\n" +
                    "(CASE WHEN r.AMOUNT < 0 THEN r.AMOUNT ELSE 0 END) DEPT , r.STATUS , a.NAME \n" +
                    "from TREASURY.REMMITANCE_TRANSACTIONS r \n" +
                    ", TREASURY.TREASURY_CROSS_ACCOUNT a\n" +
                    ",TREASURY.TREASURY_CURRENCY c\n" +
                    "where \n" +
                    "c.CODE =r.CURRENCY and\n" +
                    "a.CURRENCY_ID=c.ID and\n" +
                    "a.IS_MAIN_ACCOUNT = 1 and\n" +
                    "r.STATUS ='VALIDATED' ");
            transactionList = em.createNativeQuery(query.toString()).getResultList();
        } catch (Exception e) {
            e.getStackTrace();
        }
        tx.end();
        return transactionList;
    }

    @Override
    public void updateRemittanceStatus(BigDecimal id) {
        com.haulmont.cuba.core.Transaction tx = persistence.createTransaction();
        try {
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            //query.append("select e.VALUE_DATE , sum(e.CREDIT) , sum(e.DEPT) from TREASURY_TRANSACTION e where e.ACCOUNT_ID like ?1 and e.VALUE_DATE between ?2 and ?3 and (e.STATUS like ?4 or e.STATUS like ?5) GROUP BY e.VALUE_DATE order by e.VALUE_DATE ASC");
            query.append("update REMMITANCE_TRANSACTIONS set STATUS = 'VALIDATED'  where ID like ?1");
            em.createNativeQuery(query.toString()).setParameter(1, id).executeUpdate();
        } catch (Exception e) {
            e.getStackTrace();
        }
        tx.commit();
        tx.end();
    }

    @Override
    public void deleteRemittanceStatus(BigDecimal id) {
        com.haulmont.cuba.core.Transaction tx = persistence.createTransaction();
        try {
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            //query.append("select e.VALUE_DATE , sum(e.CREDIT) , sum(e.DEPT) from TREASURY_TRANSACTION e where e.ACCOUNT_ID like ?1 and e.VALUE_DATE between ?2 and ?3 and (e.STATUS like ?4 or e.STATUS like ?5) GROUP BY e.VALUE_DATE order by e.VALUE_DATE ASC");
            query.append("update REMMITANCE_TRANSACTIONS set STATUS = 'DELETED' where ID like ?1");
            em.createNativeQuery(query.toString()).setParameter(1, id).executeUpdate();
        } catch (Exception e) {
            e.getStackTrace();
        }
        tx.commit();
        tx.end();
    }

    @Override
    public List getTransactionListTest(CrossAccount crossAccount, LocalDate startDate, LocalDate endDate) {
        com.haulmont.cuba.core.Transaction tx = persistence.createTransaction();
        List transactionList = new ArrayList<>();
        try {
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            String id = crossAccount.getUuid().toString().replace("-", "");
            query.append("select trunc(a.VALUE_DATE) ,sum(a.CREDIT),sum(a.DEPT)\n" +
                    " from\n" +
                    "(\n" +
                    "    select e.VALUE_DATE ,\n" +
                    "     (e.CREDIT) ,\n" +
                    "      (e.DEPT)\n" +
                    "    From TREASURY_TRANSACTION e\n" +
                    "    where e.ACCOUNT_ID like ?8 and e.VALUE_DATE between ?2 and ?3 and (e.STATUS like ?4 or e.STATUS like ?5) and e.DELETE_TS is null \n" +
                    "    union all\n" +
                    "    select kd.VALUE_DATE , \n" +
                    "    (CASE WHEN kd.TRANSACTION_TYPE = ?10 THEN kd.AMOUNT ELSE 0 END) CREDIT\n" +
                    "    ,(CASE WHEN kd.TRANSACTION_TYPE = ?11 THEN kd.AMOUNT ELSE 0 END) DEPT\n" +
                    "     from kondor_deals kd  where kd.CURRENCY_CODE like ?1 and kd.VALUE_DATE BETWEEN ?2 AND ?3  AND " +
                    "kd.ID in ( select kd1.ID from Kondor_deals kd1 where kd1.DEAL_ID = kd.DEAL_ID and kd1.CAPTURE_DATE =( select max(kd2.CAPTURE_DATE) from Kondor_deals kd2\n" +
                    "\n" +
                    "where kd2.DEAL_ID =kd1.DEAL_ID and kd2.TRANSACTION_TYPE = kd1.TRANSACTION_TYPE and kd2.PAYMENT_INTERNAL_STATUS !='C' and kd2.VALIDATION_STATUS ='Y' and kd2.EVENT_ACTION !='D' and kd2.COUNTER_PART_NAME !='CAIRO INTERNATIONAL BANK- UGANDA' ) )\n" +
                    "\n" +
                    "and kd.PAYMENT_INTERNAL_STATUS !='C'\n" +
                    "\n" +
                    "and kd.VALIDATION_STATUS ='Y'\n" +
                    "\n" +
                    "and kd.EVENT_ACTION !='D'\n" +
                    "and kd.ACCOUNT_SHORT_NAME = ?12 \n"
                    + " union all\n" +
                    "    select e.VALUE_DATE ,\n" +
                    "     (e.CREDIT) ,\n" +
                    "     (e.DEPT)\n" +
                    "    From TREASURY_TRANSACTION e\n" +
                    "    where e.ACCOUNT_ID like ?8 and  (e.STATUS like ?4 or e.STATUS like ?5) and e.VALUE_DATE is null and e.DELETE_TS is null \n" + ") a group by trunc (a.VALUE_DATE)  \n"
            );
            transactionList = em.createNativeQuery(query.toString())
                    .setParameter(1, crossAccount.getCurrency().getCode())
                    .setParameter(2, startDate)
                    .setParameter(3, endDate)
                    .setParameter(4, CheckerStatus.VALIDATED.name())
                    .setParameter(5, Status.TEMPORARY.name())
                    .setParameter(8, id)
                    .setParameter(10, "Credit")
                    .setParameter(11, "Debit")
                    .setParameter(12, crossAccount.getShortName())
                    .getResultList();
        } finally {
            tx.end();
        }
        tx.end();
        return transactionList;
    }

    @Override
    public List getTransactionList(CrossAccount crossAccount, LocalDate startDate, LocalDate endDate) {
        com.haulmont.cuba.core.Transaction tx = persistence.createTransaction();
        List transactionList = new ArrayList<>();
        try {
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            String id = crossAccount.getUuid().toString().replace("-", "");
            //query.append("select e.VALUE_DATE , sum(e.CREDIT) , sum(e.DEPT) from TREASURY_TRANSACTION e where e.ACCOUNT_ID like ?1 and e.VALUE_DATE between ?2 and ?3 and (e.STATUS like ?4 or e.STATUS like ?5) GROUP BY e.VALUE_DATE order by e.VALUE_DATE ASC");
            /*query.append("select trunc(a.VALUE_DATE) ,sum(a.CREDIT),sum(a.DEPT)\n" +
                    " from\n" +
                    "(\n" +
                    "    select r.VALUE_DATE , \n" +
                    "    (CASE WHEN r.AMOUNT > 0 THEN r.AMOUNT ELSE 0 END) CREDIT ,\n" +
                    "     (CASE WHEN r.AMOUNT < 0 THEN r.AMOUNT ELSE 0 END) DEPT \n" +
                    "    from REMMITANCE_TRANSACTIONS r \n" +
                    "    where r.CURRENCY like ?1 and r.VALUE_DATE BETWEEN ?2 AND ?3   AND 1 = ?9  and r.STATUS like 'VALIDATED' \n" +
                    "    union all\n" +
                    "    select e.VALUE_DATE ,\n" +
                    "     (e.CREDIT) ,\n" +
                    "      (e.DEPT)\n" +
                    "    From TREASURY_TRANSACTION e\n" +
                    "    where e.ACCOUNT_ID like ?8 and e.VALUE_DATE between ?2 and ?3 and (e.STATUS like ?4 or e.STATUS like ?5) and e.DELETE_TS is null \n" +
                    "    union all\n" +
                    "    select d.VALUE_DATE , \n" +
                    "    (CASE WHEN d.TRANSACTION_TYPE = ?10 THEN d.AMOUNT ELSE 0 END) CREDIT\n" +
                    "    ,(CASE WHEN d.TRANSACTION_TYPE = ?11 THEN d.AMOUNT ELSE 0 END) DEPT\n" +
                    "     from DEALS d  where d.CURRENCY_CODE like ?1 and d.VALUE_DATE BETWEEN ?2 AND ?3   AND 1 = ?9 \n" + " union all\n" +
                    "    select e.VALUE_DATE ,\n" +
                    "     (e.CREDIT) ,\n" +
                    "     (e.DEPT)\n" +
                    "    From TREASURY_TRANSACTION e\n" +
                    "    where e.ACCOUNT_ID like ?8 and  (e.STATUS like ?4 or e.STATUS like ?5) and e.VALUE_DATE is null and e.DELETE_TS is null \n" + ") a group by trunc (a.VALUE_DATE)  \n"
            );*/
            query.append("select trunc(a.VALUE_DATE) ,sum(a.CREDIT),sum(a.DEPT)\n" +
                    " from\n" +
                    "(\n" +
                    "    select e.VALUE_DATE ,\n" +
                    "     (e.CREDIT) ,\n" +
                    "      (e.DEPT)\n" +
                    "    From TREASURY_TRANSACTION e\n" +
                    "    where e.ACCOUNT_ID like ?8 and e.VALUE_DATE between ?2 and ?3 and (e.STATUS like ?4 or e.STATUS like ?5) and e.DELETE_TS is null \n" +
                    "    union all\n" +
                    "    select d.VALUE_DATE , \n" +
                    "    (CASE WHEN d.TRANSACTION_TYPE = ?10 THEN d.AMOUNT ELSE 0 END) CREDIT\n" +
                    "    ,(CASE WHEN d.TRANSACTION_TYPE = ?11 THEN d.AMOUNT ELSE 0 END) DEPT\n" +
                    "     from DEALS d  where d.CURRENCY_CODE like ?1 and d.VALUE_DATE BETWEEN ?2 AND ?3    \n" + " union all\n" +
                    "    select e.VALUE_DATE ,\n" +
                    "     (e.CREDIT) ,\n" +
                    "     (e.DEPT)\n" +
                    "    From TREASURY_TRANSACTION e\n" +
                    "    where e.ACCOUNT_ID like ?8 and  (e.STATUS like ?4 or e.STATUS like ?5) and e.VALUE_DATE is null and e.DELETE_TS is null \n" + ") a group by trunc (a.VALUE_DATE)  \n"
            );
            transactionList = em.createNativeQuery(query.toString())
                    .setParameter(1, crossAccount.getCurrency().getCode())
                    .setParameter(2, startDate)
                    .setParameter(3, endDate)
                    .setParameter(4, CheckerStatus.VALIDATED.name())
                    .setParameter(5, Status.TEMPORARY.name())
                    .setParameter(8, id)
                    .setParameter(10, "Credit")
                    .setParameter(11, "Debit")
                    .getResultList();
        } finally {
            tx.end();
        }
        tx.end();
        return transactionList;
    }

    public TreasuryCtrl mapToTreasuryCtrl(CrossAccount crossAccount, LocalDate date) {

        BigDecimal minimumBalance = crossAccount.getMinimumBalance();
        BigDecimal totalBalance = BigDecimal.ZERO;
        BigDecimal transactionsBalance = BigDecimal.ZERO;
        BigDecimal totalDebit = BigDecimal.ZERO;
        BigDecimal totalCredit = BigDecimal.ZERO;
        TreasuryCtrl treasuryCtrl = dataManager.create(TreasuryCtrl.class);
        List<Balance> balances = loadOpeningBalancesList(crossAccount, date);
        if (balances.size() > 0) {
            List<Transaction> transactions = loadTransactionList(crossAccount, date);
            LocalDate now = LocalDate.now();
            // To Do filter transaction with date from UI // by from date and to date
            for (Transaction transaction : transactions) {
                transactionsBalance = transactionsBalance.add((transaction.getCredit() != null ? transaction.getCredit() : BigDecimal.ZERO)).subtract(transaction.getDept() != null ? transaction.getDept() : BigDecimal.ZERO);
                totalDebit = totalDebit.add((transaction.getDept() != null ? transaction.getDept() : BigDecimal.ZERO));
                totalCredit = totalCredit.add((transaction.getCredit() != null ? transaction.getCredit() : BigDecimal.ZERO));
            }
            for (Balance balance : balances) {
                BigDecimal totalExludedAmount = BigDecimal.ZERO;
                BigDecimal totalUnkownFunds = BigDecimal.ZERO;
                for (BalancesList balancesList : balance.getBalancesList()) {
                    totalExludedAmount = totalExludedAmount.add(balancesList.getExludedAmount() != null ? balancesList.getExludedAmount() : BigDecimal.ZERO);
                    totalUnkownFunds = totalUnkownFunds.add(balancesList.getUnkownFunds() != null ? balancesList.getUnkownFunds() : BigDecimal.ZERO);
                }
                // balance.getBalance() = Opening balance;
                BigDecimal openBalance = balance.getBalance();
                totalBalance = (openBalance != null ? openBalance : BigDecimal.ZERO).subtract(totalUnkownFunds != null ? totalUnkownFunds : BigDecimal.ZERO).subtract(totalExludedAmount != null ? totalExludedAmount : BigDecimal.ZERO);
                treasuryCtrl.setCorrespondentName(crossAccount);
//                treasuryCtrl.setBalance(totalBalance.subtract((minimumBalance != null ? minimumBalance : BigDecimal.ZERO)).add(transactionsBalance));
                treasuryCtrl.setBalance(treasuryFrontOfficeService.getClosedBalance(crossAccount, date));
//                treasuryCtrl.setCurrency(crossAccount.getCurrency().getName());
                //  Calculate total credit & total debit  //
                List<Transaction> transactionWithoutValueDateList = new ArrayList<>();
                transactionWithoutValueDateList = dataManager.load(Transaction.class)
                        .query("select e from treasury_Transaction e where e.account =:account and  e.valueDate is null and e.deleteTs is null ")
                        .parameter("account", crossAccount)
                        .view("transaction-view")
                        .list();
                for (Transaction transaction : transactionWithoutValueDateList) {
                    totalCredit = totalCredit.add(transaction.getCredit() != null ? transaction.getCredit() : BigDecimal.ZERO);
                    totalDebit = totalDebit.add(transaction.getDept() != null ? transaction.getDept() : BigDecimal.ZERO);
                }
               // List treasuryFrontOfficeRemitanceList = remTransactionService.getRemitanceTransaction(crossAccount, date);
                BigDecimal x = BigDecimal.ZERO;
               /* for (Object o : treasuryFrontOfficeRemitanceList) {
                    Object[] raw = ((Object[]) o);
                    x = (BigDecimal) raw[0] != null ? (BigDecimal) raw[0] : BigDecimal.ZERO;
                    if (x.compareTo(BigDecimal.ZERO) >= 0) {
                        totalCredit = totalCredit.add(x);
                    } else {
                        totalDebit = totalDebit.add(BigDecimal.ZERO.subtract(x));
                    }
                }*/

//                List treasuryFrontOfficeDealsList = dealsService.getDeals(crossAccount, date);
//                for (Object o : treasuryFrontOfficeDealsList) {
//                    Object[] raw = ((Object[]) o);
//                    x = (BigDecimal) raw[0] != null ? (BigDecimal) raw[0] : BigDecimal.ZERO;
//                    if (x.compareTo(BigDecimal.ZERO) >= 0) {
//                        totalCredit = totalCredit.add(x);
//                    } else {
//                        totalDebit = totalDebit.add(BigDecimal.ZERO.subtract(x));
//                    }
//                }
                /////////////////////////////////////////
                treasuryCtrl.setTotalCredit(totalCredit);
                treasuryCtrl.setTotalDebit(totalDebit);
            }
        } else if (balances.size() == 0) {
            treasuryCtrl.setCorrespondentName(crossAccount);
            treasuryCtrl.setBalance(null);
//            treasuryCtrl.setCurrency(crossAccount.getCurrency().getName());
            treasuryCtrl.setTotalCredit(totalCredit);
            treasuryCtrl.setTotalDebit(totalDebit);
        }

        return treasuryCtrl;
    }

    public List<Transaction> loadTransactionList(CrossAccount crossAccount, LocalDate date) {
        List<Transaction> transactionList = new ArrayList<>();
        if (crossAccount != null && date != null) {
            transactionList = dataManager.load(Transaction.class)
                    .query("select e from treasury_Transaction e where e.account =:account and e.valueDate =:date and (e.status = :stat1 or e.status = :stat2)")
                    .parameter("account", crossAccount)
                    .parameter("date", date)
                    .parameter("stat1", CheckerStatus.VALIDATED.name())
                    .parameter("stat2", Status.TEMPORARY.name())
                    .view("transaction-view")
                    .list();
        }
        return transactionList;
    }

    public List<Balance> loadOpeningBalancesList(CrossAccount crossAccount, LocalDate date) {
        List<Balance> balanceList = new ArrayList<>();
        if (crossAccount != null && date != null) {
            balanceList = dataManager.load(Balance.class)
                    .query("select e from treasury_Balance e where e.crossAccount =:crossAccount and e.creationDate =:date and e.status = :state")
                    .parameter("crossAccount", crossAccount)
                    .parameter("date", date)
                    .parameter("state", CheckerStatus.VALIDATED.name())
                    .view("balance-view")
                    .list();
        }
        return balanceList;
    }

    @Override
    public boolean checkCurrencyOfAccount(CrossAccount crossAccount, Currency currency) {
        com.haulmont.cuba.core.Transaction tx = persistence.createTransaction();
        List transactionList = new ArrayList<>();
        try {
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            String id = crossAccount.getUuid().toString().replace("-", "");
            //query.append("select e.VALUE_DATE , sum(e.CREDIT) , sum(e.DEPT) from TREASURY_TRANSACTION e where e.ACCOUNT_ID like ?1 and e.VALUE_DATE between ?2 and ?3 and (e.STATUS like ?4 or e.STATUS like ?5) GROUP BY e.VALUE_DATE order by e.VALUE_DATE ASC");
            query.append("select t.NAME  from TREASURY_CROSS_ACCOUNT t " +
                    "    where t.CURRENCY_ID = ?1 and t.ID = ?2 and t.STATUS = ?3 "
            );
            String accountId = crossAccount.getUuid().toString().replace("-", "");
            String currencyId = currency.getUuid().toString().replace("-", "");
            transactionList = em.createNativeQuery(query.toString())
                    .setParameter(1, currencyId)
                    .setParameter(2, accountId)
                    .setParameter(3, "VALIDATED")
                    .getResultList();
        } finally {
            tx.end();
        }
        tx.end();
        if (transactionList.size() > 0) {
            return true;
        } else {
            return false;
        }

    }
}