package com.bdc.treasury.service;

import com.bdc.treasury.entity.BalancesList;
import com.bdc.treasury.entity.CheckerStatus;
import com.bdc.treasury.entity.CrossAccount;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Query;
import com.haulmont.cuba.core.Transaction;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.security.entity.Group;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.*;

@Service(BalanceService.NAME)
public class BalanceServiceBean implements BalanceService {

    @Inject
    private Persistence persistence;
    @Inject
    private DataManager dataManager;

    @Override
    public Date getLastDateOfOpenBalance(CrossAccount crossAccount, UUID groupId) {
        Transaction tx = persistence.createTransaction();
        String group = groupId.toString().replace("-", "");
        List date = new ArrayList<>();
        try {
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            //query.append("select e.VALUE_DATE , sum(e.CREDIT) , sum(e.DEPT) from TREASURY_TRANSACTION e where e.ACCOUNT_ID like ?1 and e.VALUE_DATE between ?2 and ?3 and (e.STATUS like ?4 or e.STATUS like ?5) GROUP BY e.VALUE_DATE order by e.VALUE_DATE ASC");
            query.append("select MAX(b.CREATION_DATE)\n" +
                    "from TREASURY_BALANCE b \n" +
                    " where \n" +
                    " b.CROSS_ACCOUNT_ID = ?1 and b.STATUS = ?2 and b.GROUP_ID= ?3 \n" +
                    " group by b.CROSS_ACCOUNT_ID");
            String id = crossAccount.getUuid().toString().replace("-", "");
            date = em.createNativeQuery(query.toString())
                    .setParameter(1, id)
                    .setParameter(2, CheckerStatus.VALIDATED.name())
                    .setParameter(3, group)
                    .getResultList();
        } catch (Exception e) {
            e.getStackTrace();
        }
        tx.end();
        return (date.size() > 0 ? (Date) date.get(0) : null);
    }

    @Override
    public List<CrossAccount> getCorrespondentAcount() {
        return dataManager.load(CrossAccount.class)
                .query("select distinct(e.crossAccount) from treasury_Balance e  ")
                .view("crossAccount-view")
                .list();
    }
    @Override
    public void updateBalanceList(BalancesList balancesList) {
        com.haulmont.cuba.core.Transaction tx = persistence.createTransaction();
        try {
            EntityManager em = persistence.getEntityManager();
            em.merge(balancesList);
        } catch (Exception e) {
            e.getStackTrace();
        }
        tx.commit();
        tx.end();
    }
}