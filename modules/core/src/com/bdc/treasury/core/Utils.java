package com.bdc.treasury.core;

import com.bdc.treasury.entity.*;
import com.bdc.treasury.service.DealsService;
import com.bdc.treasury.service.RemTransactionService;
import com.bdc.treasury.service.TreasuryFrontOfficeService;
import com.bdc.treasury.service.TresuryControlService;
import com.haulmont.cuba.core.global.DataManager;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component(Utils.NAME)
public class Utils {

    public static final String NAME = "treasury_Utils";
    @Inject
    protected DataManager dataManager;
    @Inject
    TreasuryFrontOfficeService treasuryFrontOfficeService;
    @Inject
    private RemTransactionService remTransactionService;
    @Inject
    private DealsService dealsService;

    public Date getCurrentDate() throws ParseException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDateTime now = LocalDateTime.now();
        String currDate = dtf.format(now);
        Date date = new SimpleDateFormat("MM/dd/yyyy").parse(currDate);
        return date;
    }

    public TreasuryCtrl mapToTreasuryCtrl(CrossAccount crossAccount, LocalDate date) {
        BigDecimal minimumBalance = crossAccount.getMinimumBalance();
        BigDecimal totalBalance = BigDecimal.ZERO;
        BigDecimal transactionsBalance = BigDecimal.ZERO;
        BigDecimal totalDebit = BigDecimal.ZERO;
        BigDecimal totalCredit = BigDecimal.ZERO;
        TreasuryCtrl treasuryCtrl = dataManager.create(TreasuryCtrl.class);
        List<Balance> balances = loadOpeningBalancesList(crossAccount, date);
//        if (balances.size() > 0) {
        List<Transaction> transactions = loadTransactionList(crossAccount, date);
        LocalDate now = LocalDate.now();
        // To Do filter transaction with date from UI // by from date and to date
        for (Transaction transaction : transactions) {
            transactionsBalance = transactionsBalance.add((transaction.getCredit() != null ? transaction.getCredit() : BigDecimal.ZERO)).subtract(transaction.getDept() != null ? transaction.getDept() : BigDecimal.ZERO);
            totalDebit = totalDebit.add((transaction.getDept() != null ? transaction.getDept() : BigDecimal.ZERO));
            totalCredit = totalCredit.add((transaction.getCredit() != null ? transaction.getCredit() : BigDecimal.ZERO));
        }
        for (Balance balance : balances) {
            BigDecimal totalExludedAmount = BigDecimal.ZERO;
            BigDecimal totalUnkownFunds = BigDecimal.ZERO;
            String note = " ";
            for (BalancesList balancesList : balance.getBalancesList()) {
                totalExludedAmount = totalExludedAmount.add(balancesList.getExludedAmount() != null ? balancesList.getExludedAmount() : BigDecimal.ZERO);
                totalUnkownFunds = totalUnkownFunds.add(balancesList.getUnkownFunds() != null ? balancesList.getUnkownFunds() : BigDecimal.ZERO);
//                note = note.concat(balancesList.getExludedAmountDiscription()).concat("  ")
//                        .concat(balancesList.getUnkownFundsDiscription()).concat(" ");
                if (balancesList != null && balancesList.getExludedAmountDiscription() != null && balancesList.getUnkownFundsDiscription() != null)
                    note = note.concat(balancesList.getExludedAmountDiscription()).concat("  ")
                            .concat(balancesList.getUnkownFundsDiscription()).concat(" ");
            }
            // balance.getBalance() = Opening balance;
            treasuryCtrl.setNote(note);
            BigDecimal openBalance = balance.getBalance();

            totalBalance = (openBalance != null ? openBalance : BigDecimal.ZERO).subtract(totalUnkownFunds != null ? totalUnkownFunds : BigDecimal.ZERO).subtract(totalExludedAmount != null ? totalExludedAmount : BigDecimal.ZERO);

        }
        // new modification
        treasuryCtrl.setCorrespondentName(crossAccount);
        treasuryCtrl.setBalance(treasuryFrontOfficeService.getClosedBalanceForTreasuryCtrl(crossAccount, date));
        treasuryCtrl.setCurrency(crossAccount.getCurrency().getName());
        //  Calculate total credit & total debit  //
        List<Transaction> transactionWithoutValueDateList = new ArrayList<>();
        // transaction without value date
        transactionWithoutValueDateList = dataManager.load(Transaction.class)
                .query("select e from treasury_Transaction e where e.account =:account and  e.valueDate is null and e.deleteTs is null and (e.status = :stat1 or e.status = :stat2) ")
                .parameter("account", crossAccount)
                .parameter("stat1", CheckerStatus.VALIDATED.name())
                .parameter("stat2", Status.TEMPORARY.name())
                .view("transaction-view")
                .list();
        for (Transaction transaction : transactionWithoutValueDateList) {
            totalCredit = totalCredit.add(transaction.getCredit() != null ? transaction.getCredit() : BigDecimal.ZERO);
            totalDebit = totalDebit.add(transaction.getDept() != null ? transaction.getDept() : BigDecimal.ZERO);
        }
        //List treasuryFrontOfficeRemitanceList = remTransactionService.getRemitanceTransaction(crossAccount, date);
        BigDecimal x = BigDecimal.ZERO;
       // for (Object o : treasuryFrontOfficeRemitanceList) {
            //test  20-06-2021/*****************************************************************************************************
           /* Object[] raw = ((Object[]) o);
            x = (BigDecimal) raw[0] != null ? (BigDecimal) raw[0] : BigDecimal.ZERO;
            if (x.compareTo(BigDecimal.ZERO) >= 0) {
                totalCredit = totalCredit.add(x);
            } else {
                totalDebit = totalDebit.add(BigDecimal.ZERO.subtract(x));
            }*/
      //  }
        List treasuryFrontOfficeDealsList = dealsService.getDealsForTreasuryCTRL(crossAccount, date);
        for (Object o : treasuryFrontOfficeDealsList) {
            Object[] raw = ((Object[]) o);
            x = (BigDecimal) raw[0] != null ? (BigDecimal) raw[0] : BigDecimal.ZERO;
            if (x.compareTo(BigDecimal.ZERO) >= 0) {
                totalCredit = totalCredit.add(x);
            } else {
                totalDebit = totalDebit.add(BigDecimal.ZERO.subtract(x));
            }
        }
        /////////////////////////////////////////
        treasuryCtrl.setTotalCredit(totalCredit);
        treasuryCtrl.setTotalDebit(totalDebit);
        BigDecimal decimal = (totalCredit != null ? totalCredit : BigDecimal.ZERO).subtract((totalDebit != null ? totalDebit : BigDecimal.ZERO));
        treasuryCtrl.setDiffDebitCredit(decimal);
//        }
//        else if (balances.size() == 0) {
//            treasuryCtrl.setCorrespondentName(crossAccount);
//            treasuryCtrl.setBalance(null);
//            treasuryCtrl.setCurrency(crossAccount.getCurrency().getName());
//            treasuryCtrl.setTotalCredit(totalCredit);
//            treasuryCtrl.setTotalDebit(totalDebit);
//        }

        return treasuryCtrl;
    }

    public List<Transaction> loadTransactionList(CrossAccount crossAccount, LocalDate date) {
        List<Transaction> transactionList = new ArrayList<>();
        if (crossAccount != null && date != null) {
            transactionList = dataManager.load(Transaction.class)
                    .query("select e from treasury_Transaction e where e.account =:account and e.valueDate =:date and (e.status = :stat1 or e.status = :stat2)")
                    .parameter("account", crossAccount)
                    .parameter("date", date)
                    .parameter("stat1", CheckerStatus.VALIDATED.name())
                    .parameter("stat2", Status.TEMPORARY.name())
                    .view("transaction-view")
                    .list();
        }
        return transactionList;
    }

    public List<Balance> loadOpeningBalancesList(CrossAccount crossAccount, LocalDate date) {
        List<Balance> balanceList = new ArrayList<>();
        if (crossAccount != null && date != null) {
            balanceList = dataManager.load(Balance.class)
                    .query("select e from treasury_Balance e where e.crossAccount =:crossAccount and e.creationDate =:date and e.status = :state")
                    .parameter("crossAccount", crossAccount)
                    .parameter("date", date)
                    .parameter("state", CheckerStatus.VALIDATED.name())
                    .view("balance-view")
                    .list();
        }
        return balanceList;
    }


}