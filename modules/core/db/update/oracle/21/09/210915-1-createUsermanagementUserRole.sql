create table USERMANAGEMENT_USER_ROLE (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    USERS_ID varchar2(32),
    ROLE_ID varchar2(32),
    ROLE_NAME varchar2(255 char),
    --
    primary key (ID)
)^